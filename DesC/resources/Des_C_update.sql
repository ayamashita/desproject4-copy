ALTER TABLE tbl_audit MODIFY user_id  varchar(50) NOT NULL default '';

ALTER TABLE tbl_study_has_publication MODIFY publication_id  varchar(200) NOT NULL default '';

ALTER TABLE tbl_study_has_responsible MODIFY responsible_id  varchar(50) NOT NULL default '';

ALTER TABLE tbl_user MODIFY people_id  varchar(50) NOT NULL default '';

DELETE FROM tbl_audit;
DELETE FROM tbl_study_has_publication;
DELETE FROM tbl_study_has_responsible;
DELETE FROM tbl_user;

DROP TABLE people;
DROP TABLE publication;



INSERT INTO tbl_user (people_id, user_type_id) VALUES ('aiko', 1);

INSERT INTO tbl_user_type_has_action (action_id, user_type_id) VALUES (1, 1);

INSERT INTO  tbl_action (action_name, action_desc) VALUES ('MANAGE_REPORTS', 'Save, load, delete personal reports');

INSERT INTO tbl_user_type_has_action (action_id, user_type_id) VALUES (11, 1);
INSERT INTO tbl_user_type_has_action (action_id, user_type_id) VALUES (11, 3);




CREATE TABLE `tbl_people_report` (
  `people_id` varchar(50) NOT NULL default '',
  `report_name` varchar(50) NOT NULL default '',
  `responsibles_or_type` boolean NOT NULL default false,
  `subsort` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`people_id`,`report_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;

CREATE TABLE `tbl_people_report_column` (
  `people_id` varchar(50) NOT NULL default '',
  `report_name` varchar(50) NOT NULL default '',
  `column_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`people_id`,`report_name`, `column_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;

CREATE TABLE `tbl_people_report_responsible` (
  `people_id` varchar(50) NOT NULL default '',
  `report_name` varchar(50) NOT NULL default '',
  `responsible_id` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`people_id`,`report_name`, `responsible_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;