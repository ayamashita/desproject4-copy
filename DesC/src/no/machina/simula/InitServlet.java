/*
 * InitServlet.java
 *
 * Created on 15. oktober 2003, 14:47
 */

package no.machina.simula;

import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author  Cato Ervik
 * @version
 */
public class InitServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String dbdriver = config.getServletContext().getInitParameter("DBDriver");
        String dburl = config.getServletContext().getInitParameter("DBURL");
        String dbuser = config.getServletContext().getInitParameter("DBUser");
        String dbpwd = config.getServletContext().getInitParameter("DBPassword");
        String security = config.getServletContext().getInitParameter("Security");
        
        String simuladbdriver = config.getServletContext().getInitParameter("SimulaDBDriver");
        String simuladburl = config.getServletContext().getInitParameter("SimulaDBURL");
        String simuladbuser = config.getServletContext().getInitParameter("SimulaDBUser");
        String simuladbpwd = config.getServletContext().getInitParameter("SimulaDBPassword");
        
        String simulawsurl = config.getServletContext().getInitParameter("SimulaWSURL");
        String authwsurl = config.getServletContext().getInitParameter("AuthenticationWSURL");
        
        config.getServletContext().getServerInfo();
        System.setProperty("no.machina.simula.dbdriver", dbdriver);
        System.setProperty("no.machina.simula.dburl", dburl);
        System.setProperty("no.machina.simula.dbuser", dbuser);
        System.setProperty("no.machina.simula.dbpwd", dbpwd);
        System.setProperty("no.machina.simula.security", security);
        System.err.println("Security is " + security);
        
        System.setProperty("no.machina.simula.simula_dbdriver", simuladbdriver);
        System.setProperty("no.machina.simula.simula_dburl", simuladburl);
        System.setProperty("no.machina.simula.simula_dbuser", simuladbuser);
        System.setProperty("no.machina.simula.simula_dbpwd", simuladbpwd);   
        
        System.setProperty("no.machina.simula.wsurl", simulawsurl);
        System.setProperty("no.machina.simula.authenticationwsurl", authwsurl);
    }

    /** Destroys the servlet.
     */
    public void destroy() {
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
