/*
 * User.java
 *
 * Created on 10. oktober 2003, 11:15
 */

package no.machina.simula;

import java.util.List;

/**
 *
 * @author  Cato Ervik
 */
public class User {
    
    public final static String STUDY_ADMIN = "STUDY_ADMINISTRATOR";
    public final static String DATABASE_ADMIN = "DATABASE_ADMINISTRATOR";
    
    /** Holds value of property userName. */
    private String userName;
    
    /** Holds value of property userId. */
    private String userId;
    
    /** Holds list of actions available for user */
    private List actions;
    
    
    /** Getter for property userName.
     * @return Value of property userName.
     *
     */
    public String getUserName() {
        return this.userName;
    }    
    
    /** Setter for property userName.
     * @param userName New value of property userName.
     *
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }    
    
    /** Getter for property userId.
     * @return Value of property userId.
     *
     */
    public String getUserId() {
        return this.userId;
    }
    
    /** Setter for property userId.
     * @param userId New value of property userId.
     *
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public void setActions(List actions) {
		this.actions = actions;
	}
    
    public List getActions() {
		return actions;
	}
    
    public String toString() {
    	return "User id=" + getUserId() + " name=" + getUserName() + " actions=" + getActions();
    }
}
