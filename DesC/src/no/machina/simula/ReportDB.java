package no.machina.simula;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

/**
 * Class encapsulating Publication data access.
 *
 * @author :  Per Kristian Foss
 */
public class ReportDB {
	
	private DB db; 

	/**
	 * Constructs a PublicationDAO. 
	 *
	 * @throws SystemException
	 * @throws SQLException
	 */
	public ReportDB(DB db) {
		this.db = db;
	}
	
	public Vector getPersonReports(String loggedPersonId) throws Exception {
		System.out.println("LIST " + loggedPersonId);
		Vector reports = new Vector();
		if (Util.isNull(loggedPersonId)) {
			return reports;
		}
		
		StringBuffer query = new StringBuffer();    
	    query.append("select report_name from tbl_people_report where people_id = '").append(loggedPersonId).append("'");
	    
	    Statement stmt = db.getConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(query.toString());
	    
	    while(rs.next()) {
	    	reports.add(rs.getString("report_name"));
	    }
	    
	    rs.close();
	    stmt.close();
		
		return reports;
	}

	public HashMap getReport(String reportName, String loggedPersonId) throws Exception {
		System.out.println("GET " + reportName + " " + loggedPersonId);
		HashMap reportParams = new HashMap();
		
		StringBuffer query = new StringBuffer();    
	    query.append("select responsibles_or_type, subsort from tbl_people_report ");
	    query.append("where people_id = '").append(loggedPersonId).append("' and report_name = '").append(reportName).append("'");
	    
	    Statement stmt = db.getConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(query.toString());
	    
	    if (rs.next()) {
	    	reportParams.put("study_responsibles_or_type", new Boolean(rs.getBoolean("responsibles_or_type")));
	    	reportParams.put("sub_sort_by_with_order", rs.getString("subsort"));
	    }
	    rs.close();
	    
	    //Loading columns
	    Set columns = new HashSet();
	    
	    query = new StringBuffer();    
	    query.append("select column_name from tbl_people_report_column ");
	    query.append("where people_id = '").append(loggedPersonId).append("' and report_name = '").append(reportName).append("'");
	    
	    rs = stmt.executeQuery(query.toString());
	    
	    while (rs.next()) {
	    	columns.add(rs.getString("column_name"));
	    }
	    reportParams.put("columns_ids", columns);
	    rs.close();
	    
	    //Loading people responsible
	    Set responsibles = new HashSet();
	    
	    query = new StringBuffer();    
	    query.append("select responsible_id from tbl_people_report_responsible ");
	    query.append("where people_id = '").append(loggedPersonId).append("' and report_name = '").append(reportName).append("'");
	    
	    rs = stmt.executeQuery(query.toString());
	    
	    while (rs.next()) {
	    	responsibles.add(rs.getString("responsible_id"));
	    }
	    reportParams.put("study_responsibles", responsibles);
	    rs.close();
	    stmt.close();
	    
		return reportParams;
	}
	
	public void saveReport(String reportName, String loggedPersonId, HashMap reportParams) throws Exception {
		System.out.println("SAVE " + reportName + " " + loggedPersonId);
		Statement stmt = db.getConnection().createStatement();
		deleteReport(reportName, loggedPersonId, stmt);
		
		stmt.executeUpdate("insert into tbl_people_report (`people_id`,`report_name`, `responsibles_or_type`, `subsort`) values ('" + 
				loggedPersonId + "', '" + reportName + "', " + ((Boolean)reportParams.get("study_responsibles_or_type")).booleanValue() + 
				", '" + (String)reportParams.get("sub_sort_by_with_order") + "')");
		
		if (reportParams.get("columns_ids") != null) {
			Set columns = (Set)reportParams.get("columns_ids");
			for(Iterator iter = columns.iterator(); iter.hasNext();) {
				stmt.executeUpdate("insert into tbl_people_report_column (`people_id`,`report_name`, `column_name`) values ('" + 
						loggedPersonId + "', '" + reportName + "', '" + (String)iter.next() + "')");
			}
		}
		
		if (reportParams.get("study_responsibles") != null) {
			Set responsibles = (Set)reportParams.get("study_responsibles");
			for(Iterator iter = responsibles.iterator(); iter.hasNext(); ) {
				stmt.executeUpdate("insert into tbl_people_report_responsible (`people_id`,`report_name`, `responsible_id`) values ('" + 
						loggedPersonId + "', '" + reportName + "', '" + (String)iter.next() + "')");
			}
		}
	}
	
	public void deleteReport(String reportName, String loggedPersonId) throws Exception {
		System.out.println("DELETE " + reportName + " " + loggedPersonId);
		Statement stmt = db.getConnection().createStatement();
		deleteReport(reportName, loggedPersonId, stmt);
	}
	
	private void deleteReport(String reportName, String loggedPersonId, Statement stmt) throws SQLException {
		stmt.executeUpdate("delete from tbl_people_report_column where people_id = '" + loggedPersonId + "' and report_name = '" + reportName + "'");
		stmt.executeUpdate("delete from tbl_people_report_responsible where people_id = '" + loggedPersonId + "' and report_name = '" + reportName + "'");
		stmt.executeUpdate("delete from tbl_people_report where people_id = '" + loggedPersonId + "' and report_name = '" + reportName + "'");
	}

}