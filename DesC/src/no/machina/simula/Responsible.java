package no.machina.simula;

/*
 * Responsible.java
 *
 * Created on 8. oktober 2003, 16:39
 */

/**
 *
 * @author  sigmund
 */
public class Responsible {

    private String id;

    private String familyName;

    private String firstName;
    
    private String url;

    /** Creates a new instance of Responsible */
    public Responsible() {
    }

    /** Getter for property id.
     * @return Value of property id.
     *
     */
    public String getId() {
        return id;
    }

    /** Setter for property id.
     * @param id New value of property id.
     *
     */
    public void setId(String id) {
        this.id = id;
    }

    /** Getter for property familyName.
     * @return Value of property familyName.
     *
     */
    public java.lang.String getFamilyName() {
        return familyName;
    }

    /** Setter for property familyName.
     * @param familyName New value of property familyName.
     *
     */
    public void setFamilyName(java.lang.String familyName) {
        this.familyName = familyName;
    }

    /** Getter for property firstName.
     * @return Value of property firstName.
     *
     */
    public java.lang.String getFirstName() {
        return firstName;
    }

    /** Setter for property firstName.
     * @param firstName New value of property firstName.
     *
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }
    
    public String getUrl() {
		return url;
	}
    
    public void setUrl(String url) {
		this.url = url;
	}

    public String toString(){
		return "Responsible - id: " + getId() + " - firstName: " + getFirstName() + " - familyName:" + getFamilyName() + " - url: " + getUrl();
	}

}
