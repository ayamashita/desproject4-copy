package no.machina.simula;

public class Util {

	static public boolean isNull(String s) {
		return s == null || s.length() == 0;
	}
}
