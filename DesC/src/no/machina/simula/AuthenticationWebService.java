package no.machina.simula;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class AuthenticationWebService {
	
	private XmlRpcClientConfigImpl rpcConfig;
	public XmlRpcClient rcpClient;
	

	public AuthenticationWebService(String simulaWebURL)
	{
		this.rpcConfig = new XmlRpcClientConfigImpl();
		this.rcpClient = new XmlRpcClient();
		
		try {
			this.rpcConfig.setServerURL(new URL(simulaWebURL));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		this.rcpClient.setConfig(rpcConfig);
	}
	
	public Boolean getUser(String username, String password) {
		try {
			HashMap hash = new HashMap(); /* When hash is empty, there is no filtering which means: retrive all the publications */
			hash.put("login", username);
			hash.put("password", password); 
			
			Object[] methodParams = new Object[]{ hash }; /* The hash must always be contained in a Object array according to XmlRcp convention */
	    	return (Boolean)this.rcpClient.execute("check_credentials", methodParams);
		}
		catch (XmlRpcException xmlException)
		{
			return Boolean.FALSE;
		}
	}
}
