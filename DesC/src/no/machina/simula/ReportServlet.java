/*
 * ReportServlet.java
 *
 * Created on 6. oktober 2003, 18:19
 */

package no.machina.simula;

import java.io.*;
import java.lang.StringBuffer;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.fop.apps.Driver;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.xml.sax.InputSource;

/**
 *
 * @author  Cato Ervik
 * @version
 */
public class ReportServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String path = this.getServletConfig().getServletContext().getRealPath("xsl");
        String format = request.getParameter("format");
        String dateFormat = (String)getServletContext().getInitParameter("DateFormat");
        
        // Copy all the parameters to a HashMap to avoid the locked status
        Map paramMap = request.getParameterMap();
        Map newMap = new HashMap();
        Iterator it = paramMap.keySet().iterator();
        while ( it.hasNext() ) {
            String key = (String)it.next();
            newMap.put(key, paramMap.get(key));
        }
        paramMap = newMap;
        
        // Insert the dateformat and outputformat for the report
        paramMap.put("dateformat", dateFormat);
        paramMap.put("output-format", format);
        
        //Get and insert the sorting and searching parameters
        String sortByParam = request.getParameter("sort_by");
        String sortOrderParam = request.getParameter("sort_order");
        String searchString = request.getParameter("free_text_search");
        paramMap.put("sort_by", sortByParam);
        paramMap.put("sort_order", sortOrderParam);
        paramMap.put("free_text_search", searchString);
        
        // Start creating the report
        String contentStr = null;
        byte[] content = null;
        try {
            // If the format is pdf, create the xml document first, then format into pdf
            if ( "pdf".equalsIgnoreCase(format)) {
                Document doc = createDoc(paramMap);
                content = createPDF(doc, path, paramMap, "StudyList.xsl");
            } else if ("report_pdf".equalsIgnoreCase(format)) {
            	Document doc = createDoc(paramMap);
            	content = createPDF(doc, path, paramMap, "ReportStudyList.xsl");
            } // If the format is csv, create the csv format directly
            else if ( "csv".equalsIgnoreCase(format)) {
                contentStr = createCSV(paramMap);
            } // Redirect the user to an error page if the format is unknown
            else {
                response.sendRedirect("jsp/error.jsp?error=1");
            }
        } catch(Exception ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
            System.err.println(ExceptionUtils.getFullStackTrace(ex));
            Throwable root = ExceptionUtils.getRootCause(ex);
            String stack = ExceptionUtils.getFullStackTrace(root);
            if ( root != null ) {
                System.err.println(root.getClass().getName() + ": " + root.getMessage());
                System.err.println(stack);
            }
            HttpSession sess = request.getSession();
            if ( sess != null ) {
                sess.setAttribute("error.exception", ex);
            }
            response.sendRedirect("jsp/error.jsp?error=2");
            
            //request.getSession().setAttribute("error.exception", ex);
        }
        
        // Finally, return the resulting reports either as binary pdf or a csv string
        if ( ("pdf".equalsIgnoreCase(format) || "report_pdf".equalsIgnoreCase(format)) && content != null) {
            response.setContentLength(content.length);
            response.setContentType("application/pdf");
            response.getOutputStream().write(content);
            response.getOutputStream().flush();
        } else if ( "csv".equalsIgnoreCase(format) && contentStr != null) {
            byte[] csvAsBytes = contentStr.getBytes();
            response.setContentType("text/csv");
            response.setContentLength(contentStr.length());
            response.getWriter().write(contentStr);
        }
    }
    
    private static String[] csvHeaders;
    
    public static String createCSV(Map params) throws Exception {
        String retStr = null;
        
        // Get the dateformat that will be used to format start and end dates
        String dateFormat = (String)params.get("dateformat");
        if ( dateFormat == null || dateFormat.trim().length() == 0 ) {
            dateFormat = "yyyy-MM-dd";
        }
        
        // Get all the searching and sorting parameters and convert to a suitable format
        StringBuffer sb = new StringBuffer();
        Date earlyDate = Converter.stringToDate(Converter.getFirstString(params.get("early_end_date")), dateFormat);
        Date lateDate = Converter.stringToDate(Converter.getFirstString(params.get("late_end_date")), dateFormat);
        int studyType = NumberUtils.stringToInt(Converter.getFirstString(params.get("study_type_id")));
        String[] studyRepsStr = (String[])params.get("study_responsibles");
        String[] studyreps = null;
        if ( !ArrayUtils.contains(studyRepsStr, "0") ) {
            studyreps = studyRepsStr;
        }
        int sortBy = DB.SORT_STUDIES_STUDY_NAME; // Set default sort column
        int sortOrder = DB.SORT_ORDER_ASCENDING; // Set default sort order
        String sortByParam = (String)params.get("sort_by");
        String sortOrderParam = (String)params.get("sort_order");
        try{ sortBy = Integer.parseInt(sortByParam); }
        catch (NumberFormatException nfe){}
        try{ sortOrder = Integer.parseInt(sortOrderParam); }
        catch (NumberFormatException nfe){}
        String freeTextSearch = (String)params.get("free_text_search");
        
        // Get the list of studies, either using the freetext search or using studytype, dates etc.
        List studyList = null;
        if ( freeTextSearch == null || freeTextSearch.trim().length() == 0) {
            studyList = DB.getInstance().getStudies(studyType, earlyDate, lateDate, studyreps,sortBy,sortOrder);
        } else {
            studyList = DB.getInstance().getStudies(freeTextSearch,sortBy,sortOrder);
        }
        int size = -1;
        
        // // Add headers the headers for the csv report
        csvHeaders = new String[] {"Study Name", "Study type", "Desc", "Duration", "Unit", "Start",
        "End", "Keywords", "NoStudents", "NoProfs", "Notes", "Owner", "Last Edited By"};
        size = csvHeaders.length;
        for ( int i = 0;i < size;i++) {
            addToBuffer(csvHeaders[i], sb);
        }
        sb.append("\n");

        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        
        size = studyList != null ? studyList.size() : 0;
        for ( int i = 0;i < size;i++) {
            Study tmpStudy = (Study)studyList.get(i);
            StringBuffer studyBuffer = new StringBuffer();
            addToBuffer(tmpStudy.getName(), studyBuffer);
            addToBuffer(DB.getInstance().getStudyTypeName(tmpStudy.getTypeId()), studyBuffer);
            addToBuffer(tmpStudy.getDesc(), studyBuffer);
            addToBuffer(new Integer(tmpStudy.getDuration()), studyBuffer);
            addToBuffer(tmpStudy.getUnit(), studyBuffer);
            addToBuffer(tmpStudy.getStart() != null ? df.format(tmpStudy.getStart()) :  "", studyBuffer);
            addToBuffer(tmpStudy.getEnd() != null ? df.format(tmpStudy.getEnd()) :  "", studyBuffer);
            addToBuffer(tmpStudy.getKeywords(), studyBuffer);
            addToBuffer(new Integer(tmpStudy.getNoOfStudents()), studyBuffer);
            addToBuffer(new Integer(tmpStudy.getNoOfProfessionals()), studyBuffer);
            addToBuffer(tmpStudy.getStudyNotes(), studyBuffer);
            addToBuffer(tmpStudy.getOwner(), studyBuffer);
            addToBuffer(tmpStudy.getLastEditedBy(), studyBuffer);
            
            // Creates the string for the the basic elements for a study
            String currentStr = studyBuffer.toString();
            
            // Then get the publications and responsibles and output
//            List respsList = tmpStudy.getResponsibles();
//            int respSize = respsList!= null ? respsList.size() : 0;
//            List pubList = tmpStudy.getPublications();
//            int pubSize = pubList != null ? pubList.size() : 0;
//            for ( int j = 0;j < pubSize;j++) {
//                Publication pub = (Publication)pubList.get(j);
//                sb.append(currentStr);
//                addToBuffer(pub.getTitle(), sb);
//                addToBuffer(pub.getShortTitle(), sb);
//                if ( respSize > j ) {
//                    Responsible resp = (Responsible)respsList.get(j);
//                    addToBuffer(resp.getFirstName() + " " + resp.getFamilyName(), sb);
//                    //addToBuffer(resp.getFamilyName(), sb);
//                } else {
//                    addToBuffer("", sb);
//                    //addToBuffer("", sb);
//                }
//                sb.append("\n");
//            }
            
//            for ( int j = pubSize;j < respSize;j++) {
//                Responsible resp = (Responsible)respsList.get(j);
//                sb.append(currentStr);
//                addToBuffer("", sb);
//                addToBuffer("", sb);
//                addToBuffer(resp.getFirstName(), sb);
//                addToBuffer(resp.getFamilyName(), sb);
//                sb.append("\n");
//            }
            
            sb.append(currentStr);
            sb.append("\n");
        }
        
        retStr = sb.toString();
        return retStr;
    }
    
    private static char separator = ';';
    
    private static void addToBuffer(Object obj, StringBuffer sb) {
        int lineIndex = sb.length() - sb.lastIndexOf("\n");
        
        if ( lineIndex > 1 ) {
            sb.append(separator);
        }
        // Replace negative numbers
        if ( obj instanceof Integer && ((Integer)obj).intValue() < 0 ) {
            obj = new Integer(0);
        }
        
        // Search and replace special characters
        String candString = obj != null ? obj.toString() : "";

        StringUtils.replace(candString, "\"", "\"\"");
        //StringUtils.replace(candString, "\n", " ");
        // Cut the string to 100 chars
        if ( candString != null && candString.length() > 100 ) {
            candString = candString.substring(0,100);
        }

        if ( candString.indexOf(',') != -1 || candString.indexOf('\n') != -1 || candString.indexOf(';') != -1) {
            candString = "\"" + candString + "\"";
        }
        //System.err.println("Appending: " + candString);
        sb.append(candString);
    }
    
    public static Document createDoc(Map params) throws Exception {
        Document doc = null;
        Element root = new Element("study-list");
        doc = new Document(root);
        
        String dateFormat = (String)params.get("dateformat");
        if ( dateFormat == null || dateFormat.trim().length() == 0 ) {
            dateFormat = "yyyy-MM-dd";
        }
        List studyList = null;
        
        if ("report_pdf".equals(params.get("output-format"))) {
        	Set study_responsibles = new HashSet();
        	boolean study_responsibles_or_type = false; 
        	String sub_sort_by_with_order = "";
        	
        	String[] studyResponsiblesParam = (String[])params.get("study_responsibles");
        	if (studyResponsiblesParam != null) {
        		for(int i = 0; i < studyResponsiblesParam.length; ++i) {
        			study_responsibles.add(studyResponsiblesParam[i]);
        		}
        	}
        	
        	study_responsibles_or_type = Boolean.TRUE.toString().equals(((String[])params.get("study_responsibles_or_type"))[0]);
        	
        	String subSortByWithOrderParam = ((String[])params.get("sub_sort_by_with_order"))[0];
        	if (subSortByWithOrderParam != null) {
        		sub_sort_by_with_order = subSortByWithOrderParam;
        	}
        	
        	studyList = DB.getInstance().getStudies(study_responsibles, study_responsibles_or_type, sub_sort_by_with_order);
        } else {
	        Date earlyDate = Converter.stringToDate(Converter.getFirstString(params.get("early_end_date")), dateFormat);
	        Date lateDate = Converter.stringToDate(Converter.getFirstString(params.get("late_end_date")), dateFormat);
	        int studyType = NumberUtils.stringToInt(Converter.getFirstString(params.get("study_type_id")));
	        String[] studyRepsStr = (String[])params.get("study_responsibles");
	        String[] studyreps = null;
	        if ( !ArrayUtils.contains(studyRepsStr, "0") ) {
	            studyreps = studyRepsStr;
	        }
	        int sortBy = DB.SORT_STUDIES_STUDY_NAME; // Set default sort column
	        int sortOrder = DB.SORT_ORDER_ASCENDING; // Set default sort order
	        String sortByParam = (String)params.get("sort_by");
	        String sortOrderParam = (String)params.get("sort_order");
	        try{ sortBy = Integer.parseInt(sortByParam); }
	        catch (NumberFormatException nfe){}
	        try{ sortOrder = Integer.parseInt(sortOrderParam); }
	        catch (NumberFormatException nfe){}
	        String freeTextSearch = (String)params.get("free_text_search");
	        
	        //System.err.print("studytype=" + studyType + ", earlyDate=" + earlyDate + ", lateDate=" + lateDate);
	        int stsize = studyreps != null ? studyreps.length : 0;
	        //        for ( int i = 0;i < stsize;i++) {
	        //            System.err.print(", rep=" + studyreps[i]);
	        //        }
	        //        System.err.println("");
	        
	        
	        if ( freeTextSearch == null || freeTextSearch.trim().length() == 0) {
	            studyList = DB.getInstance().getStudies(studyType, earlyDate, lateDate, studyreps,sortBy,sortOrder);
	        } else {
	            studyList = DB.getInstance().getStudies(freeTextSearch,sortBy,sortOrder);
	        }
        }
        
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", new Locale("en", "GB"));
        
        if ("report_pdf".equals(params.get("output-format"))) {
        	Set columns = new HashSet();
        	String[] columnsParam = (String[])params.get("columns_ids");
        	if (columnsParam != null) {
        		for(int i = 0; i < columnsParam.length; ++i) {
        			columns.add(columnsParam[i]);
        		}
        	}
        	
        	String[] allColumns = new String[] {"name", "responsibles", "type", "description", "duration", "start", "end", "publications", 
					"keywords", "studentParticipants", "proffesionalParticipants", "material", "notes", "owner", "lastEdited"};
			String[] allColumnsNames = new String[] {"Study name", "Study Responsibles", "Type of study", "Study Description", "Duration", "Start Of Study", 
			"End of study", "Publications", "Keywords", "Student Participants", "Professional Participants", "Study Material" , "Study Notes",
			"Study Owner", "Last Edited by"};
			//String[] allColumnsSizes = new String[] {"2cm","3cm","1cm","3cm","1cm","1cm","1cm","3cm","4cm","1cm","1cm","3cm","3cm","1cm","1cm"};
			String[] allColumnsSizes = new String[] {"14mm","14mm","10mm","15mm","8mm","12mm","12mm","30mm","10mm","5mm","5mm","20mm","1cm","1cm","1cm"};
			
			
			Element columnsEl = new Element("columns");
			root.addContent(columnsEl);
			Element column = null;
			for(int i = 0; i < allColumns.length; ++i) {
				if (columns.contains(allColumns[i])) {
					column = new Element("column");
					column.setAttribute("name", allColumns[i]);
					column.setAttribute("label", prepare(allColumnsNames[i]));
					column.setAttribute("column-width", allColumnsSizes[i]);
					columnsEl.addContent(column);
				}
			}
        }

        int size = studyList != null ? studyList.size() : 0;
        //System.err.println("Retrieved " + (studyList != null ? studyList.size() : 0) + " records");
        for ( int i = 0;i < size;i++) {
            Study tmpStudy = (Study)studyList.get(i);
            Element tmpElem = new Element("study");
            root.addContent(tmpElem);
            tmpElem.addContent(new Element("name").addContent(prepare(tmpStudy.getName())));
            tmpElem.addContent(new Element("type").addContent(prepare(DB.getInstance().getStudyTypeName(tmpStudy.getTypeId()))));
            tmpElem.addContent(new Element("desc").addContent(prepare(tmpStudy.getDesc())));
            tmpElem.addContent(new Element("duration").addContent(prepare(tmpStudy.getDuration()) + " " + prepare(tmpStudy.getUnit())));
            tmpElem.addContent(new Element("unit").addContent(prepare(tmpStudy.getUnit())));
            tmpElem.addContent(new Element("end").addContent(prepare(tmpStudy.getEnd() != null ? df.format(tmpStudy.getEnd()) : "")));
            tmpElem.addContent(new Element("start").addContent(prepare(tmpStudy.getStart() != null ? df.format(tmpStudy.getStart()) : "")));
            tmpElem.addContent(new Element("keywords").addContent(prepare(tmpStudy.getKeywords())));
            tmpElem.addContent(new Element("no-students").addContent(prepare(tmpStudy.getNoOfStudents())));
            tmpElem.addContent(new Element("no-profs").addContent(prepare(tmpStudy.getNoOfProfessionals())));
            tmpElem.addContent(new Element("notes").addContent(prepare(tmpStudy.getStudyNotes())));
            tmpElem.addContent(new Element("owner").addContent(prepare(tmpStudy.getOwner())));
            tmpElem.addContent(new Element("last-edited-by").addContent(prepare(tmpStudy.getLastEditedBy())));
            
            // Then create the list of publications
            List pubs = tmpStudy.getPublications();
            int noPubs = pubs != null ? pubs.size() : 0;
            for ( int j = 0;j < noPubs;j++) {
                Publication pub = (Publication)pubs.get(j);
                Element pubElem = new Element("publication");
                tmpElem.addContent(pubElem);
                pubElem.addContent(new Element("title").addContent(prepare(pub.getTitle())));
                pubElem.addContent(new Element("short-title").addContent(prepare(pub.getShortTitle())));
            }
            
            // and the list of responsibles
            List resps = tmpStudy.getResponsibles();
            int noResps = resps != null ? resps.size() : 0;
            for ( int j = 0;j < noResps;j++) {
                Responsible resp = (Responsible)resps.get(j);
                Element respElem = new Element("responsible");
                tmpElem.addContent(respElem);
                respElem.addContent(new Element("first-name").addContent(prepare(resp.getFirstName())));
                respElem.addContent(new Element("family-name").addContent(prepare(resp.getFamilyName())));
                respElem.addContent(new Element("name").addContent(prepare(resp.getFirstName() + " " + resp.getFamilyName())));
            }
            

            List mats = DB.getInstance().getFilesForStudy(tmpStudy.getId());
            int noMats = mats != null ? mats.size() : 0;
            for ( int j = 0;j < noMats;j++) {
            	FileInfo mat = (FileInfo)mats.get(j);
                Element matElem = new Element("material");
                tmpElem.addContent(matElem);
                matElem.addContent(new Element("desc").addContent(prepare(mat.getDesc())));
            }
        }
        
        return doc;
    }
    
    static private String prepare(int value) {
    	if (value == -1) {
    		return "";
    	}
    	return Integer.toString(value);
    }
    
    static private String prepare(String text) {
    	if (text == null) {
    		return "";
    	}
    	StringBuffer result = new StringBuffer();
    	
    	String[] words = text.split(" ");
		for(int i = 0; i < words.length; ++i) {
			String word = words[i];
			int wordLength = word.length();
			for(int j = 0; j < wordLength; j += 5) {
				result.append(word.substring(j, Math.min(j + 5, wordLength)));
				if (j + 5 < wordLength) {
					result.append('\u200B');
				}
			}
			
			result.append(' ');
		}
    	return result.toString();
    }
    
    public static byte[] createPDF(Document doc, String xslPath, Map userInput, String xslFile) throws Exception {
        XMLOutputter xo = new XMLOutputter();
        StringWriter xmlOut = new StringWriter();
        xo.output(doc, xmlOut);
        String xmlOutStr = xmlOut.getBuffer().toString();
        String stylesheet = xslPath + File.separatorChar + xslFile;
        //System.err.println("Opening: " + stylesheet);
        
        // get an instance of a transformer, or initialize a new
        Transformer t = null;
        Transformer transInstance = null;
        if ( transInstance == null ) {
            StreamSource xslSource = new StreamSource(new FileReader(stylesheet));
            TransformerFactory tf = TransformerFactory.newInstance();
            transInstance = tf.newTransformer(xslSource);
        }
        t = transInstance;
        
        // Transform the document
        Source xmlSource = new javax.xml.transform.dom.DOMSource(new org.jdom.output.DOMOutputter().output(doc));
        StringWriter sw = new StringWriter();
        Result dr = new StreamResult(sw);
        t.transform(xmlSource,dr);
        sw.flush();
        String foOutStr = sw.getBuffer().toString();
        
        // Run FOP
        InputSource foSrc = new InputSource(new StringReader(sw.toString()));
        ByteArrayOutputStream outByte = new ByteArrayOutputStream();
        Driver driverInstance = null;
        if ( driverInstance == null ) {
            org.apache.fop.configuration.Configuration.put("baseDir",xslPath);
            driverInstance = new Driver(foSrc, outByte);
            driverInstance.setRenderer(org.apache.fop.apps.Driver.RENDER_PDF);
        } else {
            driverInstance.reset();
            driverInstance.setInputSource(foSrc);
            driverInstance.setOutputStream(outByte);
        }
        
        driverInstance.run();
        byte[] content = outByte.toByteArray();
        return content;
    }
    
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
