<!-- THIS IS THE MAIN PAGE FOR DES ADMINS --->
<%@page import="no.machina.simula.*, java.util.*"%>
<%@page buffer="64kb" %>

<!-- Include standard header -->
<jsp:include page="../header.jsp" />
<%
    // First get some stats about the user
    HttpSession sess = request.getSession();
    
    User user = (User)sess.getAttribute("simula_user");

    String pageTitle = "Main";
   
    if (!DB.isUserCanPerformAction(user, "VIEW_MAIN")) {
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>

      No Access, please login
</p>
</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%  return;
    } else { %>



<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td valign="top" width="40%">
<p>
<span class="path">studies > admin main</span>


<%-- Get admin message from database and display it --%>
<%=DB.getInstance().getAdminMessage() %>



<!-- Only available for DB Admins -->
<% if (DB.isUserCanPerformAction(user, "EDIT_TEXT")) { %>
<p class="bodytext-bold">
<a title="Click to edit the text on this page (HTML)" href="edit_admin_text.jsp">Edit text on this page</a>
</p>
<% } %>


	

</td>


<td valign="top" width="30%">
<br><br><br>
<jsp:include page="../menu.jsp" />
</td>

<td width="15%">
&nbsp;
</td>

</tr>

</table>

<% } %>


<!-- Include standard footer -->
<br><br><br><br>
<jsp:include page="../footer.jsp" />
