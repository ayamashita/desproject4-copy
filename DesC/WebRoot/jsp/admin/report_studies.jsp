<!-- THIS IS THE STUDY SEARCH RESULTS PAGE -->
<%@ page import="no.machina.simula.*, java.util.*, java.text.*, java.sql.SQLException" %>
<%@ page import="org.apache.commons.lang.*" %>
<%@ page buffer="64kb" %>


<%-- Include standard header --%>
<jsp:include page="../header.jsp" />
<%
    // First get some stats about the user
    User user = (User)request.getSession().getAttribute("simula_user");
%>

<%
String feedbackMsg = "";
DB db = DB.getInstance();

// Search params set up - BEGIN
Set columns = new HashSet();
Set study_responsibles = new HashSet();
boolean study_responsibles_or_type = false; 
String sub_sort_by_with_order = "";

String[] columnsParam = request.getParameterValues("columns_ids");
if (columnsParam != null) {
	for(int i = 0; i < columnsParam.length; ++i) {
		columns.add(columnsParam[i]);
	}
}

String[] studyResponsiblesParam = request.getParameterValues("study_responsibles");
if (studyResponsiblesParam != null) {
	for(int i = 0; i < studyResponsiblesParam.length; ++i) {
		study_responsibles.add(studyResponsiblesParam[i]);
	}
}

study_responsibles_or_type = Boolean.TRUE.toString().equals(request.getParameter("study_responsibles_or_type"));

String subSortByWithOrderParam = request.getParameter("sub_sort_by_with_order");
if (subSortByWithOrderParam != null) {
	sub_sort_by_with_order = subSortByWithOrderParam;
}
// Search params set up - BEGIN

List studies;
try{
        studies = db.getStudies(study_responsibles, study_responsibles_or_type, sub_sort_by_with_order);
}
catch(SQLException se){
    out.println("<tr><td>" + se.getMessage() + "</td></tr>");
    out.println("<tr><td>" + db.lastStmt + "</td></tr>");
    return;
}

String startRecordParam = null;
String endRecordParam = null;
// Set default range to be 0 to 'MaxRecordsInSearchList'
int startRecord = 0;
String maxRecordsPerPageParam = application.getInitParameter("MaxRecordsInSearchList");
int maxRecordsPerPage = 0;
maxRecordsPerPage = Integer.parseInt(maxRecordsPerPageParam); // no try-catch, want this to fail if MaxRecordsInSearchList is not set.
int endRecord = maxRecordsPerPage;

// Get parameters for first and last record to show
startRecordParam = request.getParameter("start_record");
endRecordParam = request.getParameter("end_record");
try{
    startRecord = Integer.parseInt(startRecordParam);
    endRecord = Integer.parseInt(endRecordParam);
}
catch(NumberFormatException nfe){
}

String action = request.getParameter("report_action");
if (action != null){
	if ("prev_page".equals(action)) {
		startRecord -= maxRecordsPerPage;
		endRecord -= maxRecordsPerPage;
	}
	if ("next_page".equals(action)) {
		startRecord += maxRecordsPerPage;
		endRecord += maxRecordsPerPage;
	}
}
%>






<!-- MAIN BODY -->


<tr>

<td width="5%">
&nbsp;
</td>

<td width="80%" valign="top">

<p>
<span class="path">studies > report > report results</span>


<p>

<%
if (studies.size() < 1){ // if there where no hits
%>
    Your search did not return any studies. <a href="report_search.jsp">Try again</a><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<%
}
else{
%>
    Currently showing results <b><%=startRecord + 1%></b> - <b><%=endRecord <= (studies.size() - 1)? endRecord : studies.size()%></b> of <b><%=studies.size()%></b>.
    Go back to <a href="report_search.jsp">reports</a>.
    <p>
    <table cellpadding="5" cellspacing="1">

    <%
       if ( feedbackMsg != "" ) {
    %>
        <tr>
        <td colspan="8"><%=feedbackMsg %></td>
        </tr>
    <% } %>

    </td></tr>
    <tr>
      <% if (columns.contains("name")) { %><td class="label">Study Name</td><% } %>
      <% if (columns.contains("responsibles")) { %><td class="label">Study Responsibles</td><% } %>
      <% if (columns.contains("type")) { %><td class="label">Type of study</td><% } %>
      <% if (columns.contains("description")) { %><td class="label">Study Description</td><% } %>
      <% if (columns.contains("duration")) { %><td class="label">Duration</td><% } %>
      <% if (columns.contains("start")) { %><td class="label">Start Of Study</td><% } %>
      <% if (columns.contains("end")) { %><td class="label">End of study</td><% } %>
      <% if (columns.contains("publications")) { %><td class="label">Publications</td><% } %>
      <% if (columns.contains("keywords")) { %><td class="label">Keywords</td><% } %>
      <% if (columns.contains("studentParticipants")) { %><td class="label">Student Participants</td><% } %>
      <% if (columns.contains("proffesionalParticipants")) { %><td class="label">Professional Participants</td><% } %>
      <% if (columns.contains("material")) { %><td class="label">Study Material</td><% } %>
      <% if (columns.contains("notes")) { %><td class="label">Study Notes</td><% } %>
      <% if (columns.contains("owner")) { %><td class="label">Study Owner</td><% } %>
      <% if (columns.contains("lastEdited")) { %><td class="label">Last Edited by</td><% } %>

    </tr>

    <%-- for each study in table. Listing all studies --%>
    <%
    // First check startRecord to avoid index out of bounds
    if (startRecord > studies.size() -1){
        startRecord = studies.size() - 1;
    }
    for (ListIterator i = studies.listIterator(startRecord); i.hasNext(); ) {
            Study study = (Study) i.next();
            if (i.nextIndex() > endRecord){
                break;
            }
    %>

    <tr>
      <% if (columns.contains("name")) { %>
      	<td valign="top" class="content"><a href="../view_study.jsp?id=<%=study.getId() %>"><%=study.getName() %></a></td>
      <% } %>

      <% if (columns.contains("responsibles")) { %>
	      <td valign="top" class="content">
	        <%
	            List responsibles = study.getResponsibles();
	            for (ListIterator j = responsibles.listIterator(); j.hasNext(); ) {
	                Responsible r = (Responsible) j.next();
	                out.println("<a target=\"_blank\" href=\"" + r.getUrl() + "\">" + r.getFirstName() + " " + r.getFamilyName() + "</a>");
	                // add a line break if there's more coming
	                if (j.hasNext()) out.println("<br>");
	            }
	        %>
	      </td>
      <% } %>
      
      <% if (columns.contains("type")) { %>
      	<td valign="top" class="content"><%=db.getStudyTypeName(study.getTypeId()) %></td> 
      <% } %>
      
      <% if (columns.contains("description")) { %>
	      <td valign="top" class="content">
	        <%
	            // Abbreviate desc before printing. Max. 3 lines and 200 characters.
	            String htmlDesc = study.getDesc().replaceAll("\\n", "<br>"); 
	            int pos = htmlDesc.indexOf("<br>");
	            if (pos != -1) {pos = htmlDesc.indexOf("<br>", pos + 1);/*System.out.println("Study.getShortDesc: second pos = " + pos);*/}
	            if (pos != -1) {pos = htmlDesc.indexOf("<br>", pos + 1);/*System.out.println("Study.getShortDesc: third pos = " + pos);*/}
	            if (pos != -1) {htmlDesc = htmlDesc.substring(0, pos - 3) + "...";}
	            htmlDesc = StringUtils.abbreviate(htmlDesc, 200);
	            out.println(htmlDesc);
	        %>
	      </td>
      <% } %>
      
      <% if (columns.contains("duration")) { %>
      	<td valign="top" class="content">
      		<%=StringUtils.defaultString(study.getDurationAsString()) %>
      		<%=StringUtils.defaultString(study.getUnit()) %>
      	</td>
      <% } %>
      
      <% if (columns.contains("start")) { %>
      	<td valign="top" class="content"><%=StringUtils.defaultString(study.getStartAsString(application.getInitParameter("DateFormat"))) %></td>
      <% } %>
      
      <% if (columns.contains("end")) { %>
      	<td valign="top" class="content"><%=study.getEndAsString(application.getInitParameter("DateFormat")) %></td>
      <% } %>
      
      <% if (columns.contains("publications")) { %>
	      <td valign="top" class="content">
	        <%
	            List publications = study.getPublications();
	            for (ListIterator k = publications.listIterator(); k.hasNext(); ) {
	                Publication p = (Publication) k.next();
	                out.println("<a target=\"_blank\" href=\"" + p.getUrl() + "\" title=\"" + p.getTitle() + "\">" + p.getShortTitle() + "</a>");
	                // add a line break if there's more coming
	                if (k.hasNext()) out.println("<br>");
	            }
	        %>
	      </td>
	  <% } %>
	   
	  <% if (columns.contains("keywords")) { %>
	  	<td valign="top" class="content"><%=StringUtils.defaultString(study.getKeywords()) %></td>
	  <% } %>
	  
      <% if (columns.contains("studentParticipants")) { %>
      <td valign="top" class="content"><%=StringUtils.defaultString(study.getNoOfStudentsAsString()) %></td>
      <% } %>
      
      <% if (columns.contains("proffesionalParticipants")) { %>
      <td valign="top" class="content"><%=StringUtils.defaultString(study.getNoOfProfessionalsAsString()) %></td>
      <% } %>
      
      <% if (columns.contains("material")) { %>
      	<td valign="top" class="content">
      		<%
			    List files = db.getFilesForStudy(study.getId());
			    for(ListIterator fileIter = files.listIterator(); fileIter.hasNext(); ){
			        FileInfo fi = (FileInfo) fileIter.next();
			%>
			        <a href="../../GetFile?file_id=<%=fi.getId()%>"><%=fi.getDesc()%></a> <br>
			<%
			    }
			%>
		</td>
      <% } %>
      
      <% if (columns.contains("notes")) { %>
      <td valign="top" class="content"><%=StringUtils.defaultString(study.getStudyNotes()) %></td>
      <% } %>
      
      <% if (columns.contains("owner")) { %>
      <td valign="top" class="content"><%=StringUtils.defaultString(study.getOwner()) %></td>
      <% } %>
      
      <% if (columns.contains("lastEdited")) { %>
      <td valign="top" class="content"><%=StringUtils.defaultString(study.getLastEditedBy()) %></td>
      <% } %>
    </tr>

    <%
    } // end for each study
    %>
    <tr></tr>

    </table>
    
	<form name="form" action="report_studies.jsp" method="POST">
		<div style="display: none;">
			<% for(Iterator iter = columns.iterator(); iter.hasNext(); ) { %>
				<input type="checkbox" checked="checked" name="columns_ids" value="<%=(String)iter.next()%>"/>
			<% } %>
			<% for(Iterator iter = study_responsibles.iterator(); iter.hasNext(); ) { %>
				<input type="checkbox" checked="checked" name="study_responsibles" value="<%=(String)iter.next()%>"/>
			<% } %>
		</div>
	
	    <input type="hidden" name="study_responsibles_or_type" value="<%=study_responsibles_or_type%>"/>
	    <input type="hidden" name="sub_sort_by_with_order" value="<%=sub_sort_by_with_order%>"/>
	    
	    <input type="hidden" name="start_record" value="<%=startRecord%>"/>
	    <input type="hidden" name="end_record" value="<%=endRecord%>"/>

	    <input type="hidden" name="report_action" value="">
	    
	    <input type="hidden" name="format" value="report_pdf"/>
	    
	    <% if (startRecord > 0){ %>
	    	<a href="#" onclick="document.form.action='report_studies.jsp'; document.form.report_action.value='prev_page'; document.form.submit(); return false;" >
	    		&lt; Previous</a> 
		<% }%>
		<% if (endRecord < studies.size() - 1){ %>
			<a href="#" onclick="document.form.action='report_studies.jsp'; document.form.report_action.value='next_page'; document.form.submit(); return false;" >
			Next &gt;</a>
		<% }%>
		<% if (startRecord == 0){ %>
			<br/><br/>
			<input class="input" title="Click to print report to PDF format (requires Adobe Acrobat Reader)" type="submit" value="Print report to PDF"
			onclick="document.form.action='../../Report.pdf'; document.form.report_action.value='report'; ">
		<% }%>
    </form>

    </p>

    </td>

    <td width="5%">
    &nbsp;
    </td>

    </tr>

    </table>
<%
} // end if there where hits
%>


<!-- Include standard footer -->
<br><br><br><br><br><br>
<jsp:include page="../footer.jsp" />
