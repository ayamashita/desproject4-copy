<!-- THIS IS THE STUDY SEARCH PAGE --->
<%@ page import="no.machina.simula.*, java.util.*, org.apache.commons.lang.*" %>
<%@ page buffer="64kb" %>
<!-- Include standard header -->

<%
User user = (User)request.getSession().getAttribute("simula_user");

boolean validated = true; // assume all is well

Set columns = new HashSet();
Set study_responsibles = new HashSet();
boolean study_responsibles_or_type = false; 
String sub_sort_by_with_order = "";

//Report maintenance params
String report_save_name = "";
String report_load_name = "";

if (request.getParameter("report_save_name") != null) {
	report_save_name = request.getParameter("report_save_name");
}
if (request.getParameter("report_load_name") != null) {
	report_load_name = request.getParameter("report_load_name");
}

String action = request.getParameter("report_action");

if (action != null && DB.isUserCanPerformAction(user, "MANAGE_REPORTS")){
	String[] columnsParam = request.getParameterValues("columns_ids");
	if (columnsParam != null) {
		for(int i = 0; i < columnsParam.length; ++i) {
			columns.add(columnsParam[i]);
		}
	}
	
	if (columns.size() == 0) {
		validated = false;
	}
	
	String[] studyResponsiblesParam = request.getParameterValues("study_responsibles");
	if (studyResponsiblesParam != null) {
		for(int i = 0; i < studyResponsiblesParam.length; ++i) {
			study_responsibles.add(studyResponsiblesParam[i]);
		}
	}
	
	study_responsibles_or_type = Boolean.TRUE.toString().equals(request.getParameter("study_responsibles_or_type"));
	
	String subSortByWithOrderParam = request.getParameter("sub_sort_by_with_order");
	if (subSortByWithOrderParam != null) {
		sub_sort_by_with_order = subSortByWithOrderParam;
	}
	
	if ("search".equals(action)) {
		// If input is valid, go to results page
	    if (validated == true){
	        RequestDispatcher dispatcher = application.getRequestDispatcher("/jsp/admin/report_studies.jsp");
	        dispatcher.forward(request, response);
	    }
	} else if ("load_report".equals(action)) {
		HashMap reportData = DB.getInstance().getReportDB().getReport(report_load_name, user.getUserId());
		
		columns = (Set)reportData.get("columns_ids");
		study_responsibles = (Set)reportData.get("study_responsibles");
		study_responsibles_or_type = Boolean.TRUE.equals(reportData.get("study_responsibles_or_type"));
		sub_sort_by_with_order = (String)reportData.get("sub_sort_by_with_order");
		
		validated = true;
		report_save_name = report_load_name;
	} else if ("delete_report".equals(action)) {
		DB.getInstance().getReportDB().deleteReport(report_load_name, user.getUserId());
		report_load_name = "";
	} else if ("save_report".equals(action)) {
		if (validated == true){
			HashMap reportData = new HashMap();
			reportData.put("columns_ids", columns);
			reportData.put("study_responsibles", study_responsibles);
			reportData.put("study_responsibles_or_type", new Boolean(study_responsibles_or_type));
			reportData.put("sub_sort_by_with_order", sub_sort_by_with_order);
			
			DB.getInstance().getReportDB().saveReport(report_save_name, user.getUserId(), reportData);
			report_load_name = report_save_name;
		}
	}
 
    
}
%>

<jsp:include page="../header.jsp" />

<!-- MAIN BODY -->

<% // Check security constraints. Is user authorized?
if ( !DB.isUserCanPerformAction(user, "MANAGE_REPORTS")) { 
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>

<p>
    No Access, please login
</p>

</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%  return;
    } else { // user is authorized %>


<tr>

<td width="15%">
&nbsp;
</td>

<td width="45%">



<p>
<span class="path">studies > report</span>

<!-- <p>Please enter your search criteria below (either freetext or by specifying Type, End and Responsibles), then click the 'Search' button to continue.</p> -->

<form action="<%=request.getContextPath()%>/jsp/admin/report_search.jsp" method="POST" name="form">

<h1>Report maintenance</h1>

<TABLE cellspacing="2" cellpadding="0"> 
		    <TR>
		    	<TD class="bodytext">Save name</TD>
		    	<TD class="bodytext">
		    		<INPUT type="text" name="report_save_name" size="40" maxlength="30" class="bodytext" value="<%=report_save_name %>">
		    	</TD>
		    	<TD>
		    		<INPUT type="submit" name="" value="Save" class="bodytext" 
		    			onclick="selectAllOptions(this.form['columns_ids']); selectAllOptions(this.form['study_responsibles']); document.form.report_action.value='save_report'">
		    	</TD>
		    </TR> 
		    <TR>
		    	<TD class="bodytext">Reports</TD>
		    	<TD class="bodytext">
		    		<select class="input" name="report_load_name">
		    			<option value="" <%=Util.isNull(report_load_name) ? "selected" : "" %>></option>
		    			<%
		    				Vector v = DB.getInstance().getReportDB().getPersonReports(user.getUserId());
		    				for(Iterator iter = v.iterator(); iter.hasNext();) {
		    					String reportName = (String)iter.next();
		    			%>
		    				<option value="<%=reportName %>" <%=reportName.equals(report_load_name) ? "selected" : "" %>><%=reportName %></option>
		    			<%
		    				}
		    			%>
		    		</select>
		    	</TD>
		    	<TD>
		    		<INPUT type="submit" name="" value="Load" class="bodytext" 
		    			onclick="document.form.report_action.value='load_report'">
		    		<INPUT type="submit" name="" value="Delete" class="bodytext" 
		    			onclick="selectAllOptions(this.form['columns_ids']); selectAllOptions(this.form['study_responsibles']); document.form.report_action.value='delete_report'">
		    	</TD>
		    </TR> 
		</TABLE>

<h1>Report configuration</h1>

<table width="100%">
<%
// Print error message if input did'nt validate
if (action != null && !validated){
%>
<tr><td colspan="2">Columns cannot be empty.<p></td></tr>
<%
} // end if not validated
%>

<tr>
  <td><span class="label2">Columns:</span></td>
  <td class="content2">
  	
  	<table width="100%" border="0">
		<tbody>
			<tr>
				<td width="47%">
					<span class="label2">Available</span><br/>
			        <select class="input" name="columns_ids_all" ondblclick="moveSelectedOptions(this.form['columns_ids_all'],this.form['columns_ids'],true)" style="width:100%" size="10" multiple>
		                <% 
		                String[] allColumns = new String[] {"name", "responsibles", "type", "description", "duration", "start", "end", "publications", 
		                								"keywords", "studentParticipants", "proffesionalParticipants", "material", "notes", "owner", "lastEdited"};
		                String[] allColumnsNames = new String[] {"Study name", "Study Responsibles", "Type of study", "Study Description", "Duration", "Start Of Study", 
		                			"End of study", "Publications", "Keywords", "Student Participants", "Professional Participants", "Study Material" , "Study Notes",
		                			"Study Owner", "Last Edited by"};
		                for (int i = 0; i < allColumns.length; ++i) {
		                	if (!columns.contains(allColumns[i])) {
		                %>
		                    <option value="<%=allColumns[i]%>"><%=allColumnsNames[i] %></option>
		                <%}}%>
			        </select>
        		</td>
        		<td class="content2" width="5%" valign="middle" align="center">
					<input type="button" onclick="moveSelectedOptions(this.form['columns_ids_all'],this.form['columns_ids'],true)" value="->"/>
					<br/>
					<br/>
					<input type="button" onclick="moveSelectedOptions(this.form['columns_ids'],this.form['columns_ids_all'],true)" value="<-"/>
				</td>
				<td width="47%">
					<span class="label2">Selected</span><br/>
					<select class="input" name="columns_ids" ondblclick="moveSelectedOptions(this.form['columns_ids'],this.form['columns_ids_all'],true)" style="width:100%" size="10" multiple>
		                <% 
		                for (int i = 0; i < allColumns.length; ++i) {
		                	if (columns.contains(allColumns[i])) {
		                %>
		                    <option value="<%=allColumns[i]%>"><%=allColumnsNames[i] %></option>
		                <%}}%>
			        </select>
				</td>
			</tr>
		</tbody>
	</table>	
  </td>
</tr>
<tr>
  <td><span class="label2">Study Responsibles:</span></td>
  <td class="content2">
  	
  	<table width="100%" border="0">
		<tbody>
			<tr>
				<td width="47%">
					<span class="label2">Available</span><br/>
			        <select class="input" name="study_responsibles_all" ondblclick="moveSelectedOptions(this.form['study_responsibles_all'],this.form['study_responsibles'],true)" style="width:100%" size="10" multiple>
		                <%
		                List allResponsibles = DB.getInstance().getAllResponsibles();
		                for (ListIterator i = allResponsibles.listIterator(); i.hasNext(); ){
                			Responsible resp = (Responsible) i.next();
                			if (!study_responsibles.contains(resp.getId())) {
		                %>
		                    <option value="<%=resp.getId()%>"><%=resp.getFamilyName()%>, <%=resp.getFirstName()%></option>
		                <%}}%>
			        </select>
        		</td>
        		<td class="content2" width="5%" valign="middle" align="center">
					<input type="button" onclick="moveSelectedOptions(this.form['study_responsibles_all'],this.form['study_responsibles'],true)" value="->"/>
					<br/>
					<br/>
					<input type="button" onclick="moveSelectedOptions(this.form['study_responsibles'],this.form['study_responsibles_all'],true)" value="<-"/>
				</td>
				<td width="47%">
					<span class="label2">Selected</span><br/>
					<select class="input" name="study_responsibles" ondblclick="moveSelectedOptions(this.form['study_responsibles'],this.form['study_responsibles_all'],true)" style="width:100%" size="10" multiple>
		                <% 
		                for (ListIterator i = allResponsibles.listIterator(); i.hasNext(); ){
                			Responsible resp = (Responsible) i.next();
                			if (study_responsibles.contains(resp.getId())) {
		                %>
		                    <option value="<%=resp.getId()%>"><%=resp.getFamilyName()%>, <%=resp.getFirstName()%></option>
		                <%}}%>
			        </select>
				</td>
			</tr>
		</tbody>
	</table>	
  </td>
</tr>
<tr>
    <td><span class="label2">Study Responsibles<br/> search type:</span></td>
    <td><span class="content2">
    	<select name="study_responsibles_or_type">
			<option value="false" <%=study_responsibles_or_type ? "" : "selected" %>>AND</option>
			<option value="true" <%=study_responsibles_or_type ? "selected" : "" %>>OR</option>
		</select>
    </td>
</tr> 
<tr>
    <td><span class="label2">Sort by time:</span></td>
    <td><span class="content2">
    	<select name="sub_sort_by_with_order">
			<option value="" <%=Util.isNull(sub_sort_by_with_order) ? "selected" : "" %>>No sort</option>
			<option value="study_end_date ASC" <%="study_end_date ASC".equals(sub_sort_by_with_order) ? "selected" : "" %>>ASC</option>
			<option value="study_end_date DESC" <%="study_end_date DESC".equals(sub_sort_by_with_order) ? "selected" : "" %>>DESC</option>
		</select>
    </td>
</tr> 
</table>
<br>
<input type="hidden" name="report_action" value="search">
<input class="input" type="submit" name="ok" value="Search >" onclick="selectAllOptions(this.form['columns_ids']); selectAllOptions(this.form['study_responsibles'])">
</form>


</td>

<td valign="top" width="15%">
<br><br><br>
<jsp:include page="../menu.jsp" />
</td>


<td width="10%">
&nbsp;
</td>

</tr>

</table>


<% } // end user is authorized %>
<!-- Include standard footer -->
<br><br>
<jsp:include page="../footer.jsp" />
