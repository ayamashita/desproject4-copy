<%@include file="../header.jsp" %>
<%@page import="org.apache.commons.httpclient.*"%>
<%@page import="org.apache.commons.httpclient.methods.*"%>
<%@page import="org.apache.commons.httpclient.protocol.*"%>
<%@page import="org.apache.commons.httpclient.cookie.*"%>
<%@page import="org.apache.commons.httpclient.contrib.ssl.*"%>
<%@page import="org.apache.commons.httpclient.contrib.utils.*"%>
<%@page import="no.machina.simula.*, java.util.*, java.sql.*"%>
<%@page buffer="16kb" %>
<%
    // Get status from the session object to check if user has already logged in
    //if ( true ) {   // Create a local scope for this file
    String status = (String)session.getAttribute("status");
    String cookieName = getServletContext().getInitParameter("php.cookie.name");
    String loginPage = getServletContext().getInitParameter("php.login.page");
    String statusPage = getServletContext().getInitParameter("php.status.page");
    if ( statusPage == null || statusPage.length() == 0 ) {
        statusPage = "/get_sessionstatus.php";
    }
    String loginHost = getServletContext().getInitParameter("php.login.host");
    if ( loginHost == null || loginHost.length() == 0 ) {
        loginHost = "localhost";
    }
    String loginProtocol = getServletContext().getInitParameter("php.login.protocol");
    if ( loginProtocol == null || loginProtocol.length() == 0 ) {
        loginProtocol = "https";
    }
    // Get any cookies sent and find the php session id
    javax.servlet.http.Cookie[] cookies = request.getCookies();
    String domain = null;
    String value = null;
    String path = null;
    int maxAge = -1;
    StringBuffer sb = new StringBuffer();
    int size = cookies != null ? cookies.length : 0;
    for ( int i = 0;i < size;i++) {
        String name = cookies[i].getName();
        sb.append(name);
        sb.append(",");
        if ( name != null && name.equals(cookieName)) {
            domain = cookies[i].getDomain();
            value = cookies[i].getValue();
            path = cookies[i].getPath();
            maxAge = cookies[i].getMaxAge();
        }
    }
    // Send a request to the status page to discover the clients status
    GetMethod httpget = null;
    String res = null;
    String error = null;
    String stack = null;
    // Variables holding the result of the check
    String userName = null;
    String userId = null;
    String userRight = null;
    try {
        //if ( status == null || status.equalsIgnoreCase("NOTOK")) {
            HttpClient httpclient = new HttpClient();
            Protocol myhttps = new Protocol(loginProtocol, new EasySSLProtocolSocketFactory(), 443);
            httpclient.getHostConfiguration().setHost(loginHost, 443, myhttps);
            httpclient.setConnectionTimeout(30000);
            httpclient.getState().setCookiePolicy(CookiePolicy.COMPATIBILITY);
            org.apache.commons.httpclient.Cookie phpCookie = new org.apache.commons.httpclient.Cookie(loginHost, cookieName, value);
            phpCookie.setPath("/");
            httpclient.getState().addCookie(phpCookie);
            httpget = new GetMethod(statusPage);
            int result = httpclient.executeMethod(httpget);
            res = httpget.getResponseBodyAsString();
            // Intepret the result, which should be NOTOK or OK;simula_username;simula_id;simula_right
            int semiIndex = res.indexOf(";");
            if ( semiIndex > 0 ) {
                status = res.substring(0, semiIndex);
                int oldSemiIndex = semiIndex+1;
                semiIndex = res.indexOf(";", semiIndex+1);
                userName = res.substring(oldSemiIndex, semiIndex);
                oldSemiIndex = semiIndex+1;
                semiIndex = res.indexOf(";", semiIndex+1);
                userId = res.substring(oldSemiIndex, semiIndex);
                oldSemiIndex = semiIndex+1;
                userRight = res.substring(oldSemiIndex);
            } else {
                status = res;
            }
        //}
    } catch(Exception ex) {
        error = ex.getClass().getName() + ": " + ex.getMessage();
    } finally {
        if ( httpget != null ) {
            httpget.releaseConnection();
        }
    }
    if ( status == null || status.length() == 0 || status.equalsIgnoreCase("NOTOK")) {
        session.setAttribute("status", "NOTOK");
        String redirectUrl = loginProtocol + "://" + loginHost + loginPage;
        //System.err.println("Redirecting to " + redirectUrl);
        //response.sendRedirect(redirectUrl);
        //response.flushBuffer();
    } else {
        session.setAttribute("status", "OK");
        session.setAttribute("simula_username", userName);
        session.setAttribute("simula_userid", userId);
        session.setAttribute("simula_userright", userRight);
    }

    String dbdriver = config.getServletContext().getInitParameter("DBDriver");
    String dburl = config.getServletContext().getInitParameter("DBURL");
    String dbuser = config.getServletContext().getInitParameter("DBUser");
    String dbpwd = config.getServletContext().getInitParameter("DBPassword");
    String security = config.getServletContext().getInitParameter("Security");

    List actions = (List)session.getAttribute("actionlist");
    String dberror = null;
   // }
%>
<% if ( true ) { %>
<html>
<head>
</head>
<body>

    <h3>Security</H3>
    security = <%=security%>

    <h3>PHP settings</H3>
    date = <%=new java.util.Date().toString() %><br>
    #cookies = <%=size%><br>
    found cookies = <%=sb.toString()%><br>

    domain = <%=domain%><br>
    name = <%=cookieName%><br>
    value = <%=value%><br>
    path = <%=path%><br><br>

    status= <%=status%><br>
    username= <%=userName%><br>
    userid= <%=userId%><br>
    userright= <%=userRight%><br><br>

    <h3>PHP test</h3>
    php.session.page = <%=loginProtocol + "://" + loginHost + statusPage%><br>
    error = <%=error%><br>
    result = <%=res%><br><br>


    <h3>Available actions for user</h3>
    <% int actsize = actions != null ? actions.size() : 0;
       for ( int i = 0;i < actsize;i++) {
            Action act = (Action)actions.get(i);
            out.write(act.getActionName());
            if ( i < actsize-1) {
                out.write(", ");
            }
       } %>

    <h3>Database settings and status</H3>
    dbdriver=<%=dbdriver%><br>
    dburl=<%=dburl%><br>
    dbuser=<%=dbuser%><br>
    dbpwd=<%=dbpwd%><br><br>

    <%
        try {
            Connection conn = DB.getInstance().getConnection();
            PreparedStatement ps = conn.prepareStatement("select * from tbl_user");
            ps.execute();
        } catch(Exception ex) {
            dberror = ex.getClass().getName() + ": " + ex.getMessage();
        }
    %>
    dbstatus = <%=(dberror != null && dberror.length() > 0 ) ? dberror : "OK"%>

</body>
</HTML>
<% } %>