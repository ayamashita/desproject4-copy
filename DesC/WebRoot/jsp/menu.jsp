<%@page import="no.machina.simula.*, java.util.*"%>
<%
    // First get some stats about the user
    User user = (User)request.getSession().getAttribute("simula_user");
%>

<table border="0" cellpadding="10">
<tr>
<td valign="top">
	
	<% if (DB.isUserCanPerformAction(user, "VIEW_MAIN")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/main.jsp">Admin main page</a></p>
	<% } %>
	
	<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/search.jsp">Search for studies</a></p>
	<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/statistics.jsp">Statistics</a></p>
	
	<% if (DB.isUserCanPerformAction(user, "CREATE_STUDY")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/study_edit.jsp">Create new study</a></p>
	<% } %>
	
	<% if (DB.isUserCanPerformAction(user, "MANAGE_REPORTS")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/report_search.jsp">Reports</a></p>
	<% } %>
	
	<% if (DB.isUserCanPerformAction(user, "EDIT_USER")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/edit_user.jsp">User privileges</a></p>
	<% } %>
	
	<% if (DB.isUserCanPerformAction(user, "MAINTAIN_DATA")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/dataAdmin_main.jsp">DB Admin</a></p>
	<% } %>

	<% if (user == null) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/login.jsp">Login</a></p>
	<% } else { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/logoff.jsp">Log off</a></p>
	<% } %>
</td>
</tr>
</table>