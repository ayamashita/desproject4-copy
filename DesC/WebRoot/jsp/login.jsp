<!-- THIS IS THE STUDY SEARCH PAGE --->
<%@ page import="no.machina.simula.*, java.util.*, org.apache.commons.lang.*" %>
<%@ page buffer="16kb" %>

<%
String errorMsg = "";

// Get parameters
String login = request.getParameter("login");
String password = request.getParameter("password");
String action = request.getParameter("action");


if (action != null && action.equals("login")){
   
    // Validate dates
    if(StringUtils.isBlank(login)){
        errorMsg += "Login must not be empty<br/>";
    }
    if(StringUtils.isBlank(password)){
        errorMsg += "Password must not be empty<br/>";
    }
    
    if (Util.isNull(errorMsg)) {
    	try {
	    	User user = DB.getInstance().getUser(login,password);
	    	if (user != null) {
		    	request.getSession(true).setAttribute("simula_user", user);
			    response.sendRedirect(request.getContextPath() + "/jsp/admin/main.jsp");
	    	} else {
	    		errorMsg += "Login or password not correct<br/>";
	    	}
    	} catch (UserException e) {
    		errorMsg += e.getMessage() + "<br/>";
    	}
    }
    
}
%>

<jsp:include page="header.jsp" />


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="45%">



<p>
<span class="path">login</span>

<form action="<%=request.getContextPath()%>/jsp/login.jsp" method="POST">
<table>
<%
// Print error message if input did'nt validate
if (action != null && action.equals("login") && !Util.isNull(errorMsg)){
%>
<tr><td colspan="2"><p><%=errorMsg%><p></td></tr>
<%
} // end if not validated
%>

<tr>
  <td><span class="label2">Login:</span></td>
  <td><input class="input" type="text" size="30" maxlength="25" name="login" value=""></td>
</tr>
<tr>
  <td><span class="label2">Password:</span></td>
  <td><input class="input" type="password" size="30" maxlength="25" name="password" value=""></td>
</tr> 
</table>
<br>
<input type="hidden" name="action" value="login">
<input class="input" type="submit" name="ok" value="Login">
</form>


</td>

<td valign="top" width="15%">
<br><br><br>
<jsp:include page="menu.jsp" />
</td>


<td width="10%">
&nbsp;
</td>

</tr>

</table>



<!-- Include standard footer -->
<br><br>
<jsp:include page="footer.jsp" />
