<?xml version="1.0" encoding="UTF-8" ?>
<!--
    Document   : StudyList.xsl
    Created on : 6. oktober 2003, 19:26
    Author     : cato
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    version="1.0">
    
    <xsl:output method="xml" 
              encoding="UTF-8"
              indent="yes"/>

    <xsl:attribute-set name="page-setings">
        <xsl:attribute name="page-width">210mm</xsl:attribute>
        <xsl:attribute name="page-height">297mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="body-margins">
        <xsl:attribute name="margin-top">3cm</xsl:attribute>
        <xsl:attribute name="margin-bottom">1.5cm</xsl:attribute>
        <xsl:attribute name="margin-left">1.0cm</xsl:attribute>
        <xsl:attribute name="margin-right">1.5cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="data_font">
        <xsl:attribute name="font-size">5pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-family">verdana,sans-serif</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="bold_font">
        <xsl:attribute name="font-size">5pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="font-family">verdana,sans-serif</xsl:attribute>
    </xsl:attribute-set>

    <!-- Matches the root element and initializes the fo document
    -->
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>

                <!-- layout for the first page -->
                <fo:simple-page-master master-name="first" xsl:use-attribute-sets="page-settings">
                    <fo:region-body xsl:use-attribute-sets="body-margins"/>
                    <fo:region-before extent="3cm" />
                    <fo:region-after extent="1.5cm" />
                </fo:simple-page-master>

                <fo:page-sequence-master>
                    <fo:single-page-master-reference master-reference="first"/>
                </fo:page-sequence-master>
            </fo:layout-master-set>

            <fo:page-sequence id="contract" master-name="first">
                <!-- Create the footer with company information -->
                <fo:static-content flow-name="xsl-region-before">
                    <!--fo:block-container height="1m" width="21cm" background-color="#FE7519"/-->
                    <fo:block-container height="5cm" width="21cm" background-color="#FE7519">
                        <fo:block>
                        <fo:table>
                            <fo:table-column column-width="5cm"/>
                            <fo:table-column column-width="16cm"/>
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block>
                                            <fo:external-graphic src="logo-part1.gif"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block>
                                            <fo:external-graphic src="logo-part2.gif"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                        </fo:block>
                    </fo:block-container>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <!-- 
        Matches the top study-list element and creates the basic table and headers
    -->
    <xsl:template match="study-list">
        <fo:table>
        	<xsl:for-each select="columns/column">
		        <fo:table-column>
		        	<xsl:attribute name="column-width">
		        		<xsl:value-of select="@column-width"/>
		        	</xsl:attribute>
		        </fo:table-column>
		      </xsl:for-each>
            <fo:table-column column-width="30mm"/>
            <fo:table-column column-width="20mm"/>
            <fo:table-column column-width="20mm"/>
            <fo:table-column column-width="35mm"/>
            <fo:table-column column-width="40mm"/>
            <fo:table-column column-width="40mm"/>

            <fo:table-header>
                <fo:table-row xsl:use-attribute-sets="bold_font" text-align="start">
                	<xsl:for-each select="columns/column">
				        <fo:table-cell>
	                        <fo:block><xsl:value-of select="@label"/></fo:block>
	                    </fo:table-cell>
				      </xsl:for-each>
                </fo:table-row>
            </fo:table-header>

            <fo:table-body>
                <xsl:apply-templates/>
            </fo:table-body>

        </fo:table>
    </xsl:template>

    <!-- 
         Matches the study element and creates adds one row for each study.
         Each addition study responsible and publication is added on separate lines
    -->
    <xsl:template match="study">
        <fo:table-row xsl:use-attribute-sets="data_font">
        	<xsl:if test="/study-list/columns/column[@name='name']">
        		<fo:table-cell>
	                <fo:block><xsl:value-of select="name"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='responsibles']">
	            <xsl:if test="count(responsible)>0">
	                <fo:table-cell>
	                	<xsl:for-each select="responsible">
	                		<fo:block><xsl:value-of select="concat(first-name,concat(' ',family-name))"/></fo:block>
	                    </xsl:for-each>
	                </fo:table-cell>
	            </xsl:if>
	            <xsl:if test="count(responsible)=0">
	                <fo:table-cell/>
	            </xsl:if>
	        </xsl:if>
	        <xsl:if test="/study-list/columns/column[@name='type']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="type"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='description']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="desc"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='duration']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="duration"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='start']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="start"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='end']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="end"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='publications']">
	            <xsl:if test="count(publication)>0">
	                <fo:table-cell>
	                    <xsl:for-each select="publication">
	                		<fo:block><xsl:value-of select="title"/></fo:block>
	                    </xsl:for-each>
	                </fo:table-cell>
	            </xsl:if>
	            <xsl:if test="count(publication)=0">
	                <fo:table-cell/>
	            </xsl:if>
	        </xsl:if>
	        <xsl:if test="/study-list/columns/column[@name='keywords']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="keywords"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='studentParticipants']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="no-students"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='proffesionalParticipants']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="no-profs"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='material']">
	            <xsl:if test="count(material)>0">
	                <fo:table-cell>
	                    <xsl:for-each select="material">
	                		<fo:block><xsl:value-of select="desc"/></fo:block>
	                    </xsl:for-each>
	                </fo:table-cell>
	            </xsl:if>
	            <xsl:if test="count(material)=0">
	                <fo:table-cell/>
	            </xsl:if>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='notes']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="notes"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='owner']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="owner"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
            <xsl:if test="/study-list/columns/column[@name='lastEdited']">
	            <fo:table-cell>
	                <fo:block><xsl:value-of select="last-edited-by"/></fo:block>
	            </fo:table-cell>
            </xsl:if>
        </fo:table-row>


    </xsl:template>

</xsl:stylesheet> 
