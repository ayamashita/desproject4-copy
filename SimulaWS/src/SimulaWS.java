import java.util.*;
/**
 * @author aiko
 *
 */
public interface SimulaWS {

	/**
	 * This service returns a list of all possible publications 
	 * stored in the Publications database at Simula Website  
	 *
	 * @return An Array of Objects representing a Publication
	*/
	public Object[] getPubicationList();

	
	/**
	 * This service returns a list of all possible publications 
	 * stored in the Publications database at Simula Website 
	 * that match to the author(s) criteria 
	 *
	 * @return An Array of Objects representing a Publication
	*/
	public Object[] getPublicationbyAuthor(String authorId);
	
	/**
	 * This service returns a list of all possible users 
	 * stored in the People database at Simula Website  
	 *
	 * @return An Array of Objects representing a user
	*/
	public Object[] getPeopleList();
	
	/**
	 * This service returns a specific publication  
	 * stored in the Publication database at Simula Website  
	 * @param publicationId A string representing the id of a publication
	 * normally composed by the username and first author's lastname a dot
	 * character and a number
	 * @return An Object representing the publication
	*/
	public HashMap getPublication(String publicationId);

	/**
	 * This service returns a specific person  
	 * stored in the People database at Simula Website  
	 * @param username A string representing the login name of a person
	 * @return An Object representing the person
	*/
	public HashMap getPerson(String username);
	

	/**
	 * This service authenticates a user from Simula Website  
	 * @param username A string representing the login name of a person's account
	 * @param password A string representing the password of the person's account
	 * @return A boolean indicating that the user is an authenticated user
	*/
	public Boolean login(String username, String password);
	
	/**
	 * This service returns a boolean if the user is still authenticated
	 * in the system  
	 * @param username A string representing the id of a person
	 * @return A boolean of false indicating that user's session has expired and true if it haven't expired yet
	*/
	public Boolean isLogged(String username);

}
