package com.tec.exercise;

import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.xmlrpc.XmlRpcException;

import com.tec.exercise.Test.SQLListener;
import com.tec.ws.SimulaWSClient;

/**
 * Very similar class to Test class.
 * This one uses des_test data base (instead of des_a) (created from /resources/*.sql script).
 * This code also takes People and Publication info from Web Service instead of DB.
 * 
 * Class uses copied SimulaWSClient class from SimulaWS project
 * @author marcin.staszczyk
 *
 */
public class WSTest {
	
	/**
	 * Only difference to function from Test class is name of DB
	 */
	public static Object runSQL(String query, SQLListener listener) {
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/des_test";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");
			Statement s = conn.createStatement();
			s.executeQuery(query);
			ResultSet rs = s.getResultSet();

			try {
				return listener.runOnQueryResult(rs);
			} finally {
				rs.close();
				s.close();
			}
		} catch (Exception e) {
			System.err.println("Cannot connect to database server");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
					System.out.println("Database connection terminated");
				} catch (Exception e) { /*ignore close errors*/
				}
			}
		}
		return null;
	}

	/**
	 * Exercise a.
	 * Loads studies of people (given in name as parameter). Then for every such study loads publications and for every publication olads data from Web Service
	 * @param peopleName
	 */
	public static List exerciseA(String peopleName) throws MalformedURLException {
		final SimulaWSClient wsCLient = new SimulaWSClient();
		wsCLient.configure("http://simula.no:9080/simula/xmlrpcdes");
		
		return (List)runSQL(
				"SELECT s.id, s._name FROM study_peopleresponsible sp, study s WHERE sp.people_name = '" + peopleName + "' and sp.study_id = s.id; ", 
			
				new SQLListener() {
		
					public Object runOnQueryResult(ResultSet rs) throws SQLException {
						List results = new ArrayList();
						int count = 0;
						while (rs.next()) {
							String study_name = rs.getString("_name");
							int study_id = rs.getInt("id");
							results.add(study_name);
							++count;
							
							System.out.println("Study: " + study_name);
							runSQL(
								"SELECT p.publication_code FROM publication_study p WHERE p.study_id = " + study_id,
								new SQLListener() {
									public Object runOnQueryResult(ResultSet subrs) throws SQLException {
										while (subrs.next()) {
											String pubCode = subrs.getString("publication_code");
											System.out.println(wsCLient.getPublication(pubCode));
										}
										return null;
									}
								}
							);
						}
						
						
						
						return results;
					}
					
				}
			);
	}
	
	
	public static void exerciseB() {
		//As in NON WEB SERVICE exercise
		
		Test.runSQL("SELECT st.study_type type, count(*) c FROM study s, study_type st WHERE s.study_type = st.study_type_id GROUP BY s.study_type;", 
			
		new SQLListener() {

			public Object runOnQueryResult(ResultSet rs) throws SQLException {
				int count = 0;
				while (rs.next()) {
					String type = rs.getString("type");
					int countType = rs.getInt("c");
					System.out.println("type = " + type + " ; count = " + countType);
					++count;
				}
				
				
				
				return null;
			}
			
		});
	}
	
	/**
	 * Exercise c.
	 * Function uses search available in Web Service to load People for every of provided emails.
	 */
	public static void exerciseC(List emails) throws MalformedURLException, XmlRpcException {
		final SimulaWSClient client = new SimulaWSClient();
		client.configure("http://simula.no:9080/simula/xmlrpcdes");
		
		
		for(int j = 0; j < emails.size(); ++j) {
			HashMap hash = new HashMap();
			hash.put("email", emails.get(j));
			
			Object[] methodParams = new Object[]{ hash };
			Object resultSet = client.rcpClient.execute("getPeople", methodParams);
			Object[] elementList = (Object[])resultSet;
			if (elementList.length > 0) {
				HashMap element = (HashMap)elementList[0];
				Object[] keyList = element.keySet().toArray();
		    	for(int i = 0; i< keyList.length; i++)
		    		System.out.print(keyList[i]+ ":" + element.get(keyList[i]).toString() + ", ");
		    	System.out.println();
			}
		}
		
	}
	
	/**
	 * Exercise d.
	 * Function uses sort_on parameter in hash table to get all people sorted by job title;
	 */
	public static void exerciseD() throws MalformedURLException, XmlRpcException {
		
		
		final SimulaWSClient client = new SimulaWSClient();
		client.configure("http://simula.no:9080/simula/xmlrpcdes");
		
		HashMap hash = new HashMap();
		hash.put("sort_on", "jobtitle");
		
		Object[] methodParams = new Object[]{ hash };
		Object resultSet = client.rcpClient.execute("getPeople", methodParams);
		Object[] elementList = (Object[])resultSet;
		
		for(int j = 0; j < elementList.length; ++j) {
			HashMap element = (HashMap)elementList[j];
			Object[] keyList = element.keySet().toArray();
			System.out.print("Job title: " + element.get("jobtitle") + "    ");
	    	for(int i = 0; i< keyList.length; i++)
	    		System.out.print(keyList[i]+ ":" + element.get(keyList[i]).toString() + ", ");
	    	System.out.println();
		}
	}

	public static void main(String [] args) throws MalformedURLException, XmlRpcException {
//		exerciseA("Aiko");
		
//		exerciseB();
		
//		List emails = new ArrayList();
//		emails.add("svenar@simula.no");
//		emails.add("ariel@simula.no");
//		emails.add("kaspar@simula.no");
//		exerciseC(emails);
		
		exerciseD();
	}
}
