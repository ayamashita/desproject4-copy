package com.tec.exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Class containing results for exercises done with original DB
 * @author marcin.staszczyk
 *
 */
public class Test {

	/**
	 * Example function taken from exercise document
	 */
	public static void testDBConnection() {
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/des_a";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");
			Statement s = conn.createStatement();
			s.executeQuery("SELECT * FROM people_study");
			ResultSet rs = s.getResultSet();
			int count = 0;
			while (rs.next()) {
				int idVal = rs.getInt("study_id");
				int idPeople = rs.getInt("people_id");
				System.out.println("id = " + idVal + ", name = " + idPeople);
				++count;
			}
			rs.close();
			s.close();
		} catch (Exception e) {
			System.err.println("Cannot connect to database server");
		} finally {
			if (conn != null) {
				try {
					conn.close();
					System.out.println("Database connection terminated");
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}
	}
	
	/**
	 * Modified function from above which instead of having fixed query and results interpreting code uses provided parameters
	 * @param query - query which will be run against des_a database
	 * @param listener - object containing code which will be used to interpret results of query.
	 * @return data interpreted by listener
	 */
	public static Object runSQL(String query, SQLListener listener) {
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/des_a";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");
			Statement s = conn.createStatement();
			s.executeQuery(query);
			ResultSet rs = s.getResultSet();

			try {
				return listener.runOnQueryResult(rs);
			} finally {
				rs.close();
				s.close();
			}
		} catch (Exception e) {
			System.err.println("Cannot connect to database server");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
					System.out.println("Database connection terminated");
				} catch (Exception e) { /*ignore close errors*/
				}
			}
		}
		return null;
	}
	
	/**
	 * Abstract class with code to be run against results of query
	 * @author marcin.staszczyk
	 *
	 */
	public static abstract class SQLListener {
		public abstract Object runOnQueryResult(ResultSet results) throws SQLException;
	}
	
	/**
	 * Function for exercise a 
	 * @param people_id - parameter required in task
	 */
	public static void exerciseA(int people_id) {
		runSQL("SELECT pub.publication_title FROM people_study ps, publication_study sp, publication pub " +
				"where ps.people_id = " + people_id + " and sp.study_id = ps.study_id and pub.publication_id = sp.publication_id", 
			
		new SQLListener() {

			public Object runOnQueryResult(ResultSet rs) throws SQLException {
				int count = 0;
				while (rs.next()) {
					String publication_title = rs.getString("publication_title");
					System.out.println("title = " + publication_title);
					++count;
				}
				
				
				
				return null;
			}
			
		});
	}
	
	/**
	 * Function for exercise b
	 */
	public static void exerciseB() {
		runSQL("SELECT st.study_type type, count(*) c FROM study s, study_type st WHERE s.study_type = st.study_type_id GROUP BY s.study_type;", 
			
		new SQLListener() {

			public Object runOnQueryResult(ResultSet rs) throws SQLException {
				int count = 0;
				while (rs.next()) {
					String type = rs.getString("type");
					int countType = rs.getInt("c");
					System.out.println("type = " + type + " ; count = " + countType);
					++count;
				}
				
				
				
				return null;
			}
			
		});
	}
	
	/**
	 * Function for exercise c
	 */
	public static void exerciseC(List emails) {
		String query = "SELECT p.people_first_name name, p.people_family_name surname " +
				"FROM people p where people_email in (";
		for(int i = 0; i < emails.size(); ++i) {
			if (i > 0) {
				query += ", ";
			}
			query+= "'" + emails.get(i) + "'";
		}
		query  += ")";
		runSQL(query, 
			
		new SQLListener() {

			public Object runOnQueryResult(ResultSet rs) throws SQLException {
				int count = 0;
				while (rs.next()) {
					String name = rs.getString("name");
					String surname = rs.getString("surname");
					System.out.println("Name = " + name + " " + surname);
					++count;
				}
				
				
				
				return null;
			}
			
		});
	}
	
	/**
	 * Function for exercise d
	 */
	public static void exerciseD() {
		runSQL("select p.people_first_name name, p.people_family_name surname, p.people_position job " +
				"from people p order by p.people_position", 
		new SQLListener() {

			public Object runOnQueryResult(ResultSet rs) throws SQLException {
				int count = 0;
				while (rs.next()) {
					String name = rs.getString("name");
					String surname = rs.getString("surname");
					String job = rs.getString("job");
					System.out.println("Name = " + name + " " + surname + " ; job = " + job);
					++count;
				}
				
				
				
				return null;
			}
			
		});
	}

	/**
	 * Main function to be run.
	 * Uncomment proper lines to run exercises. 
	 */
	public static void main(String [] args) {
//		testDBConnection();
		
//		exerciseA(10);
		
//		exerciseB();
		
//		List emails = new ArrayList();
//		emails.add("svenar@simula.no");
//		emails.add("ariel@simula.no");
//		exerciseC(emails);
		
		exerciseD();
	}
}
