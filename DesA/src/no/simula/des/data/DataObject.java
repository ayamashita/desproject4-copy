/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data;

import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;


/**
 * Abstract data object class that handles database connectivity
 */
public class DataObject {
    private static final String DB_JNDI = "java:comp/env/jdbc/desdb";
    public static boolean IS_TEST = false;
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);
    private Connection conn = null;

    /**
     * Retrieves a database connection to the DES database from the Servlet Engine's
     * connection pool
     *
     * @return the connection object
     * @throws Exception
     */
    public Connection getConnection() throws Exception {
        Context ctx;

        try {
            if (IS_TEST) {
                conn = getTestConnection();

                return conn;
            }

            ctx = new InitialContext();

            if (ctx == null) {
                throw new Exception("No Context found");
            }

            DataSource ds = (DataSource) ctx.lookup(DB_JNDI);

            if (ds != null) {
                conn = ds.getConnection();
            }

            return conn;
        } catch (Exception e) {
            log.error(e);

            //e.printStackTrace();
            throw new Exception(e.toString());
        }
    }

    /**
     * Closes an open connection and (if any) the open result set
     * and its statement object
     *
     * @param rs the result set
     * @return true if the action was successful
     */
    public boolean closeConnection(ResultSet rs) {
        try {
            if (rs != null) {
                Statement stmt = rs.getStatement();

                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                } else {
                    rs.close();
                    rs = null;
                }
            }

            if (conn != null) {
                if (!conn.isClosed()) {
                    //System.out.println("Closing connection");
                    log.debug("Closing connection");
                    conn.close();
                    conn = null;
                }
            }

            return true;
        } catch (SQLException sqle) {
            log.error("Error closing database connection", sqle);

            return false;
        }
    }

    //-------------- Private helper methods -------------------

    /**
     * Used to open a connection when run outside a container
     */
    private Connection getTestConnection() throws Exception {
        Class.forName("com.mysql.jdbc.Driver").newInstance();

        Connection conn = DriverManager.getConnection(
                "jdbc:mysql://80.239.13.114/des?user=desuser&password=desuser");

        return conn;
    }
}
