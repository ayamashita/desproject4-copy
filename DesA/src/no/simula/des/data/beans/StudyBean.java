/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data.beans;

import no.simula.des.util.Constants;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
/**
 * Represents a single study
 *
 */
public class StudyBean {
    //Mandatory attributes
    private int id = 0;
    private String name = null;
    private String type = null;
    private String description = null;
    private Date endDate = null;

    //Optional attributes
    private int duration =-1;
    private int durationUnit;

    //TODO make sure duration unit is properly included
    private String keywords = null;
    private String notes = null;
    private int students = -1;
    private int professionals = -1;
    private Date startDate = null;
    private Collection publications;
    private Collection responsibles;
    private ArrayList studyTypeOptions;
    private ArrayList durationUnitOptions;
    private ArrayList studyMaterial = new ArrayList();

    //Auto-generated attributes
    private Date createdDate = null;
    private Date editedDate = null;
    private String createdBy = null;
    private String editedBy = null;
    private int editorId = 0;

    // ------------- Getter and setter methods ---------------

    /**
     * @return
     */
    public String getDescription() {
        return description;
    }

    public String getShortDescription() {
        int length = description.length();

        return ((length > 90) ? description.substring(0, 90) : description);
    }

    /**
     * @return
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * @param string
     */
    public void setDescription(String string) {
        description = string;
    }

    /**
     * @param string
     */
    public void setEndDate(Date date) {
        endDate = date;
    }

    /**
     * @param i
     */
    public void setId(int i) {
        id = i;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @param i
     */
    public void setType(String string) {
        type = string;
    }

    /**
     * @return
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @return
     */
    public int getDurationUnit() {
        return durationUnit;
    }

    /**
     * @return
     */
    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateAsString(){
      if(startDate==null){
        return "";
      }
      SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US );
       return formatter.format(this.startDate);
    }

    /**
     * @param i
     */
    public void setDuration(int i) {
        duration = i;
    }

    /**
     * @param i
     */
    public void setDurationUnit(int i) {
        durationUnit = i;
    }

    /**
     * @param date
     */
    public void setStartDate(Date date) {
        startDate = date;
    }

    /**
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @return
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedDateAsString(){
       SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US );
       return formatter.format(this.createdDate);
    }

    /**
     * @return
     */
    public String getEditedBy() {
        return editedBy;
    }

    /**
     * @return
     */
    public Date getEditedDate() {
        return editedDate;
    }

    public String getEditedDateAsString(){
      SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US );
       return formatter.format(this.editedDate);
    }

    /**
     * @return
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @return
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @return
     */
    public int getProfessionals() {
        return professionals;
    }

    /**
     * @return
     */
    public int getStudents() {
        return students;
    }

    /**
     * @param string
     */
    public void setCreatedBy(String string) {
        createdBy = string;
    }

    /**
     * @param date
     */
    public void setCreatedDate(Date date) {
        createdDate = date;
    }

    /**
     * @param string
     */
    public void setEditedBy(String string) {
        editedBy = string;
    }

    /**
     * @param date
     */
    public void setEditedDate(Date date) {
        editedDate = date;
    }

    /**
     * @param string
     */
    public void setKeywords(String string) {
        keywords = string;
    }

    /**
     * @param string
     */
    public void setNotes(String string) {
        notes = string;
    }

    /**
     * @param i
     */
    public void setProfessionals(int i) {
        professionals = i;
    }

    /**
     * @param i
     */
    public void setStudents(int i) {
        students = i;
    }

    public String toString() {
        return "id: " + id + ", name: " + name + ", type: " + type +
        ", description: " + description + ", endDate: " + endDate;
    }

    public String getEndDateAsString() {
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US );

        return formatter.format(this.endDate);
    }

    /**
     * @return
     */
    public ArrayList getDurationUnitOptions() {
        return durationUnitOptions;
    }

    /**
     * @return
     */
    public ArrayList getStudyTypeOptions() {
        return studyTypeOptions;
    }

    /**
     * @param list
     */
    public void setDurationUnitOptions(ArrayList list) {
        durationUnitOptions = list;
    }

    /**
     * @param list
     */
    public void setStudyTypeOptions(ArrayList list) {
        studyTypeOptions = list;
    }

    /**
     * @return
     */
    public int getEditorId() {
        return editorId;
    }

    /**
     * @param i
     */
    public void setEditorId(int i) {
        editorId = i;
    }

    /**
     * @return
     */
    public Collection getPublications() {
        return publications;
    }

    /**
     * @return
     */
    public Collection getResponsibles() {
        return responsibles;
    }

    /**
     * @param list
     */
    public void setPublications(Collection list) {
        publications = list;
    }

    /**
     * @param list
     */
    public void setResponsibles(Collection list) {
        responsibles = list;
    }
  public ArrayList getStudyMaterial() {
    return studyMaterial;
  }
  public void setStudyMaterial(ArrayList studyMaterial) {
    this.studyMaterial = studyMaterial;
  }

}
