package no.simula.des.data.beans;


/**
 * Represents a single study material element
 *
 */
public class StudyMaterialBean {
    private int study_id;
    private int study_material_id =-1;
    private String description;
    private boolean isUrl = true;
    private String url = null;
    private byte[] file = null;
    private String filename;
    private String contenttype;

      //Utility attributes
    private boolean isAdded;

    //Utility attributes
    private boolean isDeleted;


    public StudyMaterialBean() {
    }

    public String getDescription() {
        return description;
    }

    public byte[] getFile() {
        return file;
    }

    public boolean getIsUrl() {
        return isUrl;
    }

    public int getStudy_id() {
        return study_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setStudy_id(int study_id) {
        this.study_id = study_id;
    }

    public void setIsUrl(boolean isUrl) {
        this.isUrl = isUrl;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContenttype() {
        return contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public int getStudy_material_id() {
        return study_material_id;
    }

    public void setStudy_material_id(int study_material_id) {
        this.study_material_id = study_material_id;
    }

     public boolean getAdded() {
        return isAdded;
    }

    /**
     * @return
     */
    public boolean getDeleted() {
        return isDeleted;
    }

    /**
     * @param b
     */
    public void setAdded(boolean b) {
        isAdded = b;
    }

    /**
     * @param b
     */
    public void setDeleted(boolean b) {
        isDeleted = b;
    }
}
