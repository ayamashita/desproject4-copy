package no.simula.des.data.beans;

import java.util.HashMap;


/**
 * Represents the admin privilege level for a single person
 */
public class AdminPrivilegesBean {
    public static int DBA_PRIVILEGE = 2;
    public static int SA_PRIVILEGE = 1;
    public static int GUEST_PRIVILEGE = 0;
    private HashMap privileges = new HashMap();

    public void setAdminPrivileges(int people_id, int privilege) {
        privileges.put(new Integer(people_id), new Integer(privilege));
    }

    public int getAdminPrivileges(int people_id) {
        Object tmp = privileges.get(new Integer(people_id));

        if (tmp == null) {
            return 0;
        }

        return ((Integer) tmp).intValue();
    }
}
