/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.beans.NameValueBean;
import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.PublicationDatabase;
import no.simula.des.data.StudyDatabase;
import no.simula.des.data.StudyMaterialDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PublicationBean;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.data.beans.StudyMaterialBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Assembles a Comma Separated Value (CSV) representation of a set of studies. The user
 * will be asked where to store the file
 */
public class DownloadCSVAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        log.debug("DownloadCSCAction called");

        StudyDatabase studyDb = new StudyDatabase();
        ArrayList studies = (ArrayList) studyDb.getAllStudies();

        response.setContentType("application/vnd.ms-excel");
        response.addHeader("content-disposition",
            "attachment; filename=\"studies.csv\"");

        ServletOutputStream out = response.getOutputStream();
        String content = createContent(studies);
        out.print(content);
        out.flush();
        out.close();

        return null;
    }

    public String createContent(ArrayList studies) throws Exception {
        StudyDatabase studyDb = new StudyDatabase();
        PeopleDatabase peopleDb = new PeopleDatabase();
        PublicationDatabase publicationDb = new PublicationDatabase();
        StudyMaterialDatabase matarialDb = new StudyMaterialDatabase();
        ArrayList studyTypes = studyDb.getStudyTypes();

        ArrayList durationUnits = studyDb.getDurationUnits();

        StringBuffer content = new StringBuffer();
        Iterator it = studies.iterator();
        content.append("Study Id;");
        content.append("Study Name;");
        content.append("Created Date;");
        content.append("Created by;");
        content.append("Edited Date;");
        content.append("Edited By;");
        content.append("Keywords;");
        content.append("Description;");
        content.append("Study notes;");
        content.append("Start date;");
        content.append("End date;");
        content.append("Duration;");
        content.append("Duration unit;");
        content.append("Study type;");
        content.append("No. of students;");
        content.append("No. of professionals;");
        content.append("Responsibles;");
        content.append("Publications;");
        content.append("StudyMaterial\n ");

        while (it.hasNext()) {
            StudyBean study = (StudyBean) it.next();

            if (study.getId() == 0) {
                continue;
            }

            content.append(study.getId());
            content.append(";");
            content.append("\"" + study.getName().replace('\"', ' ').replace('\n', ' ').replace('\r', ' ') + "\"");
            content.append(";");
            content.append(study.getCreatedDateAsString());
            content.append(";");
            content.append(study.getCreatedBy());
            content.append(";");
            content.append(study.getEditedDateAsString());
            content.append(";");
            content.append(study.getEditedBy());
            content.append(";");
            content.append("\"" +
                study.getKeywords().replace('\"', ' ').replace('\n', ' ')
                     .replace('\r', ' ') + "\"");
            content.append(";");
            content.append("\"" +
                study.getDescription().replace('\"', ' ').replace('\n', ' ')
                     .replace('\r', ' ') + "\"");
            content.append(";");
            content.append("\"" +
                study.getNotes().replace('\"', ' ').replace('\n', ' ').replace('\r',
                    ' ') + "\"");
            content.append(";");
            content.append(study.getStartDateAsString());
            content.append(";");
            content.append(study.getEndDateAsString());
            content.append(";");
            content.append( (study.getDuration()==-1? 0:study.getDuration() ));
            content.append(";");

            Iterator it2 = durationUnits.iterator();

            while (it2.hasNext()) {
                NameValueBean nvb = (NameValueBean) it2.next();

                if (Integer.parseInt(nvb.getValue()) == study.getDurationUnit()) {
                    content.append(nvb.getName());

                    break;
                }
            }

            content.append(";");
            content.append(study.getType());
            content.append(";");
            content.append( (study.getStudents()==-1? 0:study.getStudents() ) );
            content.append(";");
            content.append( (study.getProfessionals()==-1? 0:study.getProfessionals()) );
            content.append(";");

            Collection col = peopleDb.getResponsibleList(study.getId());

            if (col != null) {
                it2 = col.iterator();

                int i = 0;

                while (it2.hasNext()) {
                    PeopleBean people = (PeopleBean) it2.next();

                    if (i > 0) {
                        content.append(":");
                    }

                    content.append(people.getFirst_name() + " " +
                        people.getFamily_name());
                    i++;
                }
            }

            //content.append( study.getResponsibles() );
            content.append(";");
            col = publicationDb.getPublicationList(study.getId());

            if (col != null) {
                it2 = col.iterator();

                int i = 0;
                content.append("\"");

                while (it2.hasNext()) {
                    PublicationBean pub = (PublicationBean) it2.next();

                    if (i > 0) {
                        content.append(":");
                    }

                    content.append(pub.getTitle().replace('\"', ' ')
                                      .replace('\n', ' ').replace('\r', ' '));
                    i++;
                }

                content.append("\"");
            }

            content.append(";");
            col = matarialDb.getStudyMaterialInfo(study.getId());

            if (col != null) {
                int i = 0;
                it2 = col.iterator();

                while (it2.hasNext()) {
                    StudyMaterialBean material = (StudyMaterialBean) it2.next();

                    if (i > 0) {
                        content.append(":");
                    }

                    content.append(material.getDescription());
                    content.append(" ");
                    content.append((material.getIsUrl() ? "URL: "+ material.getUrl()
                                                        : "File: " + material.getFilename()));
                    content.append(" ");
                    i++;
                }
            }

            content.append("\n");
        }

        return content.toString();
    }
}
