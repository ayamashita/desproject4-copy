/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import java.util.ArrayList;

import no.simula.des.data.StudyMaterialDatabase;
import no.simula.des.data.beans.StudyMaterialBean;
import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.struts.forms.StudyMaterialForm;

import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * When a file is attached to a study material entry, this action takes care
 * of storing the file itself along with its metadata in the database
 */
public class StudyMaterialAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyMaterialForm stdmForm = (StudyMaterialForm) form;
        StudyForm studyForm = (StudyForm)request.getSession().getAttribute("studyForm");

        log.debug("stdmForm Upload: " + stdmForm.getUpload());
        log.debug("stdmForm study_id: " + stdmForm.getStudy_id());


        if ((stdmForm.getUpload() != null) && !stdmForm.getUpload().equals("")) {
            ActionErrors errors = stdmForm.validateInAction(mapping, request);

            if (errors == null) {
                saveStudyMaterial(stdmForm,studyForm);

                ActionMessages messages = new ActionMessages();
                messages.add("addmaterial",
                    new ActionMessage(
                        "no.simula.des.studtmaterial.addMaterial", "testname"));
                this.saveMessages(request, messages);
                stdmForm.reset(mapping, request);
            } else {
                this.saveErrors(request, errors);
            }

            //log.debug("stdmForm: Calling reset" );
        }

        return (mapping.findForward("success"));
    }

    public void saveStudyMaterial(StudyMaterialForm stdmForm, StudyForm studyForm)
        throws Exception {
        StudyMaterialBean stmb = new StudyMaterialBean();
        stmb.setStudy_id(stdmForm.getStudy_id());
        stmb.setDescription(stdmForm.getDescription());
        stmb.setIsUrl(stdmForm.getIsUrl());


        if (stdmForm.getIsUrl()) {
            stmb.setUrl(stdmForm.getUrl());
        } else {
            stmb.setContenttype(stdmForm.getFile().getContentType());
            stmb.setFile(stdmForm.getFile().getFileData());
            stmb.setFilename(stdmForm.getFile().getFileName());
        }

        stmb.setAdded( true );
        ((ArrayList)studyForm.getStudyMaterial()).add(stmb);
        /*
        StudyMaterialDatabase smDao = new StudyMaterialDatabase();
        boolean isOk = smDao.insertStudyMateriel(stmb);
        */
        //return isOk;
    }
}
