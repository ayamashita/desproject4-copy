/*
 * Created on 01.okt.2003
 *
 *
 */
package no.simula.des.struts.forms;

import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import java.net.MalformedURLException;
import java.net.URL;


/**
 * ActionForms are placeholders for data that are displayed in or collected from a page in a
 * Struts based solution, whether the page actually contains a HTML form or not.
 *
 * This form bean contains all study material related data. This may include a file.
 */
public class StudyMaterialForm extends ActionForm {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);
    private String filename;
    private String description;
    private boolean isUrl;
    private String url;
    private FormFile file;
    private String upload;
    private int study_id;

    public void reset(ActionMapping mapping,
        javax.servlet.http.HttpServletRequest request) {
        filename = null;
        description = null;
        isUrl = false;
        file = null;
        upload = null;
        url = null;

        return;
    }

    public String toString() {
        return "";
    }

    public String getDescription() {
        return description;
    }

    public String getFilename() {
        return filename;
    }

    public boolean getIsUrl() {
        return isUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setIsUrl(boolean isUrl) {
        this.isUrl = isUrl;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FormFile getFile() {
        return file;
    }

    public void setFile(FormFile file) {
        this.file = file;
    }

    public String getUpload() {
        return upload;
    }

    public void setUpload(String upload) {
        this.upload = upload;
    }

    public int getStudy_id() {
        return study_id;
    }

    public void setStudy_id(int study_id) {
        this.study_id = study_id;
    }

    public ActionErrors validateInAction(ActionMapping mapping,
        javax.servlet.http.HttpServletRequest request) {
        System.out.println("Validating " + getUpload());

        if ((getUpload() == null) || getUpload().equals("")) {
            return null;
        }

        ActionErrors errors = new ActionErrors();

        if ((getDescription() == null) || this.getDescription().equals("")) {
            errors = new ActionErrors();
            errors.add("description",
                new ActionError("error.des.studyMaterial.descriptionRequired",
                    "Description requiered"));
        }

        if (this.getIsUrl()) {
            if (this.getUrl().equals("")) {
                errors.add("url",
                    new ActionError("error.des.studyMaterial.urlRequired",
                        "URL requiered"));
            } else {
                try {
                    URL url = new URL(this.getUrl());
                } catch (MalformedURLException mue) {
                    errors.add("malformedURL",
                        new ActionError(
                            "error.des.studyMaterial.malformedUrl",
                            "Malformed URL"));
                }
            }
        } else if (this.getFile() != null) {
            if (getFile().getFileSize() == 0) {
                log.debug("the file is not uploadeble");
                errors.add("file",
                    new ActionError("error.des.studyMaterial.fileRequired",
                        "File requiered"));
            }
        }

        log.debug("Antall feil" + errors.size());

        int errorCount = errors.size();

        if (errorCount > 0) {
            return errors;
        }

        return null;
    }
}
