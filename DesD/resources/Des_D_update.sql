ALTER TABLE temp_people_study modify people_id varchar(50) NOT NULL default '';

ALTER TABLE people_study modify people_id varchar(50) NOT NULL default '';


ALTER TABLE temp_publication_study modify publication_id varchar(200) NOT NULL default '';

ALTER TABLE publication_study modify publication_id varchar(200) NOT NULL default '';


ALTER TABLE people_role modify people_id varchar(50) NOT NULL default '';
Insert into people_role values ('aiko', 1);

DELETE FROM people_study;
DELETE FROM temp_people_study;
DELETE FROM publication_study;
DELETE FROM temp_publication_study;

DROP TABLE people;
DROP TABLE publication;

CREATE TABLE `people_report` (
  `people_id` varchar(50) NOT NULL default '',
  `report_name` varchar(50) NOT NULL default '',
  `responsibles_or_type` boolean NOT NULL default false,
  `subsort` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`people_id`,`report_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;

CREATE TABLE `people_report_column` (
  `people_id` varchar(50) NOT NULL default '',
  `report_name` varchar(50) NOT NULL default '',
  `column_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`people_id`,`report_name`, `column_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;

CREATE TABLE `people_report_responsible` (
  `people_id` varchar(50) NOT NULL default '',
  `report_name` varchar(50) NOT NULL default '',
  `responsible_id` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`people_id`,`report_name`, `responsible_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;