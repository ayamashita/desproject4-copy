package com.tec.des.command;
import java.util.Vector;
import com.tec.des.dao.PeopleDAO;
import com.tec.des.dao.ReportDAO;

/**
 * Command for fetching all people.
 * The result of the command is a Vector containing all people.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class GetPersonReportsCommand extends Command  {
    
	private String loggedPersonId;
	
    private Vector reports;

/**
 * Constructs a new Command object.
 */
    public GetPersonReportsCommand() {
    }
    
    public void setLoggedPersonId(String loggedPersonId) {
		this.loggedPersonId = loggedPersonId;
	}

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return loggedPersonId != null && super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    	ReportDAO dao = new ReportDAO();
    	try {
            reports = dao.getPersonReports(loggedPersonId);
        } finally {
            dao.close();
        }
    }

    public Vector getResult() {
        return reports;
    }

}
