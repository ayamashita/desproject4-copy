package com.tec.des.command;
import java.io.ByteArrayOutputStream;

import javax.servlet.ServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Driver;
import org.w3c.dom.Document;

import com.tec.des.pdf.TableRenderer;
import com.tec.shared.util.HitList;

/**
 * Command for fetching a file containin information about all studies.
 * The result of the command is File containing the information.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class PDFExportStudiesCommand extends Command  {
    
	private static final String TRANSFORMER_STUDIES = "fo-studies.xsl";
	
    private ServletResponse response;
    private HitList studies;
    private String[] columns;
    
/**
 * Constructs a new Command object.
 */
    public PDFExportStudiesCommand() {
    }
    
    public void setResponse(ServletResponse response) {
		this.response = response;
	}
    
    public void setStudies(HitList studies) {
		this.studies = studies;
	}
    
    public void setColumns(String[] columns) {
		this.columns = columns;
	}

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return response != null && studies != null && columns != null && super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    	TransformerFactory transformerFactory = TransformerFactory.newInstance();
    	
		response.getOutputStream();

		Document source = new TableRenderer().getXML(studies, columns);
		String transformerSource = TRANSFORMER_STUDIES;

		// Debug to console
		//	      Transformer transformer = transformerFactory.newTransformer();
		//	      transformer.transform(new DOMSource(source), new StreamResult(System.err));

		// Setup FOP
		Driver driver = new Driver();
		//driver.setLogger(fopLog);
		driver.setRenderer(Driver.RENDER_PDF);

		// Setup a buffer to obtain the content length
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		driver.setOutputStream(out);

		// Setup Transformer
		ClassLoader cl = PDFExportStudiesCommand.class.getClassLoader();
		Source xsltSrc = new StreamSource(cl.getResourceAsStream(transformerSource));
		Transformer transformer = transformerFactory.newTransformer(xsltSrc);

		// Make sure the XSL transformation's result is piped through to FOP
		Result result = new SAXResult(driver.getContentHandler());

		// Setup input
		Source dataSource = new DOMSource(source);

		// Start the transformation and rendering process
		transformer.transform(dataSource, result);

		// Prepare response
		response.setContentType("application/pdf");
		response.setContentLength(out.size());

		// Send content to Browser
		response.getOutputStream().write(out.toByteArray());
		response.getOutputStream().flush();

		out.close();
	}
    
    public Object getResult() {
        return new Object();
    }


}