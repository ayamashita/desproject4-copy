package com.tec.des.command;
import com.tec.des.dao.ReportDAO;
import com.tec.des.dto.SearchDTO;

/**
 * Command for fetching all people.
 * The result of the command is a Vector containing all people.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class SavePersonReportCommand extends Command  {
    
	private String reportName;
	private String loggedPersonId;
	private SearchDTO search;
	
/**
 * Constructs a new Command object.
 */
    public SavePersonReportCommand() {
    }
    
    public void setLoggedPersonId(String loggedPersonId) {
		this.loggedPersonId = loggedPersonId;
	}
    
    public void setReportName(String reportName) {
		this.reportName = reportName;
	}
    
    public void setSearch(SearchDTO search) {
		this.search = search;
	}

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return reportName != null && loggedPersonId != null && search != null && super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    	ReportDAO dao = new ReportDAO();
    	try {
            dao.saveReport(reportName, loggedPersonId, search);
        } finally {
            dao.close();
        }
    }

}
