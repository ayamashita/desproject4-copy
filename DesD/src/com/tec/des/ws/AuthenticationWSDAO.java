package com.tec.des.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import com.tec.des.util.Constants;
import com.tec.shared.exceptions.UserException;

public class AuthenticationWSDAO {
	
	private String simulaWebURL = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE).getString(Constants.AUTHETICATION_WEB_SERVICE_NAME);
	
	private XmlRpcClientConfigImpl rpcConfig;
	public XmlRpcClient rcpClient;
	
	public AuthenticationWSDAO()
	{
		this.rpcConfig = new XmlRpcClientConfigImpl();
		this.rcpClient = new XmlRpcClient();
		
		try {
			this.rpcConfig.setServerURL(new URL(simulaWebURL));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		this.rcpClient.setConfig(rpcConfig);
	}
	
	public Boolean getUser(String username, String password) throws UserException {
		try {
			HashMap hash = new HashMap(); /* When hash is empty, there is no filtering which means: retrive all the publications */
			hash.put("login", username);
			hash.put("password", password); 
			
			Object[] methodParams = new Object[]{ hash }; /* The hash must always be contained in a Object array according to XmlRcp convention */
	    	return (Boolean)this.rcpClient.execute("check_credentials", methodParams);
		}
		catch (XmlRpcException xmlException)
		{
			throw new UserException(xmlException.getMessage());
		}
	}
}
