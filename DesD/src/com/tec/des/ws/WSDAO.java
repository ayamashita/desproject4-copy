package com.tec.des.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import com.tec.des.util.Constants;
import com.tec.des.util.Validator;
import com.tec.shared.util.Nuller;

public class WSDAO {
	
	private String simulaWebURL = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE).getString(Constants.SIMULAWEB_WEB_SERVICE_NAME);
	
	private XmlRpcClientConfigImpl rpcConfig;
	public XmlRpcClient rcpClient;
	
	public WSDAO()
	{
		this.rpcConfig = new XmlRpcClientConfigImpl();
		this.rcpClient = new XmlRpcClient();
		
		try {
			this.rpcConfig.setServerURL(new URL(simulaWebURL));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		this.rcpClient.setConfig(rpcConfig);
	}
	
	/*
	 * This method retrieves all the people from the Simula website*/		
	public Object[] getPeopleList() {
		HashMap hashMap = new HashMap();
		hashMap.put("sort_on", "lastname");
		return getPeople(hashMap);
	}

	public HashMap getPerson(String username) {
		HashMap hash = new HashMap(); 
		hash.put("id", username); /* Keyword 'id' is used when searching criteria is based on username */
		
		return getPerson(hash);
	}
	
	public Object[] findPeople(String somethingLike) {
		if (Nuller.isReallyNull(somethingLike)) {
			return getPeopleList();
		}
		
		String[] searchCriteria = Validator.checkEscapes(somethingLike).split(" ");
		
		List result = new ArrayList();
		Object[] allPeople = getPeopleList();
		
		for (int i = 0; i < allPeople.length; ++i) {
			boolean match = true;
			HashMap people = (HashMap)allPeople[i];
			for (int j = 0; j < searchCriteria.length; j++) {
				String criterium = searchCriteria[j];
				if (!(like(people, "is", criterium) || like(people, "firstname", criterium) || like(people, "lastname", criterium))) {
					match = false;
					break;
				}
			}
			if (match) {
				result.add(people);
			}
		}
		
		
		return result.toArray(new Object[0]);
	}
	
	private boolean like(HashMap data, String key, String criterium) {
		String string = ((String)data.get(key));
		if (string == null) {
			return false;
		}
		return string.toUpperCase().indexOf(criterium.toUpperCase()) >= 0;
	}
	
	public HashMap getPerson(HashMap parameters) {
		Object[] results = getPeople(parameters);
		if (results.length > 0) {
			return (HashMap)results[0];
		} else {
			/* If exception encounted, the default should be an empty HashMap*/
			//return new HashMap();
			/* Maybe should but this is the worst thing that can be done as there is no way to find if publication even exists in th system - Marcin */
			return null;
		}
	}
	
	public Object[] getPeople(HashMap parameters) {
		return getData(parameters, "getPeople");
	}

	/*
	 * This method retrieves all the available publications from the Simula website*/		
	public Object[] getPubicationList() {
		return getPublications(new HashMap());
	}

	public HashMap getPublication(String publicationId) {
		HashMap hash = new HashMap(); /* When hash is empty, there is no filtering which means: retrive all the publications */
		hash.put("id", publicationId); /* Keyword 'id' is used when searching criteria is based on publication id */
		
		return getPublication(hash);
	}

	public Object[] getPublicationbyAuthor(String authorId) {
		HashMap hash = new HashMap(); /* When hash is empty, there is no filtering which means: retrive all the publications */
		hash.put("user", authorId); /* Keyword 'authors' is used when searching criteria is based on author's any part of their name */
		
		return getPublications(hash);
	}
	
	public Object[] findPublications(String somethingLike) {
		if (Nuller.isReallyNull(somethingLike)) {
			return getPubicationList();
		}
		
		String[] searchCriteria = Validator.checkEscapes(somethingLike).split(" ");
		
		List result = new ArrayList();
		Object[] allPublications = getPubicationList();
		
		for (int i = 0; i < allPublications.length; ++i) {
			boolean match = true;
			HashMap publication = (HashMap)allPublications[i];
			for (int j = 0; j < searchCriteria.length; j++) {
				String criterion = searchCriteria[j];
				if (!(
						like(publication, "title", criterion) || 
						like(publication, "source", criterion) || 
						like(publication, "abstract", criterion)
					)) {
					
					//Checking if criterion can be found in author
					boolean authorsMatch = false;
					Object[] authors = (Object[])publication.get("authors");
					if (authors != null) {
						for (int k = 0; k < authors.length; ++k) {
							HashMap author = (HashMap)authors[k];
							if (like(author, "firstname", criterion) || like(author, "middlename", criterion) || like(author, "lastname", criterion)) {
								authorsMatch = true;
								break;
							}
						}
					}
					if (!authorsMatch) {
						match = false;
						break;
					}
				}
			}
			if (match) {
				result.add(publication);
			}
		}
		
		
		return result.toArray(new Object[0]);
	}
	
	public HashMap getPublication(HashMap parameters) {
		Object[] results = getPublications(parameters);
		if (results.length > 0) {
			return (HashMap)results[0];
		} else {
			/* If exception encounted, the default should be an empty HashMap*/
			//return new HashMap();
			/* Maybe should but this is the worst thing that can be done as there is no way to find if publication even exists in th system - Marcin */
			return null;
		}
	}
	
	public Object[] getPublications(HashMap parameters) {
		return getData(parameters, "getPublications");
	}
	
	public Object[] getData(HashMap parameters, String method) {
		try {
			Object[] methodParams = new Object[]{ parameters }; /* The hash must always be contained in a Object array according to XmlRcp convention */
	    	Object resultSet = this.rcpClient.execute(method, methodParams);
	    	Object[] elementList = (Object[])resultSet; /* Convert */
	    	return elementList;
		}
		catch (XmlRpcException xmlException)
		{
			/* If exception encounted, the default should be an empty array */
			return new Object[]{};	
		}
	}
	
	public Set remoteSingleWordFindIds(String singleWordLike, String[] againstTable, String method) {
		Set results = new HashSet();
		HashMap hash = new HashMap();
		
		for(int i = 0; i < againstTable.length; ++i) {
			String against = againstTable[i];
			hash.clear();
			hash.put(against, singleWordLike);
			Object[] data = getData(hash, method);
			for(int j = 0; j < data.length; ++j) {
				results.add(((HashMap)data[j]).get("id"));
			}
		}
		
		return results;
	}
	
	/**
	 * @param somethingLike cannot be empty
	 */
	public Set remoteFindIds(String somethingLike, String[] againstTable, String method) {
		Set results = null;
		
		String[] words = somethingLike.split(" ");
		for (int i = 0; i < words.length; ++i) {
			Set tmp = remoteSingleWordFindIds(words[i], againstTable, method);
			if (i == 0) {
				results = tmp;
			} else {
				results.retainAll(tmp);
			}
		}
		
		return results;
	}
	
	public Set remoteFindPeopleIdsByNameAndSurname(String somethingLike) {
		return remoteFindIds(somethingLike, new String[] {"firstname", "lastname"}, "getPeople");
	}
	
	public Set remoteFindPublicationsIdsByTitle(String somethingLike) {
		return remoteFindIds(somethingLike, new String[] {"title"}, "getPublications");
	}
}
