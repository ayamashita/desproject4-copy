package com.tec.des.dto;
import com.tec.shared.util.Nuller;
import com.tec.shared.exceptions.UserException;

/**
 * Data transfer object for a search.
 * This object encapsulates all search criterias.
 *
 * @author  Norunn Haug Christensen
 */
public class SearchDTO implements DTO {

	private boolean advancedSearch = Boolean.FALSE.booleanValue();
    private String typeOfStudy = Nuller.getStringNull();   
    private String freeText = Nuller.getStringNull();
    private String endOfStudyFrom = Nuller.getStringNull();
    private String endOfStudyTo = Nuller.getStringNull();
    private String studyResponsibles = Nuller.getStringNull();
    private String[] studyResponsiblesArray = Nuller.getStringArrayNull();
    private boolean studyResponsiblesArrsyOrType = Boolean.FALSE.booleanValue();
    private String sortBy = Nuller.getStringNull();
    private String sortOrder = Nuller.getStringNull();
    private String subSortByWithOrder = Nuller.getStringNull();
    private String[] columns = Nuller.getStringArrayNull();

    public SearchDTO() {
        super();
    }
    
    public boolean isAdvancedSearch() {
		return advancedSearch;
	}
    
    public void setAdvancedSearch(boolean advancedSearch) {
		this.advancedSearch = advancedSearch;
	}
    
    public void setTypeOfStudy(String typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }
    
    public String getTypeOfStudy() {
        return typeOfStudy;
    }
    
    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public String getFreeText() {
        return freeText;
    }
    
    public void setEndOfStudyFrom(String endOfStudyFrom) {
        this.endOfStudyFrom = endOfStudyFrom;
    }

    public String getEndOfStudyFrom() {
        return endOfStudyFrom;
    }


    public void setEndOfStudyTo(String endOfStudyTo) {
        this.endOfStudyTo = endOfStudyTo;
    }

    public String getEndOfStudyTo() {
        return endOfStudyTo;
    }
    
    public void setStudyResponsibles(String studyResponsibles) {
        this.studyResponsibles = studyResponsibles;
    }
    
    public String getStudyResponsibles() {
        return studyResponsibles;
    }
    
    public void setStudyResponsiblesArray(String[] studyResponsiblesArray) {
        this.studyResponsiblesArray = studyResponsiblesArray;
    }
    
    public String[] getStudyResponsiblesArray() {
        return studyResponsiblesArray;
    }
    
    public void setStudyResponsiblesArrsyOrType(
			boolean studyResponsiblesArrsyOrType) {
		this.studyResponsiblesArrsyOrType = studyResponsiblesArrsyOrType;
	}
    
    public boolean isStudyResponsiblesArrsyOrType() {
		return studyResponsiblesArrsyOrType;
	}
    
    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    public String getSortOrder() { 
        return sortOrder;
    } 

    public void setColumns(String[] columns) {
		this.columns = columns;
	}
    
    public String getSubSortByWithOrder() {
		return subSortByWithOrder;
	}
    
    public void setSubSortByWithOrder(String subSortByAndOrder) {
		this.subSortByWithOrder = subSortByAndOrder;
	}
    
    public String[] getColumns() {
		return columns;
	}
    
    public boolean displayColumn(String name) {
    	if (columns == null || columns.length == 0) {
    		return true;
    	}
    	for(int i = 0; i < columns.length; ++i) {
    		if (columns[i].equals(name)) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    
}
