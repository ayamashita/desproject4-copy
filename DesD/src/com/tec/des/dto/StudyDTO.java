package com.tec.des.dto;
import java.util.Vector;
import com.tec.shared.util.Nuller;

/**
 * Data transfer object for a study.
 *
 * @author  Norunn Haug Christensen
 */
public class StudyDTO extends StudySDTO {

    private String startDate = Nuller.getStringNull();
    private String duration = Nuller.getStringNull();
    private int typeId = Nuller.getIntNull(); 
    private int durationUnitId = Nuller.getIntNull();
    private String durationUnit = Nuller.getStringNull();
    private String keywords = Nuller.getStringNull();
    private String noOfStudentParticipants = Nuller.getStringNull();
    private String noOfProfessionalParticipants = Nuller.getStringNull();
    private Vector material;
    private String notes = Nuller.getStringNull();
    private String owner = Nuller.getStringNull();
    private String lastEditedBy = Nuller.getStringNull();


    public StudyDTO() {
        super();
    }
    
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
    
    public String getDuration() {
        return duration;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnitId(int durationUnitId) {
        this.durationUnitId = durationUnitId;
    }
    
    public int getDurationUnitId() {
        return durationUnitId;
    }
    
    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
    
    public int getTypeId() {
        return typeId;
    }
    
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
    
    public String getKeywords() {
        return keywords;
    }

    public void setNoOfStudentParticipants(String noOfStudentParticipants) {
        this.noOfStudentParticipants = noOfStudentParticipants;
    }

    public String getNoOfStudentParticipants() {
        return noOfStudentParticipants;
    }

    public void setNoOfProfessionalParticipants(String noOfProfessionalParticipants) {
        this.noOfProfessionalParticipants = noOfProfessionalParticipants;
    }
    
    public String getNoOfProfessionalParticipants() {
        return noOfProfessionalParticipants;
    }

    public void setMaterial(Vector material) {
        this.material = material;
    }
    
    public Vector getMaterial() {
        if (material == null)
            material = new Vector();
        return material;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    
    public String getNotes() {
        return notes;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    
    public String getOwner() {
        return owner;
    }

    public void setLastEditedBy(String lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public String getLastEditedBy() {
        return lastEditedBy;
    }

}
