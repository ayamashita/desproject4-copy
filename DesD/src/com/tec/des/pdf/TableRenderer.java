package com.tec.des.pdf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.tec.des.dto.StudyDTO;
import com.tec.des.dto.StudyMaterialDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.util.Constants;
import com.tec.shared.util.HitList;

/** This class renders <CODE>Table</CODE>s in different formats. Currently supports
 * HTML and XML. Should consider refactoring this into an interface with two
 * different implementations, one for HTML and one for XML.
 * @author Stian Eide
 */
public class TableRenderer {
	
	ResourceBundle bundle = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE);

	/** The package and name of the Struts massage resource file */
	public static final String RESOURCE = "resources/application.properties";

	/** XML-element name for a table */
	public static final String TABLE = "table";
	/** XML-element name for a collection of columns */
	public static final String COLUMNS = "columns";
	/** XML-element name for a column */
	public static final String COLUMN = "column";
	/** XML-attribute name for a column name */
	public static final String COLUMN_NAME = "name";
	/** XML-attribute name for a column name */
	public static final String COLUMN_WIDTH = "column-width";
	/** XML-element name for a collection of rows */
	public static final String ROWS = "rows";
	/** XML-element name for a row */
	public static final String ROW = "row";

	/** XML-element name for a list of items */
	public static final String LIST = "ul";
	/** XML-element name for a list item */
	public static final String LIST_ITEM = "li";

	//private Properties resource;

	/** Returns this table and its content as an XML Dom Document.
	 * @param table the <CODE>Table</CODE> to be rendered
	 * @throws TableRenderException if the table cannot be rendered
	 * @return a Dom Document representing the table
	 */
	public Document getXML(HitList table, String[] columns) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document xml = builder.newDocument();
			Element tableElement = xml.createElement(TABLE);

			Element columnsEL = xml.createElement(COLUMNS);
			for (int i = 0; i < columns.length; ++i) {
				Element columnElement = xml.createElement(COLUMN);
				columnElement.setAttribute(COLUMN_NAME, columns[i]);
				columnElement.setAttribute(COLUMN_WIDTH, bundle.getString(columns[i] + "_size"));
				//columnElement.appendChild(xml.createTextNode(bundle.getString(columns[i])));
				buildXMLForText(xml, columnElement, bundle.getString(columns[i]));
				columnsEL.appendChild(columnElement);
			}
			tableElement.appendChild(columnsEL);

			Element rows = xml.createElement(ROWS);
			for (Iterator iter = table.iterator(); iter.hasNext();) {
				StudyDTO rowContent = (StudyDTO)iter.next();

				Element rowElement = xml.createElement(ROW);
				for (int i = 0; i < columns.length; ++i) {
					Element columnElement = xml.createElement(COLUMN);
					columnElement.setAttribute(COLUMN_NAME, columns[i]);
					Object cellValue = ((StudyDTOAccess)access.get(columns[i])).getData(rowContent);
					if (cellValue instanceof List) {
						Element list = xml.createElement(LIST);
						for (Iterator k = ((List) cellValue).iterator(); k.hasNext();) {
							Element item = xml.createElement(LIST_ITEM);
							//item.appendChild(xml.createTextNode(k.next().toString()));
							buildXMLForText(xml, item, k.next().toString());
							list.appendChild(item);
						}
						columnElement.appendChild(list);
					} else {
						if (cellValue != null) {
							//columnElement.appendChild(xml.createTextNode(cellValue.toString()));
							buildXMLForText(xml, columnElement, cellValue.toString());
						}

					}
					rowElement.appendChild(columnElement);
				}
				rows.appendChild(rowElement);
			}
			tableElement.appendChild(rows);
			xml.appendChild(tableElement);
			return (xml);
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();//TableRenderException("Failed to render table as XML.");
		}
	}
	
	private void buildXMLForText(Document xml, Element item, String text) {
		StringBuffer result = new StringBuffer();
//		for(int i = 1; i < text.length(); ++i) {
//			char prev = text.charAt(i-1);
//			char next = text.charAt(i);
//			result.append(prev);
//			if (prev != ' ' && next != ' ') {
//				result.append('\u200B');
//			}
//		}
//		if (text.length() > 0) {
//			result.append(text.charAt(text.length() - 1));
//		}
		
		String[] words = text.split(" ");
		for(int i = 0; i < words.length; ++i) {
			String word = words[i];
			int wordLength = word.length();
			for(int j = 0; j < wordLength; j += 5) {
				result.append(word.substring(j, Math.min(j + 5, wordLength)));
				if (j + 5 < wordLength) {
					result.append('\u200B');
				}
			}
			
			result.append(' ');
		}
		
		item.appendChild(xml.createTextNode(result.toString()));
	}

	private interface StudyDTOAccess {
		public Object getData(StudyDTO study);
	}
	
	static private class NameAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getName();
		}
	}
	static private class ResponsiblesAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			List list = new ArrayList();
			Iterator iter = study.getResponsibles().values().iterator();
			while (iter.hasNext()) {
				list.add(((String)iter.next()).split(WebConstants.DATA_URL_SEPARATOR)[0]);
			}
			return list;
		}
	}
	static private class TypeAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getType();
		}
	}
	static private class DescriptonAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getDescription();
		}
	}
	static private class DurationAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getDuration() + " " + study.getDurationUnit();
		}
	}
	static private class StartAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getStartDate();
		}
	}
	static private class EndAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getEndDate();
		}
	}
	static private class PublicationsAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			List list = new ArrayList();
			Iterator iter = study.getPublications().values().iterator();
			while (iter.hasNext()) {
				list.add(((String)iter.next()).split(WebConstants.DATA_URL_SEPARATOR)[0]);
			}
			return list;
		}
	}
	static private class KeywordsAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getKeywords();
		}
	}
	static private class StudentParticipantsAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getNoOfStudentParticipants();
		}
	}
	static private class ProffesionalParticipantsAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getNoOfProfessionalParticipants();
		}
	}
	static private class MaterialAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			List list = new ArrayList();
			Iterator iter = study.getMaterial().iterator();
			while (iter.hasNext()) {
				list.add(((StudyMaterialDTO)iter.next()).getDescription());
			}
			return list;
		}
	}
	static private class NotesAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getNotes();
		}
	}
	static private class OwnerAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getOwner();
		}
	}
	static private class LastEditedAccess implements StudyDTOAccess {
		public Object getData(StudyDTO study) {
			return study.getLastEditedBy();
		}
	}
	
	static Map access = new HashMap();
	static {
		access.put("name", new NameAccess());
		access.put("responsibles", new ResponsiblesAccess());
		access.put("type", new TypeAccess());
		access.put("description", new DescriptonAccess());
		access.put("duration", new DurationAccess());
		access.put("start", new StartAccess());
		access.put("end", new EndAccess());
		access.put("publications", new PublicationsAccess());
		access.put("keywords", new KeywordsAccess());
		access.put("studentParticipants", new StudentParticipantsAccess());
		access.put("proffesionalParticipants", new ProffesionalParticipantsAccess());
		access.put("material", new MaterialAccess());
		access.put("notes", new NotesAccess());
		access.put("owner", new OwnerAccess());
		access.put("lastEdited", new LastEditedAccess());
		
	}
}