package com.tec.des.taglib;
import java.util.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import com.tec.des.dto.StudyResponsibleDTO;

/**
 * Multi select JSP tag library, used to select study responsibles.
 *
 * @author Norunn Haug Christensen
 */
public class StudyResponsibleSelectTag extends TagSupport 
{
  private String name = "multiselect1";
  private Vector items;
  private String[] value;
  private String attributes = "";
  private String boxsize = "";
  
  public int doStartTag () throws JspException {
    try {

      if (items != null) {
          StringBuffer htmlOutput = new StringBuffer();
          htmlOutput.append("<select multiple name=\"" + name + "\" " + attributes + " size=\"" + boxsize + "\" >");
          String displayvalue;
          String optionvalue;
          StudyResponsibleDTO responsible;
          if (value != null) {
        	  for(int i = 0; i < value.length; ++i) {
        		  for(int j = 0; j < items.size(); ++ j) {
        			  responsible = (StudyResponsibleDTO)items.get(j);
        			  if (responsible.getId().equals(value[i])) {
        				  displayvalue = responsible.getListName();
        		          optionvalue = responsible.getId();
        		          htmlOutput.append("<option selected value=\"" + optionvalue + "\">" + displayvalue + "</option>");
        		          items.remove(j);
        		          break;
        			  }
        		  }
        		  
        	  }
          }
          
          Iterator it = items.iterator();
          while (it.hasNext()) {
            responsible = (StudyResponsibleDTO)it.next();
            displayvalue = responsible.getListName();
            optionvalue = responsible.getId();
            htmlOutput.append("<option value =\"" + optionvalue + "\">" + displayvalue + "</option>");
          }
          htmlOutput.append("</select>");
          pageContext.getOut().print(htmlOutput.toString());
          
      }
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setName(String name) {
      this.name = name; 
  }

  public void setItems(Vector items) {
    this.items = items;
  }

  public void setValue(String[] value) {
    this.value = value;
  }

  public void setAttributes(String attributes) {
    this.attributes = attributes;
  }
  
  public void setBoxsize(String boxsize) {
    this.boxsize = boxsize;
  }
}