package com.tec.des.taglib;
import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import com.tec.des.dto.SearchDTO;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

/**
 * This class is a tag creating hidden input parameters for search criterias.
 * They are needed to 'remember' the search if a hitlist has more counts than 
 * the hitlist length and the user uses the navigation links to show another 
 * part of the hitlist, and for viewing the hitlist in a printerfriendly version.
 *
 * @author Norunn Haug Christensen
 */
public class SearchCriteriaTag extends TagSupport {
    
    private SearchDTO searchCriteria;
    
/**
 * Method called when the SearchCriteria tag is called.
 *
 * @throws JspException When an exception occurs.
 */
  public int doEndTag() throws JspException {
  try {

    if (searchCriteria != null) {
        StringBuffer outputString = new StringBuffer();
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_TEXT +"\" value=\""+ skipNull(searchCriteria.getFreeText()) +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_TYPE_OF_STUDY +"\" value=\""+ skipNull(searchCriteria.getTypeOfStudy()) +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM +"\" value=\""+ skipNull(searchCriteria.getEndOfStudyFrom()) +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO +"\" value=\""+ skipNull(searchCriteria.getEndOfStudyTo()) +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES +"\" value=\""+ skipNull(searchCriteria.getStudyResponsibles()) +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_SORT_BY +"\" value=\""+ skipNull(searchCriteria.getSortBy()) +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING +"\" value=\""+ skipNull(searchCriteria.getSortOrder()) +"\">");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_SUB_SORT_BY_WITH_ORDER +"\" value=\""+ skipNull(searchCriteria.getSubSortByWithOrder()) +"\">");
        
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES_ARRAY_TYPE +"\" value=\""+ searchCriteria.isStudyResponsiblesArrsyOrType() +"\">");

        outputString.append("<DIV style=\"display: none\">");
        
        String[] studyResponsibles = searchCriteria.getStudyResponsiblesArray();
        if (!Nuller.isNull(studyResponsibles)) {
        	for(int i = 0; i < studyResponsibles.length; ++i) {
            	outputString.append("<INPUT type=\"checkbox\" checked name=\""+ WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES_ARRAY +"\" value=\""+ studyResponsibles[i] +"\">");
            }
        }
        
        String[] columns = searchCriteria.getColumns();
        if (!Nuller.isNull(columns)) {
        	for(int i = 0; i < columns.length; ++i) {
        		outputString.append("<INPUT type=\"checkbox\" checked name=\""+ WebKeys.REQUEST_PARAM_SEARCH_COLUMNS_TO_DISPLAY +"\" value=\""+ columns[i] +"\">");
        	}
        }

        outputString.append("</DIV>");

        pageContext.getOut().println(outputString);
      } 
    
  } catch (Exception e) {
      throw new JspException(e.getMessage());
  }
    return SKIP_BODY;
  }

  public void setSearchCriteria(SearchDTO searchCriteria) {
      this.searchCriteria = searchCriteria;
  }
  
  private final String skipNull(String value) {
	  if (value == null) {
		  return "";
	  }
	  return value;
  }
  
}
