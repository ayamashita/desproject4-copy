package com.tec.des.taglib;
import java.util.Hashtable;
import java.util.Iterator;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;

import com.tec.des.http.WebConstants;

/**
 * JSP tag library displaying one or more links. The displayed link is a 
 * concationation of the input parameter url and a key of the input parameter
 * items. The value of the items hastable is used as the name of the link.
 * The tag is used to display links to persons and publications in simulaweb.
 *
 * @author Norunn Haug Christensen 
 */
public class LinkTag extends TagSupport {
  private String url;
  private Hashtable items;
  private String separator = "<BR>";
  private boolean printLink = true;

/**
 * Method called when the LinkTag tag is invoked.
 *
 * @throws JspException If an exception occurs.
 */
  public int doStartTag() throws JspException {
    try {

        if (items != null) {
            StringBuffer htmlOutput = new StringBuffer();
            Iterator it = items.keySet().iterator();
            while (it.hasNext()) {
                String id = (String)it.next();
                String[] data = ((String)items.get(id)).split(WebConstants.DATA_URL_SEPARATOR);
                if (data.length > 1) {
                	if (printLink) {
                		htmlOutput.append("<A href=\""+ url + data[1] +"\" target='_blank'>");
                	}
    				htmlOutput.append(data[0]);
    				if (printLink) {
    					htmlOutput.append("</A>");
    				}
                }
                
                if (it.hasNext())
                    htmlOutput.append(separator);
            }
          pageContext.getOut().print(htmlOutput.toString());
          
        }
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setUrl(String url) {
      this.url = url; 
  }

  public void setItems(Hashtable items) { 
    this.items = items;
  }
  
  public void setSeparator(String separator) { 
    this.separator = separator;
  }  
  
  public void setPrintLink(boolean printLink) {
	this.printLink = printLink;
  }
  
}