package com.tec.des.dao;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.ResourceBundle;
import com.tec.des.db.ConnectionPool;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.SystemException;

/**
 * Superclass for Data Access Objects. 
 * The DAO encapsulates the database connection and the sql for reading and writing data.
 * The DAO creates a database connection in the constructor. 
 * The implementing subclass uses this connection in its read/write methods.
 * The connection is freed by calling the DAO's close method.
 * The DAO super class also has methods for fetching the name of other databases that the
 * application needs to perform selects.
 *
 * @author :  Norunn Haug Christensen
 */
public abstract class DAO  {
  private ConnectionPool pool = null;
  private Connection connection = null;

  private String simulaDb = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE).getString(Constants.SIMULA_DATABASE_NAME);

 /**
  * Constructs a DAO. Obtains a connection from the connection pool.
  *
  * @throws SystemException
  * @throws SQLException
  */
  public DAO() throws SystemException, SQLException {
    super();
    pool = ConnectionPool.Instance();
    connection = pool.getConnection();
  }

 /**
  * Returns the database connection.
  *
  * @return The connection
  */
  protected Connection getConnection() {
    return connection;
  }

 /**
  * Closes the DAO, frees the connection.
  *
  * @throws SQLException  
  */
  public void close() throws SQLException{
    pool.releaseConnection(connection);
  }

 /**
  * Returns the name of the Simulaweb database.
  *
  */
  protected String getSimulaDb() {
    return simulaDb;
  }
  
  

}