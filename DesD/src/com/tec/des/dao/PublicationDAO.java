package com.tec.des.dao;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

import com.tec.des.dto.PublicationDTO;
import com.tec.des.ws.WSDAO;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.util.Nuller;

/**
 * Class encapsulating Publication data access.
 *
 * @author :  Per Kristian Foss
 */
public class PublicationDAO extends DAO {

	WSDAO wsDAO = new WSDAO();
	
 /**
  * Constructs a PublicationDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
    public PublicationDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns a Vector containing selected Publications. 
  * @param String containing Id's of selected Publications
  * @return Vector
  * @throws SQLException
  */

  public Vector getSelectedPublications(String publicationIdsString) throws SQLException {
	  
    Vector publications = new Vector();  
    if (Nuller.isReallyNull(publicationIdsString)) {
    	return publications;
    }
    
    String[] publicationsIds = publicationIdsString.split(",");
    for(int i = 0; i < publicationsIds.length; ++i) {
    	HashMap publicationData = wsDAO.getPublication(publicationsIds[i].trim());
    	if (publicationData == null) {
    		continue;
    	}
    	
    	PublicationDTO pub = new PublicationDTO();
        pub.setId((String)publicationData.get("id"));
        try {
			pub.setTitle(URLDecoder.decode((String)publicationData.get("title"), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		pub.setSource((String)publicationData.get("source"));
		pub.setUrl((String)publicationData.get("url"));
        publications.add(pub);
    }
    
    return publications;
  }
  
 /**
  * Returns a Vector containing Publications, based on the given search criteria. 
  * @param String searchText containing the words typed in by user
  * @return Vector
  * @throws SQLException
  */

  public Vector findPublications(String searchText) throws SQLException { 
	  
    Vector publications = new Vector();
    Object[] publicationsData = wsDAO.findPublications(searchText);
    
    for(int i = 0; i < publicationsData.length; ++i) {
    	HashMap publicationData = (HashMap)publicationsData[i];
    	PublicationDTO pub = new PublicationDTO();
        pub.setId((String)publicationData.get("id"));
        try {
			pub.setTitle(URLDecoder.decode((String)publicationData.get("title"), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		pub.setSource((String)publicationData.get("source"));
		pub.setUrl((String)publicationData.get("url"));
        publications.add(pub);
    }
    
    return publications;
  }
}