package com.tec.des.dao;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Vector;

import com.tec.des.dto.PublicationDTO;
import com.tec.des.dto.SearchDTO;
import com.tec.des.ws.WSDAO;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.util.Nuller;

/**
 * Class encapsulating Publication data access.
 *
 * @author :  Per Kristian Foss
 */
public class ReportDAO extends DAO {

	/**
	 * Constructs a PublicationDAO. 
	 *
	 * @throws SystemException
	 * @throws SQLException
	 */
	public ReportDAO() throws SystemException, SQLException {
		super();
	}
	
	public Vector getPersonReports(String loggedPersonId) throws SQLException {
		System.out.println("LIST " + loggedPersonId);
		Vector reports = new Vector();
		if (Nuller.isReallyNull(loggedPersonId)) {
			return reports;
		}
		
		StringBuffer query = new StringBuffer();    
	    query.append("select report_name from people_report where people_id = '").append(loggedPersonId).append("'");
	    
	    LoggerFactory.getLogger(ReportDAO.class).debug(query.toString()); 
	    Statement stmt = getConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(query.toString());
	    
	    while(rs.next()) {
	    	reports.add(rs.getString("report_name"));
	    }
	    
	    rs.close();
	    stmt.close();
		
		return reports;
	}

	public SearchDTO getReport(String reportName, String loggedPersonId) throws SQLException {
		System.out.println("GET " + reportName + " " + loggedPersonId);
		SearchDTO searchDTO = new SearchDTO();
		
		StringBuffer query = new StringBuffer();    
	    query.append("select responsibles_or_type, subsort from people_report ");
	    query.append("where people_id = '").append(loggedPersonId).append("' and report_name = '").append(reportName).append("'");
	    
	    LoggerFactory.getLogger(ReportDAO.class).debug(query.toString()); 
	    Statement stmt = getConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(query.toString());
	    
	    if (rs.next()) {
	    	searchDTO.setStudyResponsiblesArrsyOrType(rs.getBoolean("responsibles_or_type"));
	    	searchDTO.setSubSortByWithOrder(rs.getString("subsort"));
	    }
	    rs.close();
	    
	    //Loading columns
	    Vector columns = new Vector();
	    
	    query = new StringBuffer();    
	    query.append("select column_name from people_report_column ");
	    query.append("where people_id = '").append(loggedPersonId).append("' and report_name = '").append(reportName).append("'");
	    
	    LoggerFactory.getLogger(ReportDAO.class).debug(query.toString()); 
	    rs = stmt.executeQuery(query.toString());
	    
	    while (rs.next()) {
	    	columns.add(rs.getString("column_name"));
	    }
	    searchDTO.setColumns((String[])columns.toArray(new String[0]));
	    rs.close();
	    
	    //Loading people responsible
	    Vector responsibles = new Vector();
	    
	    query = new StringBuffer();    
	    query.append("select responsible_id from people_report_responsible ");
	    query.append("where people_id = '").append(loggedPersonId).append("' and report_name = '").append(reportName).append("'");
	    
	    LoggerFactory.getLogger(ReportDAO.class).debug(query.toString()); 
	    rs = stmt.executeQuery(query.toString());
	    
	    while (rs.next()) {
	    	responsibles.add(rs.getString("responsible_id"));
	    }
	    searchDTO.setStudyResponsiblesArray((String[])responsibles.toArray(new String[0]));
	    rs.close();
	    stmt.close();
	    
		return searchDTO;
	}
	
	public void saveReport(String reportName, String loggedPersonId, SearchDTO report) throws SQLException {
		System.out.println("SAVE " + reportName + " " + loggedPersonId);
		Statement stmt = getConnection().createStatement();
		deleteReport(reportName, loggedPersonId, stmt);
		
		stmt.executeUpdate("insert into people_report (`people_id`,`report_name`, `responsibles_or_type`, `subsort`) values ('" + 
				loggedPersonId + "', '" + reportName + "', " + report.isStudyResponsiblesArrsyOrType() + ", '" + report.getSubSortByWithOrder() + "')");
		
		if (report.getColumns() != null) {
			String[] columns = report.getColumns();
			for(int i = 0; i < columns.length; ++i) {
				stmt.executeUpdate("insert into people_report_column (`people_id`,`report_name`, `column_name`) values ('" + 
						loggedPersonId + "', '" + reportName + "', '" + columns[i] + "')");
			}
		}
		
		if (report.getStudyResponsiblesArray() != null) {
			String[] responsibles = report.getStudyResponsiblesArray();
			for(int i = 0; i < responsibles.length; ++i) {
				stmt.executeUpdate("insert into people_report_responsible (`people_id`,`report_name`, `responsible_id`) values ('" + 
						loggedPersonId + "', '" + reportName + "', '" + responsibles[i] + "')");
			}
		}
	}
	
	public void deleteReport(String reportName, String loggedPersonId) throws SQLException {
		System.out.println("DELETE " + reportName + " " + loggedPersonId);
		Statement stmt = getConnection().createStatement();
		deleteReport(reportName, loggedPersonId, stmt);
	}
	
	private void deleteReport(String reportName, String loggedPersonId, Statement stmt) throws SQLException {
		stmt.executeUpdate("delete from people_report_column where people_id = '" + loggedPersonId + "' and report_name = '" + reportName + "'");
		stmt.executeUpdate("delete from people_report_responsible where people_id = '" + loggedPersonId + "' and report_name = '" + reportName + "'");
		stmt.executeUpdate("delete from people_report where people_id = '" + loggedPersonId + "' and report_name = '" + reportName + "'");
	}

}