package com.tec.des.dao;
import java.sql.*;
import java.util.*;

import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.*;
import com.tec.des.dto.*;
import com.tec.server.db.ResultSetHelper;
import com.tec.des.util.*;
import com.tec.des.ws.AuthenticationWSDAO;
import com.tec.des.ws.WSDAO;

/**
 * Class encapsulating People data access.
 *
 * @author :  Per Kristian Foss
 */
public class PeopleDAO extends DAO {
	
	WSDAO wsDAO = new WSDAO();
	AuthenticationWSDAO authDAO = new AuthenticationWSDAO();

 /**
  * Constructs a PeopleDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
  public PeopleDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns a Vector containing all People. 
  * @return Vector
  * @throws SQLException
  */

  public Vector getAllPeople() throws SQLException {
	  
	Object[] peopleData = wsDAO.getPeopleList();
	
    Vector people = new Vector();
    
    StudyResponsibleDTO person;
    for(int i = 0; i < peopleData.length; ++i) {
    	person = new StudyResponsibleDTO();
    	HashMap personData = (HashMap)peopleData[i];
		person.setId((String)personData.get("id"));
        person.setListName(personData.get("lastname") + ", " + personData.get("firstname"));
        person.setUrl((String)personData.get("url"));
        people.add(person);
    }
       
    return people;
  }
  
 /**
  * Returns a Vector containing selected Study Responsibles. 
  * @param String[] containg the ids of the study responsibles to fetch.
  * @return Vector
  * @throws SQLException
  */

  public Vector getSelectedPeople(String[] selectedPeople) throws SQLException {

    Vector people = new Vector();
    
    StudyResponsibleDTO person;
    for (int i = 0; i < selectedPeople.length; i++) {
    	HashMap personData = wsDAO.getPerson(selectedPeople[i]);
    	if (personData == null) { //ignoring people not existing in web service
    		continue;
    	}
    	person = new StudyResponsibleDTO();
        person.setId((String)personData.get("id"));
        person.setListName(personData.get("lastname") + ", " + personData.get("firstname"));
        person.setSingleName(personData.get("firstname") + " " + personData.get("lastname"));
        person.setUrl((String)personData.get("url"));
        people.add(person);
    }
    
    return people;
  }
  
 /**
  * Fetches all people matching the given searchText. 
  * @param onlyRole If true only people with role matching the search criteria is fetched.
  * @param searchText The search criteria
  * @return Vector containing UserDTOs.
  * @throws SQLException
  */

  public Vector getPeopleRole(boolean onlyRole, String searchText) throws SQLException {
	  
	Vector peopleRole = new Vector();
	Object[] searchResults = wsDAO.findPeople(searchText);
	
    HashMap userRoles = new HashMap();
    StringBuffer query = new StringBuffer();    

    query.append("SELECT pr.people_id, r.role_id, r.role_name ");
    query.append("FROM people_role pr, role r ");
    query.append("WHERE pr.role_id = r.role_id ");
   
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        UserDTO user = new UserDTO();
        user.setId(rsh.getString("people_id"));           
        user.setRoleId(rsh.getInt("role_id"));           
        user.setRoleName(rsh.getString("role_name"));  
        userRoles.put(user.getId(), user);
    }
    
    rs.close();
    stmt.close();
    
    for(int i = 0; i < searchResults.length; ++i) {
    	HashMap personData = (HashMap)searchResults[i];
    	UserDTO role = (UserDTO)userRoles.get(personData.get("id"));
		if (role != null) {
    		role.setListName(personData.get("lastname") + ", " + personData.get("firstname"));
    		peopleRole.add(role);
    	} else if (!onlyRole) {
    		role = new UserDTO();
    		role.setId((String)personData.get("id"));           
    		role.setListName(personData.get("lastname") + ", " + personData.get("firstname"));
    		peopleRole.add(role);
    	}
    }
    
    return peopleRole;
  }
  
  /**
  * Returns a hashtable containing all Roles in the database 
  * @param Hashtable
  * @throws SQLException
  */

  public Hashtable getRoles() throws SQLException {

    Hashtable roles = new Hashtable();
    StringBuffer query = new StringBuffer();    
    
    query.append("select role_id, role_name from role;");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        roles.put(String.valueOf(rsh.getInt("role_id")), rsh.getString("role_name"));
    }   
    
    rs.close();
    stmt.close();
    
    return roles;
  }
  
 /**
  * Updates the people_role tablewith the given hashtable.
  * @throws SQLException
  */

  public void setPeopleRole(Hashtable peopleRole) throws SQLException {

    StringBuffer query_find = new StringBuffer();
    StringBuffer query_update = new StringBuffer();
    String[] peopleId = (String[])peopleRole.keySet().toArray(new String[0]);
    String[] peopleRoleId = (String[])peopleRole.values().toArray(new String[0]);
    Vector peopleWithRole = new Vector();;
    
    // Fetch all people having a role
    query_find.append("select people_id from people_role;");    
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query_find.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query_find.toString());    
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        peopleWithRole.add(rsh.getString("people_id"));
    }   
    
    // Checks previous role (if any) with new role and updates people_role table accordingly
    for (int i = 0; i < peopleId.length; i++) {       
        if (peopleWithRole.contains(peopleId[i])) {
            if (peopleRoleId[i].equals("0"))
                stmt.executeUpdate("delete from people_role where people_id = '" + peopleId[i] + "'");
            else {
                stmt.executeUpdate("update people_role set role_id = " + peopleRoleId[i] + " where people_id = '" + peopleId[i] + "';");
            }
        } else {            
            if (!(peopleRoleId[i].equals("0")))
                stmt.executeUpdate("insert into people_role values ('" +  peopleId[i] + "'," + peopleRoleId[i] + ");");
        }
    }
    
    rs.close();
    stmt.close();
  }
  
 /**
  * Returns the user connected to the specified username and password
  * @param username (e-mail) of the user to fetch
  * @param password of the user to fetch
  * @return UserDTO
  * @throws SQLException
  */
  public UserDTO getUser(String username, String password) throws SQLException, UserException {

	Boolean authSuccess = authDAO.getUser(username, password);
	if (!Boolean.TRUE.equals(authSuccess)) {
		throw new UserException(ExceptionMessages.INVALID_USER_NAME_OR_PASSWORD);
	}
	  
	HashMap personData = wsDAO.getPerson(username);
	if (personData == null) {
		throw new UserException(ExceptionMessages.USER_NOT_EXISTS_IN_SIMULAWEB);
	}
	  
    //String encryptedPassword = Password.crypt(password, "7$");  
    StringBuffer query = new StringBuffer();    
    query.append("SELECT r.role_id, r.role_name ");
    query.append("FROM people_role pr, role r ");
    query.append("WHERE pr.people_id = '" + Validator.checkEscapes(username) + "' and pr.role_id = r.role_id");
    // + "' and p.people_password = '" + encryptedPassword + "';");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    
    UserDTO user = new UserDTO();
    user.setId((String)personData.get("id"));           
    user.setSingleName(personData.get("firstname") + " " + personData.get("lastname")); 
    if (rs.next()) {
        user.setRoleName(rsh.getString("role_name"));           
        user.setRoleId(rsh.getInt("role_id"));           
    } else {
    	// TODO check default values from join's
    }
    
    rs.close();
    stmt.close();

    return user;
  }
      
}