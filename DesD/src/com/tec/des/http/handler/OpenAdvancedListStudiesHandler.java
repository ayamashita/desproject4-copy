package com.tec.des.http.handler;
import javax.servlet.http.*;

import com.tec.shared.util.*;
import com.tec.shared.exceptions.UserException;
import com.tec.des.http.*;
import com.tec.des.util.Constants;
import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.GetPersonReportsCommand;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.dto.UserDTO;

/**
 * 
 * Initialize the list studies view.
 *
 * @author Norunn Haug Christensen
 */
public class OpenAdvancedListStudiesHandler extends UsecaseHandler {

  /**
   * Initialize the view by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    GetStudyTypesCommand command = new GetStudyTypesCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());
    
    GetAllPeopleCommand command_all = new GetAllPeopleCommand();   
    command_all.execute();     
    request.setAttribute(WebKeys.REQUEST_BEAN_ALL_PEOPLE, command_all.getResult());
    
    request.setAttribute(WebKeys.REQUEST_BEAN_ADVANCED_SEARCH, Boolean.TRUE);
    
    GetPersonReportsCommand command_reports = new GetPersonReportsCommand();
	command_reports.setLoggedPersonId(((UserDTO)request.getSession().getAttribute(WebKeys.SESSION_PARAM_USER)).getId());
	command_reports.execute();
	request.setAttribute(WebKeys.REQUEST_BEAN_PERSON_REPORTS, command_reports.getResult());

    return WebConstants.VIEW_LIST_STUDIES;
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}
