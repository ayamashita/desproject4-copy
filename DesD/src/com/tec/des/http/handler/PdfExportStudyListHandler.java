package com.tec.des.http.handler;

import javax.servlet.http.*;

import com.tec.shared.util.*;
import com.tec.shared.exceptions.UserException;
import com.tec.des.http.*;
import com.tec.des.util.Constants;
import com.tec.des.util.Validator;
import com.tec.des.command.FindStudiesCommand;
import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.dto.SearchDTO;

/**
 * 
 * Initialize the list studies view.
 *
 * @author Norunn Haug Christensen
 */
public class PdfExportStudyListHandler extends FindStudiesHandler {

	/**
	 * Initialize the view by executing the command.
	 *
	 * @return The name of the resulting view. 
	 */
	public String doHandle(HttpServletRequest request) throws Exception {

		SearchDTO searchCriteria = getSearchCriteria(request);
		putSearchParameterOnRequest(searchCriteria, request);

		Validator.validateSearchCriteria(searchCriteria);
		FindStudiesCommand command = new FindStudiesCommand();
		command.setSearchCriteria(searchCriteria);
		command.setMaxHitListLength(1000000);
		command.setStartPosition(0);
		command.setFullData(true);
		command.execute();

		request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_RESULT, command.getResult());
		
		return WebConstants.VIEW_EXPORT_STUDIES_PDF;
	}

	/**
	 * Return the required role for the usecase.
	 *
	 * @return The role required to execute the usecase. 
	 */
	public int getRequiredRole() {
		return Constants.ROLE_STUDY_ADMIN;
	}

}
