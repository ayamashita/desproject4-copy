package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.dto.SearchDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.util.Constants;
import com.tec.des.util.Validator;

public abstract class AbstractPersonReportHandler extends FindStudiesHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
		SearchDTO searchCriteria = getSearchCriteria(request);   
	    putSearchParameterOnRequest(searchCriteria, request);
	    
	    Validator.validateSearchCriteria(searchCriteria);
	    
	    doPersonReportHandle(request);
	    
	    putSearchDataOnRequest(request);
		
		return WebConstants.VIEW_LIST_STUDIES;
	}
	
	abstract public void doPersonReportHandle(HttpServletRequest request) throws Exception;

	public int getRequiredRole() {
		return Constants.ROLE_STUDY_ADMIN;
	}
	
}
