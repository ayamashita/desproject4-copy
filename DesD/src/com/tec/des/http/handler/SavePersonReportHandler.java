package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.SavePersonReportCommand;
import com.tec.des.dto.SearchDTO;
import com.tec.des.http.WebKeys;

public class SavePersonReportHandler extends AbstractPersonReportHandler {

	public void doPersonReportHandle(HttpServletRequest request) throws Exception {
		SavePersonReportCommand command = new SavePersonReportCommand();
	    command.setLoggedPersonId(getLoggedPersonId(request));
		String saveName = request.getParameter(WebKeys.REQUEST_PARAM_REPORT_SAVE_NAME);
		command.setReportName(saveName);
		command.setSearch((SearchDTO)request.getAttribute(WebKeys.REQUEST_BEAN_SEARCH_CRITERIA));
		command.execute();
		request.setAttribute(WebKeys.REQUEST_PARAM_REPORT_LOAD_NAME, saveName);
	}
}
