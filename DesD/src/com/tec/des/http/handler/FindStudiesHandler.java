package com.tec.des.http.handler;
import java.util.Vector;

import javax.servlet.http.*;

import com.tec.shared.util.*;
import com.tec.shared.exceptions.UserException;
import com.tec.des.http.*;
import com.tec.des.dto.SearchDTO;
import com.tec.des.dto.UserDTO;
import com.tec.des.command.FindStudiesCommand;
import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.GetPersonReportsCommand;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.util.Validator;

/**
 * 
 * Handles the find studies use case. 
 *
 * @author Norunn Haug Christensen
 */
public class FindStudiesHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   * Validation of the search criterias is done before the search is performed.
   *
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    SearchDTO searchCriteria = getSearchCriteria(request);   
    boolean advancedSearch = putSearchParameterOnRequest(searchCriteria, request);

    try {
    	Validator.validateSearchCriteria(searchCriteria);
    } catch (UserException e) {
    	putSearchDataOnRequest(request);
    	throw e;
    }
    FindStudiesCommand command = new FindStudiesCommand();
    command.setSearchCriteria(searchCriteria); 
    command.setMaxHitListLength(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH)));
    command.setStartPosition(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_START_POSITION)));
    command.setFullData(advancedSearch);
    command.execute();

    if (command.getResult().isEmpty()) {
    	putSearchDataOnRequest(request);
        throw new UserException(ExceptionMessages.NO_HITS);
    }
    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_RESULT, command.getResult());
    
    String view;
    String printerFriendly = request.getParameter(WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY);
    
    if (!Nuller.isReallyNull(printerFriendly) && Parser.parseBoolean(printerFriendly)) {
        view = WebConstants.VIEW_STUDY_OVERVIEW_REPORT_PF;
    } else {
        view = WebConstants.VIEW_STUDY_OVERVIEW_REPORT;
    }

    return view;
  }
  
  /**
   * Returns a SearchDTO containing the search criteria.
   */
  protected SearchDTO getSearchCriteria(HttpServletRequest request) {
    SearchDTO search = new SearchDTO();
    search.setAdvancedSearch(Boolean.TRUE.toString().equals(request.getParameter(WebKeys.REQUEST_PARAM_ADVANCED_SEARCH)));
    search.setFreeText(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_TEXT));
    search.setTypeOfStudy(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_TYPE_OF_STUDY));
    search.setEndOfStudyFrom(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM));    
    search.setEndOfStudyTo(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO));    
    search.setStudyResponsibles(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES));    
    search.setStudyResponsiblesArray(request.getParameterValues(WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES_ARRAY));    
    search.setStudyResponsiblesArrsyOrType(Boolean.TRUE.toString().equals(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES_ARRAY_TYPE)));
    search.setSortBy(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_SORT_BY));
    if (WebConstants.SORT_ORDER_DESCENDING.equals(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING))) {
    	search.setSortOrder(WebConstants.SORT_ORDER_DESCENDING);    
    } else {
    	search.setSortOrder(WebConstants.SORT_ORDER_ASCENDING);    
    }
    search.setSubSortByWithOrder(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_SUB_SORT_BY_WITH_ORDER));
    search.setColumns(request.getParameterValues(WebKeys.REQUEST_PARAM_SEARCH_COLUMNS_TO_DISPLAY));

    return search;
  }
  
  /**
   * Put the search criteria and the study types on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  protected boolean putSearchParameterOnRequest(SearchDTO searchCriteria, HttpServletRequest request) throws Exception {
    
    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_CRITERIA, searchCriteria);
    
    boolean advancedSearch = Boolean.TRUE.toString().equals(request.getParameter(WebKeys.REQUEST_PARAM_ADVANCED_SEARCH));
    request.setAttribute(WebKeys.REQUEST_BEAN_ADVANCED_SEARCH, advancedSearch ? Boolean.TRUE : Boolean.FALSE);
	
	return advancedSearch;
  }
  
  /**
   * Put the search criteria and the study types on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  protected boolean putSearchDataOnRequest(HttpServletRequest request) throws Exception {
    
    GetStudyTypesCommand command = new GetStudyTypesCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());
    
    GetAllPeopleCommand command_all = new GetAllPeopleCommand();   
    command_all.execute();     
    request.setAttribute(WebKeys.REQUEST_BEAN_ALL_PEOPLE, command_all.getResult());
    
    boolean advancedSearch = Boolean.TRUE.toString().equals(request.getParameter(WebKeys.REQUEST_PARAM_ADVANCED_SEARCH));
	
	Vector personReports;
	if (advancedSearch) {
		GetPersonReportsCommand command_reports = new GetPersonReportsCommand();
		
		command_reports.setLoggedPersonId(getLoggedPersonId(request));
		command_reports.execute();
		personReports = command_reports.getResult();
	} else {
		personReports = new Vector(0);
	}
	
	request.setAttribute(WebKeys.REQUEST_BEAN_PERSON_REPORTS, personReports);
	
	return advancedSearch;
  }
  
  protected String getLoggedPersonId(HttpServletRequest request) {
    UserDTO userDTO = (UserDTO) request.getSession().getAttribute(WebKeys.SESSION_PARAM_USER);
    return userDTO.getId();
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Nuller.getIntNull();
  }
  
}