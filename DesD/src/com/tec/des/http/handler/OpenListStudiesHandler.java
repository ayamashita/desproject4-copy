package com.tec.des.http.handler;
import java.util.Vector;

import javax.servlet.http.*;

import com.tec.shared.util.*;
import com.tec.shared.exceptions.UserException;
import com.tec.des.http.*;
import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.GetStudyTypesCommand;

/**
 * 
 * Initialize the list studies view.
 *
 * @author Norunn Haug Christensen
 */
public class OpenListStudiesHandler extends UsecaseHandler {

  /**
   * Initialize the view by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    GetStudyTypesCommand command = new GetStudyTypesCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());
    
    request.setAttribute(WebKeys.REQUEST_BEAN_ADVANCED_SEARCH, Boolean.FALSE);
    
    request.setAttribute(WebKeys.REQUEST_BEAN_PERSON_REPORTS, new Vector());

    return WebConstants.VIEW_LIST_STUDIES;
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Nuller.getIntNull();
  }
  
}
