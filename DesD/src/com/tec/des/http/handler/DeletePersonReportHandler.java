package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.DeletePersonReportCommand;
import com.tec.des.http.WebKeys;

public class DeletePersonReportHandler extends AbstractPersonReportHandler {

	public void doPersonReportHandle(HttpServletRequest request) throws Exception {
		DeletePersonReportCommand command = new DeletePersonReportCommand();
	    command.setLoggedPersonId(getLoggedPersonId(request));
		command.setReportName(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_LOAD_NAME));
		command.execute();
		request.setAttribute(WebKeys.REQUEST_PARAM_REPORT_LOAD_NAME, "");
	}
}
