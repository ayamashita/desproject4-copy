package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.LoadPersonReportCommand;
import com.tec.des.http.WebKeys;

public class LoadPersonReportHandler extends AbstractPersonReportHandler {

	public void doPersonReportHandle(HttpServletRequest request) throws Exception {
		LoadPersonReportCommand command = new LoadPersonReportCommand();
	    command.setLoggedPersonId(getLoggedPersonId(request));
		String loadName = request.getParameter(WebKeys.REQUEST_PARAM_REPORT_LOAD_NAME);
		command.setReportName(loadName);
		command.execute();
		request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_CRITERIA, command.getResult());
		request.setAttribute(WebKeys.REQUEST_PARAM_REPORT_SAVE_NAME, loadName);
	}
}
