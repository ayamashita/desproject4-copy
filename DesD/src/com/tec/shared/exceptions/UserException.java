package com.tec.shared.exceptions;
/**
 * User exception, the main node of user exceptions in 
 * the system's exception hierarchy.
 *
 * @author :  Norunn Haug Christensen
 */
public class UserException extends GeneralException  {
  /**
   * Creates a UserException with the specified message.
   */
  public UserException(String message) {
    super(message);
  }
}