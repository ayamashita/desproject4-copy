package com.tec.server.db;
import java.util.Iterator;
import java.util.Set;

import com.tec.shared.util.Nuller;

/**
 * SQL helper class. 
 *
 * @author : Norunn Haug Christensen
 */
public class SQLHelper  {
    
  private SQLHelper() {
  }
/**
 * Returns "like" if the search string starts or ends with an %, else "=" is returned.
 */
  public static String getLikeOrEquals(String searchString){
    String percent = "%";
    if (searchString.startsWith(percent)||searchString.endsWith(percent)){
      return "like";
    } else {
      return "=";
    }
    
  }
  
/**
 * Returns a "like" condition.
 */
  public static String getLike(String searchString){
    return "like '%" + searchString + "%'";
  }

/**
 * Returns null if the input value is STRINGNULL
 */
  public static String getValue(String value) {
    if (Nuller.isNull(value))
        return null;
    else 
        return "'" + value + "'";
  }

/**
 * Returns null if the input value is INTNULL
 */
  public static String getValue(int value) {
    if (Nuller.isNull(value))
        return null;
    else 
        return String.valueOf(value);
  }

  
/**
 * Returns 1 if the input value is true, 0 otherwise.
 */
  public static int getValue(boolean value) {
    if (value)
        return 1;
    else 
        return 0;
  }
  
  public static String getInStatement(String what, Set set) {
	  StringBuffer buffer = new StringBuffer();
	  if (!set.isEmpty()) {
		  buffer.append(what).append(" in (");
	    	Iterator iter = set.iterator();
	    	while(iter.hasNext()) {
	    		buffer.append("'").append(iter.next()).append("'");
	    		if (iter.hasNext()) {
	    			buffer.append(",");
	    		}
	    	}
	    	buffer.append(") ");
	    }
	  
	  return buffer.toString();
  }
  
	public static String getInStatement(String what, String[] set) {
		StringBuffer buffer = new StringBuffer();
		if (set.length > 0) {
			buffer.append(what).append(" in (");
			for(int i = 0; i < set.length; ++i) {
				if (i > 0) {
					buffer.append(",");
				}
				buffer.append("'").append(set[i]).append("'");
			}
	    	buffer.append(") ");
	    }
	return buffer.toString();
  }

}