<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
>

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>

        <fo:simple-page-master
          master-name="page" 
          page-height="21cm"
          page-width="29.7cm"
          margin-top="1.5cm"
          margin-bottom="2cm"
          margin-left="1.5cm"
          margin-right="1.5cm"
        >
          <fo:region-before extent="1cm"/>
          <fo:region-after extent="1cm"/>
          <fo:region-body margin-top="1.1cm" margin-bottom="1.1cm"/>
        </fo:simple-page-master>

      </fo:layout-master-set>

      <fo:page-sequence master-reference="page">

        <fo:static-content flow-name="xsl-region-before">
          <fo:block
            font-family="Arial, Helvetica"
            font-size="6pt"
            text-align="center"
          >
            <xsl:text>[ simula.research laboratory ]  studies overview report</xsl:text>
          </fo:block>
        </fo:static-content>

        <fo:static-content flow-name="xsl-region-after">
          <fo:block
            font-family="Arial, Helvetica"
            font-size="6pt"
            text-align="center"
          >
            Page <fo:page-number />
          </fo:block>
	</fo:static-content>
        
        <fo:flow flow-name="xsl-region-body">
          <xsl:apply-templates/>
        </fo:flow>
       </fo:page-sequence>
      
  </fo:root>

  </xsl:template>
  
  
  <xsl:template match="table">
    <fo:table table-layout="fixed">
      <!-- Hard coded column-widths for now -->
      <xsl:for-each select="columns/column">
        <fo:table-column>
        	<xsl:attribute name="column-width">
        		<xsl:value-of select="@column-width"/>
        	</xsl:attribute>
        </fo:table-column>
      </xsl:for-each>
      <!--
      <fo:table-column column-width="4cm"/>
      <fo:table-column column-width="4cm"/>
      <fo:table-column column-width="2cm"/>
      <fo:table-column column-width="4cm"/>
      <fo:table-column column-width="6cm"/>
      <fo:table-column column-width="6cm"/>
      -->
        
      <fo:table-body>
          <xsl:apply-templates/>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template match="rows">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="columns|row">
    <fo:table-row><xsl:apply-templates/></fo:table-row>
  </xsl:template>

  <xsl:template match="columns/column">
    <fo:table-cell font-size="8pt" font-weight="bold" text-align="center">
      <xsl:attribute name="border-style">solid</xsl:attribute>
      <xsl:attribute name="border-width">0.1pt</xsl:attribute>
      <fo:block margin="2pt" padding="2pt">
        <xsl:apply-templates/>
      </fo:block>
    </fo:table-cell>
  </xsl:template>

  <xsl:template match="row/column">
    <fo:table-cell font-size="8pt">
      <xsl:attribute name="border-style">solid</xsl:attribute>
      <xsl:attribute name="border-width">0.1pt</xsl:attribute>
      <fo:block margin="2pt" padding="2pt">
        <xsl:apply-templates/>
      </fo:block>
    </fo:table-cell>
  </xsl:template>
  
  <xsl:template match="ul|ol">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="li">
    <fo:block><xsl:apply-templates/></fo:block>
  </xsl:template>
  
  <!--xsl:template match="ul|ol">
    <fo:list-block space-before="0em" space-after="0em">
      <xsl:apply-templates/>
    </fo:list-block>
  </xsl:template>

  <xsl:template match="ol/li">
    <fo:list-item space-after="0.5em">
      <fo:list-item-label start-indent="1em">
        <fo:block><xsl:number/>.</fo:block>
      </fo:list-item-label>
      <fo:list-item-body>
        <fo:block>
          <xsl:apply-templates/>
        </fo:block>
      </fo:list-item-body>
    </fo:list-item>
  </xsl:template>

  <xsl:template match="ul/li">
    <fo:list-item space-after="0.5em">
      <fo:list-item-label start-indent="1em">
        <fo:block>&#x2022;</fo:block>
      </fo:list-item-label>
      <fo:list-item-body>
        <fo:block>
          <xsl:apply-templates/>
        </fo:block>
      </fo:list-item-body>
    </fo:list-item>
  </xsl:template-->
    
</xsl:stylesheet> 
