<%@ page import="java.io.*,javax.servlet.*,com.tec.des.command.PDFExportStudiesCommand,com.tec.des.util.Message" %>
<jsp:useBean id="searchResult" class="com.tec.shared.util.HitList" scope="request"/> 
<jsp:useBean id="searchCriteria" class="com.tec.des.dto.SearchDTO" scope="request"/> 
<%
//Fetches an object containing information about all studies as a csv-file
int iRead;
FileInputStream stream = null;
Message message = null;
try { 
    //response.setContentLength((int)file.length());
    response.setContentType("application/pdf");
    response.setHeader("Content-Disposition","attachment; filename=studies.pdf");

    PDFExportStudiesCommand command = new PDFExportStudiesCommand();   
    command.setStudies(searchResult);
    command.setResponse(response);
    command.setColumns(searchCriteria.getColumns());
    command.execute();
} catch (Exception ex) {
    message = new Message(ex.getMessage());
%>
<jsp:include page="top.jsp"/>
<TABLE cellspacing="6" cellpadding="6">
    <TR>
        <TD>
            <TABLE cellspacing="2" cellpadding="0"> 
            <% if (message != null) { %>
            <FONT class="errortext" ><%=message.getText()%></FONT><BR>
            <% } %>
            </TABLE>
        </TD>
    </TR> 
</TABLE> 
<jsp:include page="bottom.html"/>
<%
} finally {
    if (stream != null) {
        stream.close(); 
    }
}
%>
