<jsp:include page="top.jsp"/>
<%@ page import="com.tec.des.http.*" %>
<TABLE cellspacing="6" cellpadding="6">
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
    <TR>
        <TD>
            <TABLE cellspacing="2" cellpadding="0">
            <TR>
                <TD class="bodytext-bold"><jsp:include page="messages.jsp"/></TD>
            </TR> 
            <TR>
                <TD align="right"><A href="JavaScript:help('help.html#login')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
            </TR>
            <TR>
                <TD class="bodytext-bold">User name</TD>
            </TR>
            <TR>
                <TD class="bodytext"><INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_USER_NAME%>" value="<%=request.getParameter(WebKeys.REQUEST_PARAM_USER_NAME)==null?"":request.getParameter(WebKeys.REQUEST_PARAM_USER_NAME)%>" size="30"></TD>
            </TR>
            <TR>
                <TD class="bodytext-bold">Password</TD>
            </TR>
            <TR>
                <TD class="bodytext"><INPUT class="bodytext" type="password" name="<%=WebKeys.REQUEST_PARAM_PASSWORD%>" value="" size="30"></TD>
            </TR>
            <TR>
                <TD class="bodytext"><INPUT class="bodytext" type="submit" name="Login" value="Login"></TD>
            </TR>
            </TABLE>
        </TD>
    </TR> 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="<%=WebConstants.USECASE_LOGIN%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_LOGIN%>" > 
</FORM>
</TABLE> 
<jsp:include page="bottom.html"/>