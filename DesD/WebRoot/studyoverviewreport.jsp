<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.*" %>
<%@ page import="com.tec.des.dto.UserDTO" %>
<%@ page import="com.tec.des.dto.StudyDTO"%>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="advancedSearch" type="java.lang.Boolean" scope="request"/> 
<jsp:useBean id="searchResult" class="com.tec.shared.util.HitList" scope="request"/> 
<jsp:useBean id="searchCriteria" class="com.tec.des.dto.SearchDTO" scope="request"/> 
<% UserDTO user = (UserDTO)session.getAttribute(WebKeys.SESSION_PARAM_USER); %>
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top"><A id="hl-link" href="<%=WebConstants.FRONTCONTROLLER_URL%>?usecase=<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;list studies</A> &gt; study overview report</TD>
        <TD align="right"><A href="JavaScript:help('help.html#studyoverviewreport')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="6" cellpadding="6"><TR><TD>
<jsp:include page="messages.jsp"/>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<util:hitlist name="hitlist" mode="search" maxHits="<%=WebConstants.MAX_HIT_LIST_LENGTH%>" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
<TABLE cellspacing="0" cellpadding="1"> 
    <TR>
        <TD colspan="2"><H2>Study Overview Report</H2></TD>
        <TD colspan="4" align="right" class="bodytext" valign="top"><util:searchnavigation totalHitListCount="<%= searchResult.getCount() %>" hitListLength="<%=request.getParameter(WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH)%>" startPosition="<%=request.getParameter(WebKeys.REQUEST_PARAM_START_POSITION)%>" /></TD>
    </TR> 
    <TR>
<% if (!advancedSearch.booleanValue()) { %>
    	<% if (searchCriteria.displayColumn("name")) { %><TD class="bodytext-bold" valign="top">Study Name
    	<nobr>
    	<a onclick="document.form.target='_self';document.form.printer_friendly.value='false';document.form.start_position.value='0';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING %>.value='<%=WebConstants.SORT_ORDER_ASCENDING %>';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_BY %>.value='<%=WebConstants.SORT_BY_STUDY_NAME %>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';" href="javascript:document.form.submit();"><IMG src="./images/down_arrow.bmp" /></a>
    	<a onclick="document.form.target='_self';document.form.printer_friendly.value='false';document.form.start_position.value='0';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING %>.value='<%=WebConstants.SORT_ORDER_DESCENDING %>';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_BY %>.value='<%=WebConstants.SORT_BY_STUDY_NAME %>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';" href="javascript:document.form.submit();"><IMG src="./images/up_arrow.bmp" /></a>
    	</nobr>
    	
    	&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("type")) { %><TD class="bodytext-bold" valign="top">Type Of Study
        <nobr>
        <a onclick="document.form.target='_self';document.form.printer_friendly.value='false';document.form.start_position.value='0';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING %>.value='<%=WebConstants.SORT_ORDER_ASCENDING %>';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_BY %>.value='<%=WebConstants.SORT_BY_TYPE_OF_STUDY %>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';" href="javascript:document.form.submit();"><IMG src="./images/down_arrow.bmp" /></a>
    	<a onclick="document.form.target='_self';document.form.printer_friendly.value='false';document.form.start_position.value='0';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING %>.value='<%=WebConstants.SORT_ORDER_DESCENDING %>';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_BY %>.value='<%=WebConstants.SORT_BY_TYPE_OF_STUDY %>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';" href="javascript:document.form.submit();"><IMG src="./images/up_arrow.bmp" /></a>
    	</nobr>
        &nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("end")) { %><TD class="bodytext-bold" valign="top">End Of Study
        <nobr>
        <a onclick="document.form.target='_self';document.form.printer_friendly.value='false';document.form.start_position.value='0';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING %>.value='<%=WebConstants.SORT_ORDER_ASCENDING %>';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_BY %>.value='<%=WebConstants.SORT_BY_END_OF_STUDY %>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';" href="javascript:document.form.submit();"><IMG src="./images/down_arrow.bmp" /></a>
    	<a onclick="document.form.target='_self';document.form.printer_friendly.value='false';document.form.start_position.value='0';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING %>.value='<%=WebConstants.SORT_ORDER_DESCENDING %>';document.form.<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_BY %>.value='<%=WebConstants.SORT_BY_END_OF_STUDY %>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';" href="javascript:document.form.submit();"><IMG src="./images/up_arrow.bmp" /></a>
    	</nobr>
        &nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("responsibles")) { %><TD class="bodytext-bold" valign="top">Study Responsibles&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("description")) { %><TD class="bodytext-bold" valign="top">Study Description&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("publications")) { %><TD class="bodytext-bold" valign="top">Publications&nbsp;&nbsp;</TD><% } %>
<% if (user != null) { %>
        <TD class="bodytext-bold" valign="top" align="center">Delete</TD> 
<% } %>
<% } %>

<% if (advancedSearch.booleanValue()) { %>
		<% if (searchCriteria.displayColumn("name")) { %><TD class="bodytext-bold" valign="top">Study Name&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("responsibles")) { %><TD class="bodytext-bold" valign="top">Study Responsibles&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("type")) { %><TD class="bodytext-bold" valign="top">Type Of Study&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("description")) { %><TD class="bodytext-bold" valign="top">Study Description&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("duration")) { %><TD class="bodytext-bold" valign="top">Duration&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("start")) { %><TD class="bodytext-bold" valign="top">Start Of Study&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("end")) { %><TD class="bodytext-bold" valign="top">End Of Study&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("publications")) { %><TD class="bodytext-bold" valign="top">Publications&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("keywords")) { %><TD class="bodytext-bold" valign="top">Keywords&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("studentParticipants")) { %><TD class="bodytext-bold" valign="top">Student Participants&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("proffesionalParticipants")) { %><TD class="bodytext-bold" valign="top">Professional Participants&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("material")) { %><TD class="bodytext-bold" valign="top">Study Material&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("notes")) { %><TD class="bodytext-bold" valign="top">Study Notes&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("owner")) { %><TD class="bodytext-bold" valign="top">Study Owner&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("lastEdited")) { %><TD class="bodytext-bold" valign="top">Last Edited by&nbsp;&nbsp;</TD><% } %>
<% } %>
    </TR>

    <util:iteration name="study" type="com.tec.des.dto.StudySDTO" rowAlternator="row" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
    <TR class="<%=row%>">
<% if (!advancedSearch.booleanValue()) { %>
        <% if (searchCriteria.displayColumn("name")) { %><TD class="bodytext" valign="top">
        	<A href="javascript:document.form.submit();" onclick="document.form.target='_self';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_SINGLE_STUDY_REPORT%>';document.form.<%=WebKeys.REQUEST_PARAM_STUDY_ID%>.value='<%=study.getId()%>';document.form.<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>.value='false';"><%=study.getName()%></A>&nbsp;&nbsp;</TD>
        <% } %>
        <% if (searchCriteria.displayColumn("type")) { %><TD class="bodytext" valign="top"><%=study.getType()%>&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("end")) { %><TD class="bodytext" valign="top"><%=study.getEndDate()%></TD><% } %>
        <% if (searchCriteria.displayColumn("responsibles")) { %><TD class="bodytext" valign="top"><util:links url="" items="<%=study.getResponsibles()%>"/>&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("description")) { %><TD class="bodytext" valign="top"><%=Validator.replaceEnterWithBreak(study.getDescription())%>&nbsp;&nbsp;</TD><% } %>
        <% if (searchCriteria.displayColumn("publications")) { %><TD class="bodytext" valign="top"><util:links url="" items="<%=study.getPublications()%>"/>&nbsp;&nbsp;</TD><% } %>
<% if (user != null && !advancedSearch.booleanValue()) { %>
        <TD valign="top" align="center"><INPUT type="radio" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID_DELETE%>" value="<%=study.getId()%>" class="bodytext" checked></TD>
<% } %>
<% } %>
<% if (advancedSearch.booleanValue()) { %>
		<% if (searchCriteria.displayColumn("name")) { %><TD class="bodytext" valign="top"><%=study.getName()%></TD><% } %>
		<% if (searchCriteria.displayColumn("responsibles")) { %><TD class="bodytext" valign="top"><util:links url="" items="<%=study.getResponsibles()%>"/>&nbsp;&nbsp;</TD><% } %>
		<% if (searchCriteria.displayColumn("type")) { %><TD class="bodytext" valign="top"><%=study.getType()%>&nbsp;&nbsp;</TD><% } %>
		<% if (searchCriteria.displayColumn("description")) { %><TD class="bodytext" valign="top"><%=Validator.replaceEnterWithBreak(study.getDescription())%>&nbsp;&nbsp;</TD><% } %>
		<% if (searchCriteria.displayColumn("duration")) { %><TD class="bodytext" valign="top"><%=((StudyDTO)study).getDuration()%> <%=((StudyDTO)study).getDurationUnit()%></TD><% } %>
		<% if (searchCriteria.displayColumn("start")) { %><TD class="bodytext" valign="top"><%=((StudyDTO)study).getStartDate()%></TD><% } %>
		<% if (searchCriteria.displayColumn("end")) { %><TD class="bodytext" valign="top"><%=study.getEndDate()%></TD><% } %>
		<% if (searchCriteria.displayColumn("publications")) { %><TD class="bodytext" valign="top"><util:links url="" items="<%=study.getPublications()%>"/>&nbsp;&nbsp;</TD><% } %>
		<% if (searchCriteria.displayColumn("keywords")) { %><TD class="bodytext" valign="top"><%=((StudyDTO)study).getKeywords()%></TD><% } %>
		<% if (searchCriteria.displayColumn("studentParticipants")) { %><TD class="bodytext" valign="top"><%=((StudyDTO)study).getNoOfStudentParticipants()%></TD><% } %>
		<% if (searchCriteria.displayColumn("proffesionalParticipants")) { %><TD class="bodytext" valign="top"><%=((StudyDTO)study).getNoOfProfessionalParticipants()%></TD><% } %>
		<% if (searchCriteria.displayColumn("material")) { %><TD class="bodytext" valign="top"><util:studymaterial items="<%=((StudyDTO)study).getMaterial()%>" /></TD><% } %>
		<% if (searchCriteria.displayColumn("notes")) { %><TD class="bodytext" valign="top"><%=((StudyDTO)study).getNotes()%></TD><% } %>
		<% if (searchCriteria.displayColumn("owner")) { %><TD class="bodytext" valign="top"><%=((StudyDTO)study).getOwner()%></TD><% } %>
		<% if (searchCriteria.displayColumn("lastEdited")) { %><TD class="bodytext" valign="top"><%=((StudyDTO)study).getLastEditedBy()%></TD><% } %>
<% } %>
    </TR>
    </util:iteration>
</TABLE>
</util:hitlist>
<TABLE cellspacing="4" cellpadding="1" width="100%"> 
    <TR>
        <TD class="bodytext">&nbsp;</TD>
    </TR>
    <TR>
        <TD><INPUT type="submit" name="" value="New search" class="bodytext" onclick="document.form.target='_self';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>';"></TD>
<% if (user != null && !advancedSearch.booleanValue()) { %>
        <TD align="right"><INPUT type="submit" name="" value="Delete" class="bodytext" onclick="document.form.target='_self';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_STUDY%>';return confirm('Do you really want to delete selected Study?');"></TD>
<% } %>
    </TR>
    <TR>
        <TD class="bodytext">&nbsp;</TD>
    </TR>
    <TR>
        <TD class="bodytext">
            <A href="JavaScript:document.form.submit();" onclick="document.form.target='_blank'; 
            document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=advancedSearch.booleanValue() ? WebConstants.USECASE_PDF_EXPORT_STUDIES : WebConstants.USECASE_LIST_STUDIES%>';
            document.form.<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>.value='<%=searchResult.getCount()%>';
            document.form.<%=WebKeys.REQUEST_PARAM_START_POSITION%>.value='<%=WebConstants.DEFAULT_START_POSITION%>';
            document.form.<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>.value='true';"><%= advancedSearch.booleanValue() ? "Export into PDF" : "Printer friendly version" %></A>
        </TD>
    </TR>
</TABLE>

<util:searchcriteria searchCriteria="<%= searchCriteria %>" /> 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAINTAIN_MODE%>" value="<%=Constants.MAINTAIN_MODE_FINAL%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>" value="<%=WebConstants.MAX_HIT_LIST_LENGTH%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_START_POSITION%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID_DELETE%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_ADVANCED_SEARCH%>" value="<%=advancedSearch.toString()%>" >
</FORM>
</TD></TR> 
</TABLE> 
<jsp:include page="bottom.html"/>