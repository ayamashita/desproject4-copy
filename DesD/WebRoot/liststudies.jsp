<%@ page import="com.tec.des.http.*" %>
<%@page import="com.tec.des.util.JSP"%>
<%@page import="com.tec.shared.util.Nuller"%>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="advancedSearch" type="java.lang.Boolean" scope="request"/> 
<jsp:useBean id="allPeople" class="java.util.Vector" scope="request"/> 
<jsp:useBean id="personReports" class="java.util.Vector" scope="request"/> 
<jsp:useBean id="studyTypes" class="java.util.Hashtable" scope="request"/> 
<jsp:useBean id="searchCriteria" class="com.tec.des.dto.SearchDTO" scope="request"/> 
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top"><A id="hl-link" href="<%=WebConstants.FRONTCONTROLLER_URL%>?usecase=<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>">&nbsp;&nbsp;&nbsp;&nbsp;list studies</A></TD>
        <TD align="right"><A href="JavaScript:help('help.html#liststudies')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="6" cellpadding="6"><TR><TD>
<jsp:include page="messages.jsp"/>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TABLE>
<TR>
<TD>
<TABLE cellspacing="2" cellpadding="0"> 
    <TR>
        <TD colspan="3"><H1><%= advancedSearch.booleanValue() ? "Report configuration" : "List Studies" %></H1></TD>
    </TR> 
    <% if (!advancedSearch.booleanValue()) { %>
    <TR>
        <TD class="bodytext">Free text</TD>
        <TD colspan="2"><INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_TEXT%>" value="<%=searchCriteria.getFreeText()%>" size="50"></TD>
    </TR> 
    <TR>
        <TD class="bodytext">Type of Study</TD>  
        <TD class="bodytext">
            <util:dropdownlist name="<%=WebKeys.REQUEST_PARAM_SEARCH_TYPE_OF_STUDY%>" attributes="class=bodytext" optional="true" displayedvalues="<%=(String[])studyTypes.values().toArray(new String[0]) %>" selectedvalue="<%=searchCriteria.getTypeOfStudy() %>" optionValues="<%=(String[])studyTypes.keySet().toArray(new String[0]) %>" /> 
        </TD>
    </TR>
    <TR>
        <TD class="bodytext">End of Study</TD>
        <TD class="bodytext">
            <INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM%>" value="<%=searchCriteria.getEndOfStudyFrom()%>" size="7" maxlength="10" class="bodytext"> 
            <A href="JavaScript:calendar()" onclick="document.form.target='calendar';document.form.action='calendar.jsp';document.form.<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>.value='<%=WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM%>'">From</A>&nbsp;&nbsp;
            <INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO%>" value="<%=searchCriteria.getEndOfStudyTo()%>" size="7" maxlength="10" class="bodytext"> 
            <A href="JavaScript:calendar()" onclick="document.form.target='calendar';document.form.action='calendar.jsp';document.form.<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>.value='<%=WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO%>'">To</A>&nbsp;&nbsp; 
            <FONT class="bodytext-italic">yyyy/mm/dd</FONT> 
        </TD>
    </TR>
    <% } %>
    <TR>
        <TD class="bodytext">Study Responsibles</TD>
        <TD colspan="2">
        	<% if (!advancedSearch.booleanValue()) { %>
        		<INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES%>" value="<%=searchCriteria.getStudyResponsibles()%>" size="50" class="bodytext">
        	<% } else { %>
	        	<util:studyresponsibleselect name="<%=WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES_ARRAY%>" boxsize="5" attributes="class=bodytext" items="<%=allPeople%>" value="<%= (String[])searchCriteria.getStudyResponsiblesArray() %>"/>
	
				<SPAN class="bodytext">Type</SPAN>
				<SELECT name="<%=WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES_ARRAY_TYPE%>">
					<OPTION value="false" <%=searchCriteria.isStudyResponsiblesArrsyOrType() ? "" : "selected"%>>AND</OPTION>
					<OPTION value="true" <%=searchCriteria.isStudyResponsiblesArrsyOrType() ? "selected" : ""%>>OR</OPTION>
				</SELECT>
			<% } %>
        </TD>
    </TR> 
    <% if (advancedSearch.booleanValue()) { %>
    	<TR>
    		<TD class="bodytext">Columns</TD> 
    		<TD class="bodytext">
    			<SELECT multiple name="<%=WebKeys.REQUEST_PARAM_SEARCH_COLUMNS_TO_DISPLAY%>">
    				<OPTION value="name" <%=searchCriteria.displayColumn("name") ? "selected" : "" %>>Study name</OPTION>
    				<OPTION value="responsibles" <%=searchCriteria.displayColumn("responsibles") ? "selected" : "" %>>Study Responsibles</OPTION>
    				<OPTION value="type" <%=searchCriteria.displayColumn("type") ? "selected" : "" %>>Type of study</OPTION>
    				<OPTION value="description" <%=searchCriteria.displayColumn("description") ? "selected" : "" %>>Study Description</OPTION>
    				<OPTION value="duration" <%=searchCriteria.displayColumn("duration") ? "selected" : "" %>>Duration</OPTION>
    				<OPTION value="start" <%=searchCriteria.displayColumn("start") ? "selected" : "" %>>Start Of Study</OPTION>
    				<OPTION value="end" <%=searchCriteria.displayColumn("end") ? "selected" : "" %>>End of study</OPTION>
    				<OPTION value="publications" <%=searchCriteria.displayColumn("publications") ? "selected" : "" %>>Publications</OPTION>
    				<OPTION value="keywords" <%=searchCriteria.displayColumn("keywords") ? "selected" : "" %>>Keywords</OPTION>
    				<OPTION value="studentParticipants" <%=searchCriteria.displayColumn("studentParticipants") ? "selected" : "" %>>Student Participants</OPTION>
    				<OPTION value="proffesionalParticipants" <%=searchCriteria.displayColumn("proffesionalParticipants") ? "selected" : "" %>>Professional Participants</OPTION>
    				<OPTION value="material" <%=searchCriteria.displayColumn("material") ? "selected" : "" %>>Study Material</OPTION>
    				<OPTION value="notes" <%=searchCriteria.displayColumn("notes") ? "selected" : "" %>>Study Notes</OPTION>
    				<OPTION value="owner" <%=searchCriteria.displayColumn("notes") ? "selected" : "" %>>Study Owner</OPTION>
    				<OPTION value="lastEdited" <%=searchCriteria.displayColumn("notes") ? "selected" : "" %>>Last Edited by</OPTION>
    			</SELECT>
    		</TD> 
    	</TR>
    <% } %>
    <TR>
        <TD class="bodytext"><%= advancedSearch.booleanValue() ? "Sort by time" : "Sort by"%></TD> 
        <TD class="bodytext">
        	<% if (!advancedSearch.booleanValue()) { %>
            <SELECT name="<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_BY%>" class="bodytext">
            <OPTION value="<%=WebConstants.SORT_BY_STUDY_NAME%>">Study Name</OPTION>
            <OPTION value="<%=WebConstants.SORT_BY_TYPE_OF_STUDY%>">Type of Study</OPTION>
            <OPTION value="<%=WebConstants.SORT_BY_END_OF_STUDY%>" selected>End of Study </OPTION>
            </SELECT>&nbsp;&nbsp;
            <INPUT type="checkbox" name="<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING%>" value="<%=WebConstants.SORT_ORDER_DESCENDING %>" class="bodytext" checked> Descending
            <% } %>
            <% if (advancedSearch.booleanValue()) { %>
            <SELECT name="<%=WebKeys.REQUEST_PARAM_SEARCH_SUB_SORT_BY_WITH_ORDER%>" class="bodytext">
            <OPTION value="" <%= Nuller.isReallyNull(searchCriteria.getSubSortByWithOrder()) ? "selected" : ""%>>No sort</OPTION>
            <OPTION value="<%=WebConstants.SUB_SORT_BY_TIME_ASC%>" <%=WebConstants.SUB_SORT_BY_TIME_ASC.equals(searchCriteria.getSubSortByWithOrder()) ? "selected" : ""%>>ASC</OPTION>
            <OPTION value="<%=WebConstants.SUB_SORT_BY_TIME_DESC%>" <%=WebConstants.SUB_SORT_BY_TIME_DESC.equals(searchCriteria.getSubSortByWithOrder()) ? "selected" : ""%>>DESC</OPTION>
            </SELECT>
            <% } %>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <INPUT type="submit" name="" value="Search" class="bodytext" onclick="document.form.target='_self';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';">
        </TD>
    </TR>
</TABLE>
</TD>
<TD style="width: 50px">
</TD>
<% if (advancedSearch.booleanValue()) { %>
	<TD style="vertical-align: top;">
		<TABLE cellspacing="2" cellpadding="0"> 
		    <TR>
		        <TD colspan="3"><H1>Report maintenance</H1></TD>
		    </TR>
		    <TR>
		    	<TD class="bodytext">Save name</TD>
		    	<TD class="bodytext">
		    		<INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_REPORT_SAVE_NAME%>" size="40" maxlength="30" class="bodytext"
		    			value="<%=(String)((request.getAttribute(WebKeys.REQUEST_PARAM_REPORT_SAVE_NAME) != null) ? request.getAttribute(WebKeys.REQUEST_PARAM_REPORT_SAVE_NAME) : JSP.removeNull(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_SAVE_NAME)))%>">
		    	</TD>
		    	<TD>
		    		<INPUT type="submit" name="" value="Save" class="bodytext" onclick="document.form.target='_self';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_SAVE_PERSON_REPORT%>';">
		    	</TD>
		    </TR> 
		    <TR>
		    	<TD class="bodytext">Reports</TD>
		    	<TD class="bodytext">
		    		<util:dropdownlist name="<%=WebKeys.REQUEST_PARAM_REPORT_LOAD_NAME%>" attributes="class=bodytext" optional="true" optionValues="<%=(String[])personReports.toArray(new String[0]) %>" 
		    			selectedvalue="<%=(String)((request.getAttribute(WebKeys.REQUEST_PARAM_REPORT_LOAD_NAME) != null) ? request.getAttribute(WebKeys.REQUEST_PARAM_REPORT_LOAD_NAME) : request.getParameter(WebKeys.REQUEST_PARAM_REPORT_LOAD_NAME)) %>"/>
		    	</TD>
		    	<TD>
		    		<INPUT type="submit" name="" value="Load" class="bodytext" onclick="document.form.target='_self';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LOAD_PERSON_REPORT%>';">
		    		<INPUT type="submit" name="" value="Delete" class="bodytext" onclick="document.form.target='_self';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_PERSON_REPORT%>';">
		    	</TD>
		    </TR> 
		</TABLE>
	</TD>
<% } %>
</TR>
</TABLE>
<BR><BR>
<% if (!advancedSearch.booleanValue()) { %>
<P class="bodytext"><A href="JavaScript:document.form.submit();" onclick="document.form.target='_blank';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_GET_GRAPHICAL_REPORT%>';">Graphical Report of Aggregated Study Information</A></P>
<% } %>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_LIST_STUDIES%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>" value="<%=WebConstants.MAX_HIT_LIST_LENGTH%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_START_POSITION%>" value="<%=WebConstants.DEFAULT_START_POSITION%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>" value="" >  
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_ADVANCED_SEARCH%>" value="<%=advancedSearch.toString()%>" >
</FORM>
</TD></TR>  
</TABLE> 
<jsp:include page="bottom.html"/>