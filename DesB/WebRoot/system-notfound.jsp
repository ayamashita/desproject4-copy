<%@include file="WEB-INF/jsp/include/include-top.jsp"%>

  <h1>Page not found</h1>

  <p>The page you tried to access is not available on the system</p>

  <p>
    If you got this message when clicking any links on this site
    (<a href="http://www.simula.no/">http://www.simula.no/</a>),
    please contact
    <a href="mailto:webmaster@simula.no">webmaster@simula.no</a>
    with a description of the problem.
  </p>

<%@include file="WEB-INF/jsp/include/include-bottom.jsp"%>
