<%@page import="java.util.List"%>
<%@include file="include/include-top.jsp"%>
<%
  List nav = (List)request.getAttribute(no.halogen.presentation.web.Constants.NAVIGATION_PATH);
  String action = (String)nav.get(nav.size() - 1);
%>

  <h1>Search studies</h1>

  <html:form action="<%= action %>">

    <logic:messagesPresent>
      <table class="error">
        <tr>
          <td>
            <ul>
              <html:messages id="error">
                <li><bean:write name="error"/></li>
              </html:messages>
            </ul>
          </td>
        </tr>
      </table>
    </logic:messagesPresent>

    <table class="container">
      <tr>
        <td>free text</td>
        <td><html:text property="searchText" size="36"/></td>
      </tr>
      <tr>
        <td>type of study</td>
        <td>
          <html:select property="types" multiple="true" size="5" style="width: 100%">
            <html:optionsCollection name="<%= Constants.BEAN_STUDY_TYPES %>" value="id" label="name"/>
          </html:select>
        </td>
      </tr>
      <tr>
        <td>responsibles</td>
        <td>
          <html:select property="responsibles" multiple="true" size="5" style="width: 100%">
            <html:optionsCollection name="<%= Constants.BEAN_PERSONS %>" value="id" label="fullName"/>
          </html:select>
        </td>
      </tr>
      <tr>
        <td>
          end date (between)<br/>
          (dd/mm-yyyy)
        </td>
        <td>
          <html:text property="startDay" size="2" maxlength="2"/> /
          <html:text property="startMonth" size="2" maxlength="2"/> -
          <html:text property="startYear" size="4" maxlength="4"/>
            <a href="javascript:openWindow('start');">
            <html:img page="/images/calendar.gif" border="0"/>
          </a>
          <br/>
          <html:text property="endDay" size="2" maxlength="2"/> /
          <html:text property="endMonth" size="2" maxlength="2"/> -
          <html:text property="endYear" size="4" maxlength="4"/>
          <a href="javascript:openWindow('end');">
            <html:img page="/images/calendar.gif" border="0"/>
          </a>
        </td>
      </tr>
      <tr>
        <td/>
        <td>
          <html:submit property="search" value="search"/>
          <html:submit property="clear" value="clear fields"/>
        </td>
      </tr>
    </table>
  </html:form>

  <logic:present name="<%= Constants.TABLE_SEARCH_STUDIES %>">
    <logic:notEmpty name="<%= Constants.TABLE_SEARCH_STUDIES %>" property="source.content">
      <halogen:table name="<%= Constants.TABLE_SEARCH_STUDIES %>"/>
      <html:link target="_blank" page="/pdf/studies.pdf">printer friendly version</html:link>
    </logic:notEmpty>
  </logic:present>


<%@include file="include/include-bottom.jsp"%>