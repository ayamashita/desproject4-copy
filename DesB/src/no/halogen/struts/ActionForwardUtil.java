package no.halogen.struts;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionForward;

/** This class contains utility methods for <CODE>ActionForward</CODE>s.
 * @author Stian Eide
 */
public class ActionForwardUtil {
  
  /** Adds a request-parameter to an <CODE>ActionForward</CODE>. To be used if a
   * request parameter on the current request has to be added to a redirecting
   * forward at runtime.
   * @param forward the <CODE>ActionForward</CODE>
   * @param request current <CODE>HttpServletRequest</CODE>
   * @param parameter the name of the request parameter to add
   * @return the modified <CODE>ActionForward</CODE>
   */  
  public static ActionForward addRequestParameter(ActionForward forward, HttpServletRequest request, String parameter) {
    if(forward == null) {
      return(null);
    }
    String value = request.getParameter(parameter);
    StringBuffer path = new StringBuffer(forward.getPath());
    boolean isQuery = (path.indexOf("?") != -1);
    if(isQuery) {
      path.append("&amp;" + parameter + "=" + value);
    }
    else {
      path.append("?" + parameter + "=" + value);
    }
    ActionForward newForward = new ActionForward(
      forward.getName(),
      path.toString(),
      forward.getRedirect(),
      forward.getContextRelative()
      );
    return(newForward);
  }
}