/*
 * Created on 22.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.halogen.utils.sql;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DataSourceConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

/**
 * This class contains common utility methods used when accessing a database
 *
 * @author Frode Langseth
 */

public class SqlHelper {
  /**
   * Field desDs
   */
  static private DataSource desDs;
  /**
   * Field simulaDs
   */
  static private DataSource simulaDs;

  /**
   * Field PROPERTIES
   */
  static final private String PROPERTIES = "no/simula/simula.properties";
  /**
   * Field desJNDIName
   */
  static private String desJNDIName = null;
  /**
   * Field simulaJNDIName
   */
  static private String simulaJNDIName = null;

  /**
   * Field properties
   */
  private static Properties properties;

  //public static String desJNDIName = new String("jdbc:mysql://localhost:3306/des?user=root&autoReconnect=true");
  //public static String simulaJNDIName = new String("jdbc:mysql://localhost:3306/simula?user=root&autoReconnect=true");

  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(SqlHelper.class);

  /**
   * If no db name is provided for the connection, the connection string for the DES database is used 
   * 
   * @return a connection to the database from the pool 
   * 
   * @throws SQLException if there's an error when getting a connection from the datasource
   * @throws NamingException if it's not possible to create the datasource by the name given in the property for the database
   */

  public static Connection getConnection() throws SQLException, NamingException {
    return getConnection(getDesJNDIName());
  }

  /**
   * Returns a connection from the pool to the database provided in the in parameter 
   * 
   * @param db - JNDI name to a database
   * @return a connection to the database from the pool 
   * 
   * @throws SQLException if there's an error when getting a connection from the datasource
   * @throws NamingException if it's not possible to create the datasource by the name given in the property for the database
   */

  public static Connection getConnection(String db) throws SQLException, NamingException {
    InitialContext ctx = new InitialContext();
    DataSource ds = null;
    if (db.equals(getDesJNDIName())) {
      if (desDs == null) {
        desDs = (DataSource) ctx.lookup(db);
      }
      ds = desDs;
    } else if (db.equals(getSimulaJNDIName())) {
      if (simulaDs == null) {
        simulaDs = (DataSource) ctx.lookup(db);
      }
      ds = simulaDs;
    }
    //System.out.println("Aiko Aiko Aiko:" + db);
    Connection conn = ds.getConnection();
    
    /*
        Connection conn = null;
    
        try {
          Class.forName("org.gjt.mm.mysql.Driver");
    
          DataSource ds = getDataSource(db);
    
          conn = ds.getConnection();
    
        } catch (ClassNotFoundException e) {
          log.error("Fant ikke drivfilen for mysql!!!", e);
    
          return null;
        } catch (Exception e) {
          log.error("Fant ikke drivfilen for mysql!!!", e);
    
          return null;
        }*/

    return conn;
  }

  /**
   * Closes a statement and a connection, nice to have to ensure that such objects are "left behind", 
   * and might cause trouble for subsequent statements
   * 
   * @param pstmt the statement to close
   * @param conn the connection to close
   */
  public static void closeConnection(Statement pstmt, Connection conn) {
    try {
      if (pstmt != null) {
        pstmt.close();
        pstmt = null;
      }
    } catch (SQLException e) {
      log.error("Exception closing statement!", e);
    } catch (Exception e) {
      log.error("Exception trying to close statement!", e);
    }
    try {
      if (conn != null && !conn.isClosed()) {
        conn.close();
        conn = null;
      }
    } catch (SQLException e) {
      log.error("Exception closing connection!", e);
    } catch (Exception e) {
      log.error("Exception trying to close connection!", e);
    }
  }

  /**
   * Adds a parameter as a value to a <code>prepared statement</code>
   * Hides the type of the parameter making it easier for the developer to create statements
   *  
   * @param LoggableStatement the prepared statement to populate values into
   * @param i index of the parameter to set the value
   * @param obj the value to populate the prepared statement with
   * 
   * @throws SQLException if anything fails during prepared statement population
   */
  public static void addParameter(LoggableStatement LoggableStatement, int i, Object obj) throws SQLException {
    if (obj instanceof Long) { //			if(log.isDebugEnabled())if(log.isDebugEnabled())log.debug(((Long)obj).longValue() + "(Long)");
      LoggableStatement.setLong(i, ((Long) obj).longValue());
    } else if (obj instanceof Integer) { //			if(log.isDebugEnabled())if(log.isDebugEnabled())log.debug(((Integer)obj).intValue() + "(Integer)");
      LoggableStatement.setInt(i, ((Integer) obj).intValue());
    } else if (obj instanceof Short) { //			if(log.isDebugEnabled())if(log.isDebugEnabled())log.debug(((Short)obj).shortValue() + "(Short)");
      LoggableStatement.setShort(i, ((Short) obj).shortValue());
    } else if (obj instanceof Timestamp) { //			if(log.isDebugEnabled())if(log.isDebugEnabled())log.debug((Timestamp)obj + "(Timestamp)");
      LoggableStatement.setTimestamp(i, (Timestamp) obj);
    } else if (obj instanceof Date) { //			if(log.isDebugEnabled())if(log.isDebugEnabled())log.debug((Timestamp)obj + "(Timestamp)");
      Date date = (Date) obj;
      LoggableStatement.setTimestamp(i, new Timestamp(date.getTime()));
    } else if (obj instanceof Boolean) { //			if(log.isDebugEnabled())if(log.isDebugEnabled())log.debug(((Boolean)obj).booleanValue() + "(Boolean)");
      LoggableStatement.setBoolean(i, ((Boolean) obj).booleanValue());
    } else if (obj instanceof byte[]) {
      byte[] data = (byte[]) obj;
      ByteArrayInputStream inStream = new ByteArrayInputStream(data);
      LoggableStatement.setBinaryStream(i, inStream, data.length);
    } else { //			if(log.isDebugEnabled())if(log.isDebugEnabled())log.debug(obj.toString() + "(String)");
      LoggableStatement.setString(i, obj.toString());
    }
  }

  /**
   * Method setupDataSource
   * @param connectURI String
   * @return DataSource
   */
  public static DataSource setupDataSource(String connectURI) {
    //
    // First, we'll need a ObjectPool that serves as the
    // actual pool of connections.
    //
    // We'll use a GenericObjectPool instance, although
    // any ObjectPool implementation will suffice.
    //
    ObjectPool connectionPool = new GenericObjectPool(null);
    //
    // Next, we'll create a ConnectionFactory that the
    // pool will use to create Connections.
    // We'll use the DriverManagerConnectionFactory,
    // using the connect string passed in the command line
    // arguments.
    //
    ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI, null);
    //
    // Now we'll create the PoolableConnectionFactory, which wraps
    // the "real" Connections created by the ConnectionFactory with
    // the classes that implement the pooling functionality.
    //
    try {
      PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, connectionPool, null, null, false, true);
    } catch (Exception e) {
      log.error("Feil ved henting av connectionFacory", e);
      return (null);
    } //
    // Finally, we create the PoolingDriver itself,
    // passing in the object pool we created.
    //
    PoolingDataSource dataSource = new PoolingDataSource(connectionPool);
    return dataSource;
  }

  /**
   * Method getDataSource
   * @param db String
   * @return DataSource
   * @throws Exception
   */
  public static DataSource getDataSource(String db) throws Exception { //	Create Properties from file in classpath
    /*Properties p = new Properties();
    p.load(DatabaseUtils.class.getResourceAsStream("/" + DB_PROPERTIES));
    */
    BasicDataSource ds = new BasicDataSource();
    ds.setUrl(db);
    ds.setDriverClassName("com.mysql.jdbc.Driver");
    //	(Optional) Set BasicDataSource properties such as maxActive and    maxIdle, as described in
    //    http : //jakarta.apache.org/commons/dbcp/api/org/apache/commons/dbcp/BasicDataSource.html    ds.setMaxActive(5);
    //	Create a PoolableDataSource as described in    http : //jakarta.apache.org/commons/dbcp/api/overview-summary.html#overview_description
    ObjectPool connectionPool = new GenericObjectPool(null);
    ConnectionFactory connectionFactory = new DataSourceConnectionFactory(ds);
    PoolableObjectFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, connectionPool, null, null, false, true);
    PoolingDataSource dataSource = new PoolingDataSource(connectionPool);
    return dataSource;
  }

  /**
   * @return Name used to connect to the connection pool for the Des database
   */
  public static String getDesJNDIName() {
    if (desJNDIName == null) {
      desJNDIName = getProperties().getProperty("des.url");
    }

    return desJNDIName;
  }

  /**
   * @return Name used to connect to the connection pool for the Simula database
   */
  public static String getSimulaJNDIName() {
    if (simulaJNDIName == null) {
      simulaJNDIName = getProperties().getProperty("simula.url");
    }

    return simulaJNDIName;
  }

  /**
   * Gets properties from a property file
   * Used to get database connection strings
   * 
   * @return Properties the properties in the property file
   */
  private static Properties getProperties() {
    if (properties == null) {
      ClassLoader cl = SqlHelper.class.getClassLoader();
      properties = new Properties();
      try {
        properties.load(cl.getResourceAsStream(PROPERTIES));
      } catch (IOException e) {
        log.error("Could not open properties file. This is required for database connection pool", e);
        return (null);
      }
    }

    return properties;
  }

}
