package no.halogen.utils.table;

/** Thrown if the value of this column could not be extracted from the row of a
 * <CODE>Table</CODE>.
 * @author Stian Eide
 */
public class UnreadableRowValueException extends Exception {
  
  /** Creates a new <CODE>UnreadableRowValueException</CODE>. */  
  public UnreadableRowValueException() {}
  
  /** Creates a new <CODE>UnreadableRowValueException</CODE> with the specified
   * message.
   * @param message a text message describing the error
   */  
  public UnreadableRowValueException(String message) {
    super(message);
  }
}