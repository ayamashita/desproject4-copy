/*
 * Created on 22.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.halogen.statements;

import java.sql.DriverManager;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StatementException extends Exception {
	/**
	 * Constructs an <code>Statementxception</code> object with a reason;
	 *
	 * @param reason a description of the exception 
	 */

	public StatementException(String reason) {
		super(reason);

		if (DriverManager.getLogWriter() != null) {
			printStackTrace(DriverManager.getLogWriter());
		}
	}

	/**
	 * Constructs an <code>StatementException</code> object;
	 *
	 */
	public StatementException() {
		super();

		if (DriverManager.getLogWriter() != null) {
			printStackTrace(DriverManager.getLogWriter());
		}
	}

}
