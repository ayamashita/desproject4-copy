package no.simula.des;

import java.util.List;
import no.halogen.search.SearchFactory;
import no.halogen.utils.table.TableSource;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.search.SearchFactoryImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** This is a table source that has the results from a study search as content. The
 * <CODE>update</CODE>-method will execute the search.
 * @author Stian Eide
 */
public class StudySearchTableSource implements TableSource {
  
  /** Creates a new instance of <CODE>StudySearchTableSource</CODE> with the list of
   * criteria.
   * @param criteria the criteria that the search result must satisfy
   */
  public StudySearchTableSource(List criteria) {
    this.criteria = criteria;
    update();
  }

  private static Log log = LogFactory.getLog(StudySearchTableSource.class);

  private List criteria;
  private List content;
  
  /** Returns a list of studies that satisfy the list of criteria.
   * @return a list of studies that satisfy the list of criteria
   */  
  public List getContent() {
    return(content);
  }

  /** Execurtes the study search with the list of criteria. */  
  public void update() {
    try {
      Simula simula = SimulaFactory.getSimula();
      content = simula.getStudiesByCriteria(criteria);
    }
    catch(Exception e) {
      log.error("Could not update Table Source.", e);
    }
  }
}