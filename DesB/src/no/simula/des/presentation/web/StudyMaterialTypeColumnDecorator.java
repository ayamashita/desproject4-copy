package no.simula.des.presentation.web;

import java.util.Properties;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import no.halogen.utils.table.ColumnDecorator;
import org.apache.struts.Globals;
import org.apache.struts.util.RequestUtils;

/** Looks up the display name for the given type of study material 
 * in the application resource file as specified by <CODE>resource</CODE>.
 *
 * The key, which is looked up in <CODE>resource</CODE>, will be 
 * <code>studymaterial.type.&lt;study-material-type&gt;</code>.
 *
 * @author Stian Eide
 */
public class StudyMaterialTypeColumnDecorator implements ColumnDecorator {
  
  /** Creates a new <CODE>StudyMaterialTypeColumnDecorator</CODE>. */
  public StudyMaterialTypeColumnDecorator() {}
  
  private Properties resource;
  
  /** The prefix used in front of the <CODE>type</CODE> defined in the study material
   * in order to create a key to lookup.
   */  
  private static final String KEY_PREFIX = "studymaterial.type.";
  
  /** Renders the study material type as given by <CODE>value</CODE>. Please note that
   * <CODE>resource</CODE> must be set before invoking this method.
   * @param value the <CODE>type</CODE> of study material
   * @throws IllegalStateException if <CODE>resource</CODE> is not set
   * @return the rendered value
   */  
  public String render(Object value) throws IllegalStateException {
    if(resource == null) {
      throw new IllegalStateException("Can't invoke render() before resource is set.");
    }
    String key = KEY_PREFIX + value;
    return(resource.getProperty(key));
  }
  
  /** Returns the message resource.
   * @return Value of property resource.
   */
  public Properties getResource() {
    return(resource);
  }
  
  /** Sets the message resource to be used when looking up keys.
   * @param resource New value of property resource.
   */
  public void setResource(Properties resource) {
    this.resource = resource;
  }
  
}