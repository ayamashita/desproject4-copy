package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.File;
import no.simula.des.Link;
import no.simula.des.Study;
import no.simula.des.StudyMaterial;
import no.simula.des.StudyMaterialManager;
import no.simula.des.presentation.web.forms.StudyForm;
import no.simula.des.presentation.web.forms.StudyMaterialForm;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.struts.action.ActionErrors;

/** Handles creation of new study material, editing existing study material and
 * saving changes in both cases.
 * <p>
 * The study material will not be saved if no name is specified or if the name is equal
 * to another study material's name.
 * <p>
 * Returns <CODE>edit</CODE> when the study material form should be displayed and
 * <CODE>return</CODE> when cancel is hit or the changes have been successfully
 * saved.
 */
public class StudyMaterialEditAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {

    ///////////////////////////////////////////////////////
    // Inputs that redirects the user to other
    // actions than this one.
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }

    StudyMaterialManager smm = StudyMaterialManager.getInstance();
    StudyMaterialForm form = (StudyMaterialForm)actionForm;
    ActionErrors errors = null;
    Simula simula = SimulaFactory.getSimula();
        
    if(saveChanges(request)) {

      errors = validate(simula, form);
      if(errors == null) {
        
        StudyMaterial studyMaterial = smm.createStudyMaterial(form.getType());

        studyMaterial.setDescription(form.getDescription());
        if(form.getType().equals(File.TYPE)) {
          File file = (File)studyMaterial;
          if(form.getRemoteFile() == null) {
            file.setContent(form.getContent());
            file.setName(form.getName());
          }
          else {
            file.setContent(form.getRemoteFile().getFileData());
            file.setName(form.getRemoteFile().getFileName());
          }
        }
        else {
          Link link = (Link)studyMaterial;
          link.setUrl(form.getUrl());
        }

        if(isNewStudyMaterial(form)) {
          simula.addStudyMaterial(studyMaterial);
        }
        else {
          studyMaterial.setId(form.getId());
          simula.storeStudyMaterial(studyMaterial);
        }

        return(mapping.findForward("return"));
      }
    }

    ///////////////////////////////////////////////////////
    // Mutually exclusive actions that directs the user
    // to the main view of this action

    if(isInitialRequest(request)) {
      
      form.setTypes(smm.getTypes());
      form.setId(null);
      form.setName(null);
      form.setContent(null);
      form.setRemoteFile(null);
      form.setDescription(null);
      form.setUrl(null);
      
      if(isNew(request)) {
        form.setType((String)StudyMaterialManager.TYPES.get(0));
      }
      else {
        Integer id = Integer.valueOf(request.getParameter("id"));
        BeanUtils.copyProperties(form, simula.getStudyMaterial(id));
        if(form.getType().equals(File.TYPE)) {
          form.setUrl(null);
        }
      }
    }
    
    else if(removeFile(request)) {
      if(form.getRemoteFile() != null) {
        form.getRemoteFile().destroy();
        form.setRemoteFile(null);
      }
      form.setName(null);
      form.setContent(null);
    }

    if(errors != null) {
      saveErrors(request, errors);
    }
    return(mapping.findForward("edit"));
  }

  ///////////////////////////////////////////////////////////
  // Helper methods to make main method cleaner and more
  // readable.

  private ActionErrors validate(Simula simula, StudyMaterialForm form) throws Exception {
    ActionErrors errors = new ActionErrors();
    
    if(form.getDescription() == null || form.getDescription().length() == 0) {
      ActionError error = new ActionError("errors.required", "description");
      errors.add("url", error);
    }
    
    if(form.getType().equals(File.TYPE)) {
      if(form.getRemoteFile() != null && form.getRemoteFile().getFileSize() == 0 && form.getName() == null) {
        ActionError error = new ActionError("errors.studymaterial.file");
        errors.add("remoteFile", error);
      }
    }
    
    else {
      if(form.getUrl() == null || form.getUrl().length() == 0) {
        ActionError error = new ActionError("errors.studymaterial.link");
        errors.add("url", error);
      }
      
      if(form.getUrl().indexOf("://") == -1) {
        ActionError error = new ActionError("errors.studymaterial.link.protocol");
        errors.add("url", error);
      }
    }
    
    if(errors.isEmpty()) {
      return(null);
    }
    else {
      return(errors);
    }
  }
  
  private boolean isInitialRequest(HttpServletRequest request) {
    return(request.getParameter("new") != null || request.getParameter("id") != null);
  }
  
  private boolean isNew(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }

  private boolean attachFile(HttpServletRequest request) {
    return(request.getParameter("attach") != null);
  }

  private boolean removeFile(HttpServletRequest request) {
    return(request.getParameter("remove") != null);
  }
  
  private boolean uploadFile(HttpServletRequest request) {
    return(request.getParameter("upload") != null);
  }
  
  private boolean attachIsCancelled(HttpServletRequest request) {
    return(request.getParameter("attach-cancel") != null);
  }

  private boolean saveChanges(HttpServletRequest request) {
    return(request.getParameter("save") != null);
  }

  private boolean isNewStudyMaterial(StudyMaterialForm form) {
    return(form.getId() == null);
  }
}