package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.Study;

/** Controls the flow of the delete studies process. The request may be in
 * two states:
 * <ul>
 *  <li>
 *    A list of deletable studies exists in session
 *  </li>
 *  <li>
 *    If not an <CODE>error</CODE> action forward is issued
 *  </li>
 * </ul>
 *
 *
 * Next a study, if specified by the <CODE>id</CODE> request parameter,
 * is deleted.
 *
 * The deleted study is deleted from deletable studies, and the next
 * in this list is processed. If the list is empty a <CODE>return</CODE> action
 * forward is returned.
 *
 * The study is added as deletable and a <CODE>confirm</CODE> action
 * forward is returned.
 */
public class StudiesDeleteAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
    
    Simula simula = SimulaFactory.getSimula();
    
    // Get deletable studies from the session.
    List deletableStudies = (List)request.getSession().getAttribute(Constants.DELETE_STUDIES);
    
    // If the list is null at this point, we have an illegal request and return an error.
    if(deletableStudies == null) {
      return(mapping.findForward("error"));
    }

    // Lets delete a study as specified in the request and display either a new
    // confirmation if there are more studies to be deleted or the new list of
    // studies.
    else {
      String deleteIdString = request.getParameter("id");
      if(!skipStudy(request) && deleteIdString != null) {
        Integer deleteId = Integer.valueOf(deleteIdString);
        simula.deleteStudy(deleteId);
      }
      if(deletableStudies.isEmpty()) {
        return(mapping.findForward("return"));
      }
      else {
        Study study = (Study)deletableStudies.remove(0);
        request.getSession().setAttribute(Constants.DELETE_STUDIES, deletableStudies);
        request.setAttribute(Constants.DELETE_THIS_STUDY, study);
        return(mapping.findForward("confirm"));
      }
    }
  }
  
  private boolean skipStudy(HttpServletRequest request) {
    return(request.getParameter("skip") != null);
  }
}