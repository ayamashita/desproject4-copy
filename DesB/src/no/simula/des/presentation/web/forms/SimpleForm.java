package no.simula.des.presentation.web.forms;

import no.halogen.presentation.web.CheckNewActionForm;

/** This is a form that contains only a <CODE>id</CODE> and a <CODE>name</CODE>
 * property. This form is used by several action mappings.
 * @author Stian Eide
 */
public class SimpleForm extends CheckNewActionForm {
  
  /** Holds value of property name. */
  private String name;
  
  /** Creates a new instance of SimpleForm */
  public SimpleForm() {}
  
  /** Getter for property name.
   * @return Value of property name.
   *
   */
  public String getName() {
    return this.name;
  }
  
  /** Setter for property name.
   * @param name New value of property name.
   *
   */
  public void setName(String name) {
    this.name = name;
  }
  
}
