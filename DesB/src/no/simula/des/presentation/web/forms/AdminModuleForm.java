package no.simula.des.presentation.web.forms;

import no.halogen.presentation.web.CheckNewActionForm;

/** Holds data to be displayed when editing admin opening text. */
public class AdminModuleForm extends CheckNewActionForm {
    
  /** Holds value of property openingText. */
  private String openingText;
  
  /** Getter for property description.
   * @return Value of property description.
   *
   */
  public String getOpeningText() {
    return(openingText);
  }
  
  /** Setter for property openingText.
   * @param openingText the opening text
   */
  public void setOpeningText(String openingText) {
    this.openingText = openingText;
  }
}