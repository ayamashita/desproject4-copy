/*
 * Created on 08.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.simula.des.File;
import no.simula.des.Link;

import no.simula.des.StudyMaterialManager;
import junit.framework.TestCase;

/**
 * @author frodel
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentStudyMaterialTest extends TestCase {
	/**
	 * Field persistentStudyMaterial
	 */
	private PersistentStudyMaterial persistentStudyMaterial = null;
	
	/**
	 * Field file1
	 */
	File file1= null;
	/**
	 * Field file2
	 */
	File file2= null;
	/**
	 * Field link1
	 */
	Link link1= null;
	/**
	 * Field link2
	 */
	Link link2= null;
	
	private List oneElement = null;
	/**
	 * Field allElements
	 */
	private List allElements = null;
	/**
	 * Field emptyElements
	 */
	private List emptyElements = null;
	
  /**
   * Method main
   * @param args String[]
   */
  public static void main(String[] args) {
  }

  /*
   * @see TestCase#setUp()
   */
  /**
   * Method setUp
   * @throws Exception
   */
  protected void setUp() throws Exception {
    super.setUp();
    
    persistentStudyMaterial = new PersistentStudyMaterial();
    
    StudyMaterialManager materialManager = StudyMaterialManager.getInstance();
    
    file1 = (File) materialManager.createStudyMaterial("file");
		file2 = (File) materialManager.createStudyMaterial("file");
		link1 = (Link) materialManager.createStudyMaterial("link");
		link2 = (Link) materialManager.createStudyMaterial("link");
		
		file1.setDescription("Dette er den f�rste filen");
		file1.setName("fil1");
		
		file2.setDescription("Dette er den andre filen");
		file2.setName("fil2");
		
		link1.setDescription("Dette er den f�rste linken");
		link1.setUrl("http://www.saab.no");
		
		link2.setDescription("Dette er den andre linken");
		link2.setUrl("http://www.saab.com");
		
		oneElement = new ArrayList();
		oneElement.add(file2);
		
		allElements = new ArrayList();
		allElements.add(file1);
		allElements.add(file2);
		allElements.add(link1);
		allElements.add(link2);
		
		emptyElements = new ArrayList();
  }

  /**
   * Method testCreate
   */
  public void testCreate() {
  	try {
      persistentStudyMaterial.create(link1);
			persistentStudyMaterial.create(link2);
    } catch (CreatePersistentObjectException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * Method testDelete
   */
  public void testDelete() {
  }

  /**
   * Method testFindByKey
   */
  public void testFindByKey() {
  }

  /**
   * Method testFindByKeys
   */
  public void testFindByKeys() {
  }

  /**
   * Method testFindByStudy
   */
  public void testFindByStudy() {
  }

  /*
   * Class to test for void update(Integer, Object)
   */
  /**
   * Method testUpdateIntegerObject
   */
  public void testUpdateIntegerObject() {
  }

  /*
   * Class to test for void update(Object)
   */
  /**
   * Method testUpdateObject
   */
  public void testUpdateObject() {
  }

  /**
   * Method testFind
   */
  public void testFind() {
  }

}
