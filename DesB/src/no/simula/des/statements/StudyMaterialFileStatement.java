/*
 * Created on 30.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.simula.des.File;

import no.halogen.statements.ObjectStatementImpl;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StudyMaterialFileStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(StudyMaterialFileStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "no.simula.des.File";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "smf_sm_id, smf_filename, smf_file";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?, ?, ?)";

  private final String KEY = "smf_sm_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "smf_sm_id, smf_filename, smf_file";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "smf_smfile";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "smt_sm_id = ?, smf_filename = ?, smf_file = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return (INSERT_COLUMNS);
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return (KEY);
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return (SELECT_COLUMNS);
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return (TABLE_NAME);
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return (UPDATE_VALUES);
  }

  /** 
   * Populates the data bean with the result from a query
   * Since StudyMaterial is abstract, and implemented by File, this method will populate File from both sm_studymaterial and smf_smfile
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    File studyMaterialFile = null;

    try {
      if (getDataBean() != null) {
        studyMaterialFile = (File) getDataBean().getClass().newInstance();
      } else {
        studyMaterialFile = new File();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new study material file data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new study material file data object");
      e.printStackTrace();
    }

    studyMaterialFile.setId(new Integer(rs.getInt("smf_sm_id")));
    studyMaterialFile.setDescription(rs.getString("sm_description"));
    studyMaterialFile.setName(rs.getString("smf_filename"));

    Blob data = rs.getBlob("smf_file");
    studyMaterialFile.setContent(data.getBytes(1L, (int) data.length()));

    return (studyMaterialFile);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /**
   * Populates the prepared statement string for an update or insert statement with values from the data bean
   * 
   * Used e.g. by INSERT statements
   * The string does not contain the id 
   * 
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      File file = (File) getDataBean();

      pstmt.setInt(1, file.getId().intValue());
      pstmt.setString(2, file.getName());

      ByteArrayInputStream inStream = new java.io.ByteArrayInputStream(file.getContent());
      pstmt.setBinaryStream(3, inStream, file.getContent().length);
    }
  }

}
