package no.simula.des;

/** This is a study material of the type File. A file has binary content that may be
 * stored and retrived from a persistent store.
 */
public class File extends StudyMaterial {

  /** Creates an empty file. */  
  public File() {
    super();
  }
  
  /** Creates a file with the specified id, description, name and content.
   * @param id the id
   * @param description the description
   * @param name the file name
   * @param content the file content
   */  
  public File(Integer id, String description, String name, byte[] content) {
    super(id, description);
    this.name = name;
    this.content = content;
  }
  
  public static final String TYPE = "file";

  private byte[] content;
  private String name;
  
  /** Returns the file content.
   * @return the file content
   */  
  public byte[] getContent() {
    return(content);
  }
  
  /** Returns the file name.
   * @return the file name
   */  
  public String getName() {
    return(name);
  }
  
  /** Sets the file content
   * @param content the file content
   */  
  public void setContent(byte[] content) {
    this.content = content;
  }
  
  /** Sets the file name.
   * @param name the file name
   */  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getUrl() {
    // TODO: Change this into: return("/" + id.toString() + "/" + name);
    // (also change JSPs that call this method accordingly)
    return(id.toString());
  }
  
  public String getType() {
    return(TYPE);
  }  
}