/**
 * @(#) CriteriumFactoryImpl.java
 */

package no.simula.des.search;

import java.util.Iterator;
import java.util.List;

import no.halogen.search.Criterium;
import no.halogen.search.Search;
import no.halogen.search.SearchFactory;

/**
 * @author Frode Langseth
 */
public class SearchFactoryImpl implements SearchFactory {
	
	/* (non-Javadoc)
	 * @see halogen.search.CriteriumFactory#create(halogen.search.Criterium)
	 */
	/**
	 * Method create
	 * @param search String
	 * @return Search
	 * @see no.halogen.search.SearchFactory#create(String)
	 */
	public Search create(String search) {
		Search concreteSearch= null;

		if(search.equals("study")) {
			concreteSearch= new StudySearch();
		}

		return concreteSearch;
	}

	/**
	 * Method create
	 * @param search String
	 * @param criteria List
	 * @return Search
	 * @see no.halogen.search.SearchFactory#create(String, List)
	 */
	public Search create(String search, List criteria) {
		Search concreteSearch= null;

		if(search.equals("study")) {
			concreteSearch= new StudySearch();
			
			Iterator i = criteria.iterator();
			
			while(i.hasNext()) {
				concreteSearch.addCriterium((Criterium) i.next());
			}
		}

		return concreteSearch;
	}

}
